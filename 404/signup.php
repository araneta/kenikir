<?php
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$email = $_POST['email'];
	$agency = $_POST['agency'];
	$message = $_POST['message'];
	$error = array();
	if(empty($first_name)){
		$error[] = 'First Name is required';	
	}
	if(empty($last_name)){
		$error[] = 'Last Name is required';	
	}	
	if(empty($email)){
		$error[] = 'Email is required';	
	}else{
		if(!validEmail($email)){
			$error[] = 'Invalid Email';	
		}		
	}
	if(empty($agency)){
		$error[] = 'Agency is required';	
	}
	/*if(empty($message)){
		$error[] = 'Message is required';	
	}*/
	if(count($error)>0){
		$msg = implode('<br />',$error);
		$msg .= '<br /><a href="http://govonomy.com/">Back</a>';
		die($msg);
	}else{
		require dirname(__FILE__).'/PHPMailer-master/class.phpmailer.php';
		$msg = 'First Name : ' . $first_name;
		$msg .= '<br />'.'Last Name : ' . $last_name;
		$msg .= '<br />'.'Email : ' . $email;
		$msg .= '<br />'.'Agency : ' . $agency;
		$msg .= '<br />'.'Message : ' . $message;
		
		$mail = new PHPMailer;
		/*
		$mail->IsSMTP();    
		$mail->SMTPDebug = 2;
		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';                                  // Set mailer to use SMTP
		$mail->Host = 'smtp.gmail.com';  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'nopicishoax@gmail.com';                            // SMTP username
		$mail->Password = 'P@ssw0rd';                           // SMTP password
		$mail->Port       = 587;
		$mail->SMTPSecure = 'ssl';                          // Enable encryption, 'ssl' also accepted
		*/	
		$mail->From = 'automailer@govonomy.com';
		$mail->FromName = 'automailer';
		$mail->AddAddress('ty@lmcdata.com', 'Ty');  // Add a recipient
		//$mail->AddAddress('aldo@coralyn.com');               // Name is optional				
		$mail->AddBCC('coralyn@coralyn.com');		
		$mail->IsHTML(true);                                  // Set email format to HTML

		$mail->Subject = 'Govonomy: Sign Up';
		$mail->Body    = $msg;		

		if(!$mail->Send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}		
		header('location: thankyou.html');exit(0);
		
	}	
	/**
	Validate an email address.
	Provide email address (raw input)
	Returns true if the email address has the email 
	address format and the domain exists.
	*/
	function validEmail($email)
	{
	   $isValid = true;
	   $atIndex = strrpos($email, "@");
	   if (is_bool($atIndex) && !$atIndex)
	   {
		  $isValid = false;
	   }
	   else
	   {
		  $domain = substr($email, $atIndex+1);
		  $local = substr($email, 0, $atIndex);
		  $localLen = strlen($local);
		  $domainLen = strlen($domain);
		  if ($localLen < 1 || $localLen > 64)
		  {
			 // local part length exceeded
			 $isValid = false;
		  }
		  else if ($domainLen < 1 || $domainLen > 255)
		  {
			 // domain part length exceeded
			 $isValid = false;
		  }
		  else if ($local[0] == '.' || $local[$localLen-1] == '.')
		  {
			 // local part starts or ends with '.'
			 $isValid = false;
		  }
		  else if (preg_match('/\\.\\./', $local))
		  {
			 // local part has two consecutive dots
			 $isValid = false;
		  }
		  else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
		  {
			 // character not valid in domain part
			 $isValid = false;
		  }
		  else if (preg_match('/\\.\\./', $domain))
		  {
			 // domain part has two consecutive dots
			 $isValid = false;
		  }
		  else if
	(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
					 str_replace("\\\\","",$local)))
		  {
			 // character not valid in local part unless 
			 // local part is quoted
			 if (!preg_match('/^"(\\\\"|[^"])+"$/',
				 str_replace("\\\\","",$local)))
			 {
				$isValid = false;
			 }
		  }
		  if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
		  {
			 // domain not found in DNS
			 $isValid = false;
		  }
	   }
	   return $isValid;
	}
?>
	
