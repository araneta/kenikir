var catwidth = 195;		
var islogin = false;
var iscartempty = true;
jQuery(document).ready(function($){ 
	function fixCategoryBox(){
		var t = $("#category option:selected").html();
		$('#selcat').html(t);
		var catsize = (30 + parseInt($('#selcat').width(),10));
		$('#cats').css({marginLeft:-1*catsize});
		$('#keywords').css({width: parseInt($('#middlest').width(),10)-catsize-10});
	}
	$('#cats').click(function(){				
		$('#catsddl').css({marginLeft:-1*(catwidth +parseInt($('#cats').css('margin-left'),10))});
		$('#catsddl').toggle('fast',function(){
			$('#category').attr('size',6);						
		});	
	});
	$('#category').change(function(){
		fixCategoryBox();
	});
	$('.titem').click(function(e){
		e.stopPropagation();
		$('.arrow_box').hide();
		$('#'+$(this).attr('target')).toggle();	
	});
	$(document).click(function(){
		$('.arrow_box').hide();		
	});
	$(document).hover(function(){
		$('#cartitem').hide();
		$('#wishitem').hide();
	});
	/*$('.signin').click(function(){
		$('#usernamebox').attr('target','accountbox');
		$('#usernamebox .ttitle').html('Hello, John!');
		$('#usernamebox .tdesc').html('Welcome Back');
		$('.arrow_box').hide();
	});
	$('.signout').click(function(){
		$('#usernamebox').attr('target','loginregister');
		$('#usernamebox .ttitle').html('Hello!');
		$('#usernamebox .tdesc').html('Sign In or Register');
		$('.arrow_box').hide();
	});*/
	
	/*$('.viewcartitem').click(function(){
		$('#cartbox').attr('target','cartitem');
		$('#cartbox .tdesc').html('2 Items');
		$('.arrow_box').hide();
	});
	$('.viewcartempty').click(function(){
		$('#cartbox').attr('target','cartempty');
		$('#cartbox .tdesc').html('0 Items');
		$('.arrow_box').hide();
	});*/
	$('#cartbox').hover(function(e){		
		e.stopPropagation();
		if(!$('#cartempty').is(':visible'))
			$('#cartitem').show();
	},function(){
		//$('#cartitem').hide('slow');
	});
	$('#wishbox').hover(function(e){		
		e.stopPropagation();
		if(!$('#wishempty').is(':visible'))
			$('#wishitem').show();
	},function(){
		//$('#wishitem').hide('slow');
	});
	/*
	$('.wishitem').click(function(){
		$('#wishbox').attr('target','wishitem');
		$('#wishbox .tdesc').html('3 Lists');
		$('.arrow_box').hide();
	});
	$('.wishempty').click(function(){
		$('#wishbox').attr('target','wishempty');
		$('#wishbox .tdesc').html('0 Lists');
		$('.arrow_box').hide();
	});
	*/ 
	fixCategoryBox();
});
