		     
//http://stackoverflow.com/a/2855946
function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	return pattern.test(emailAddress);
}
function validateForm(selector,errorcallback){
	jQuery(selector).submit(function(){
		var valid = true;
		jQuery('.error').removeClass('error');
		jQuery(this).find('.required').each(function() {
			var validx = true;
			var attr = jQuery(this).attr('disabled');
			// For some browsers, `attr` is undefined; for others,
			// `attr` is false.  Check for both.
			if (typeof attr !== 'undefined' && attr !== false) {
				
			}else{
				validx = jQuery.trim("" +jQuery(this).val())!="" ;
				
				if(jQuery(this).hasClass('email')){					
					validx &= isValidEmailAddress(jQuery(this).val());
				}
				// Get the base name by removing _2
				var name = jQuery(this).attr('name').replace(/_2$/, '');
				// Compare 1 to 2
				if( $('input[name="'+name+'_1"]').val() != $('input[name="'+name+'_2"]').val()) {
					 validx = false;
				}				
				if(!validx){
					jQuery(this).addClass('error').removeClass('valid');
				}else{
					jQuery(this).removeClass('error').addClass('valid');
				}
				
			}
			valid &= validx;
		});      
		if(!valid){
			if (errorcallback && typeof(errorcallback) === "function") {
				errorcallback();
			}
			return false;	
		}      			
	}); 
}
	
