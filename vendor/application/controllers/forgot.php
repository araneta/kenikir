<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        		
	}
	
	function index()
	{				 
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'forgot/email';		
		$this->load->view('template',$this->data);
	}
	function blank()
	{				 
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'forgot/forgot_blank';		
		$this->load->view('template',$this->data);
	}
	function verify_email(){
		$this->import('entities/vendorloginform');
		$this->import('process/vendorprocess');		
				
		$form = new VendorForgotForm1();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('forgot','refresh');	
		}else{						
			$process = new VendorProcess($this->Gateway);
			$vendor = $process->get_vendor_by_email($form->PrimaryEmail);
			if($vendor!=NULL){				
				$this->data['title'] = 'Become a GOVonomy Member';		
				$this->data['main'] = 'forgot/forgot';		
				$this->data['vendor'] = $vendor;
				$this->load->view('template',$this->data);
			}else
			{
				$this->session->set_flashdata('message','Email not found');
				redirect('forgot','refresh');	
			}	
		}
	}
	function verify(){
			
		$this->import('process/vendorprocess');
		$this->import('entities/vendorloginform');
		
		$form = new VendorForgotForm2();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('forgot','refresh');	
		}else{						
			$process = new VendorProcess($this->Gateway);
			if($process->recover_password($form)){				
				//die('Password changed, please check your email');
				$this->session->set_flashdata('message','Password changed, please check your email');
				redirect('forgot/blank','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('forgot','refresh');	
			}	
		}
		
	}
	
	
}
?>
