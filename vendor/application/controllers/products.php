<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        	
		$this->need_login();
	}
	function product_images_dir(){
		return $this->config->item('vendor_product_images_dir').'/'.$this->session->userdata('mediaid');
	}
	function product_thumbs_dir(){
		return $this->config->item('vendor_product_thumbs_dir').'/'.$this->session->userdata('mediaid');
	}
	function product_videos_dir(){		
		return $this->config->item('vendor_product_videos_dir').'/'.$this->session->userdata('mediaid');
	}
	function product_docs_dir(){
		return $this->config->item('vendor_product_docs_dir').'/'.$this->session->userdata('mediaid');
	}
	function set_js_css(){
		$this->data['cssfiles'] = array(
			$this->config->item('jqueryui_css'),
			//'jquery.tagit.css',
			//'tagit.ui-zendesk.css',
			'jquery.multiselect.css',
			'jquery.fileupload-ui.css');
		$this->data['jsfiles'] = array(
			$this->config->item('jqueryui_js'),
			'ajaxfileupload.js',
			//'tag-it.js'			
			'jquery.multiselect.min.js'
			);		
	}
	function index()
	{				 		
		$this->data['title'] = 'View Products List';		
		$this->data['main'] = 'product/list';
		$this->data['jsfiles'] = array('jquery.dataTables.min.js');
		$this->data['cssfiles'] = array('demo_table_jui.css');	
		$this->load->view('template',$this->data);
	}
	public function add(){
		$this->import('entities/productform');
		$this->data['title'] = 'Add New Product';		
		$this->data['main'] = 'product/add';
		$this->set_js_css();
		$product = new AddProductForm();		
		$this->data['product'] = $product;
		$this->import('process/productprocess');
		$process = new ProductProcess($this->Gateway);
		$this->data['solutiontags'] = $process->get_solution_tags('%%');
		$this->data['federaltags'] = $process->get_federal_tags('%%');
		
		$this->load->view('template',$this->data);	
	}
	function save(){
		$this->import('process/productprocess');
		$this->import('entities/productform');
		$this->import('helpers/imagehelper');
		
		$form = new AddProductForm();
		$this->bind($form,'POST');
		$form->VendorID = $this->session->userdata('userid');		
		//var_dump($form);exit(0);
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());						
		}else{			
			if(is_array($form->SolutionTags))
				$form->SolutionTags = implode(',',$form->SolutionTags);
			if(is_array($form->FederalTags))	
				$form->FederalTags = implode(',',$form->FederalTags);
			$imgfile = $this->product_images_dir().'/'.$form->ProductImage;
			$thumbpath = $this->product_thumbs_dir().'/'.$form->ProductImage;
			if(ImageHelper::create_thumb($imgfile,$thumbpath,$this->config->item('thumb_width'),$this->config->item('thumb_height'))==FALSE){
				$this->session->set_flashdata('message','Error creating thumb image');																
			}else{
				$process = new ProductProcess($this->Gateway);
				if(empty($form->ProductID)){
					$retprocess = $process->add_product($form);
				}else{
					$retprocess = $process->update_product($form);
				}
				
				if($retprocess){
					//delete other image files
					$process->clean_product_dirs(
						$this->session->userdata('userid'),
						$this->product_images_dir(),
						$this->product_thumbs_dir(),
						$this->product_videos_dir(),
						$this->product_docs_dir()
					);
					$this->session->set_flashdata('message','Product saved');							
					
				}else
				{					
					$this->session->set_flashdata('message',$process->error_messages());					
				}					
			}
		}	
		if(!empty($form->ProductID))
			redirect('products/edit/'.$form->ProductID,'refresh');
		else	
			redirect('products','refresh');					
	}
	function edit($productID){
		$this->data['title'] = 'Edit a Product';		
		$this->data['main'] = 'product/add';
		$this->set_js_css();
		
		$this->import('process/productprocess');
		$this->import('entities/productform');
		$process = new ProductProcess($this->Gateway);
		$product = $process->get_product_and_media_by_id(id_clean($productID));
		if($product->VendorID!=$this->session->userdata('userid'))
			die('Product not found');
		if($product==NULL){
			$product = new AddProductForm();
		}
		$this->data['product'] = $product;
		$this->import('process/productprocess');
		$process = new ProductProcess($this->Gateway);
		$this->data['solutiontags'] = $process->get_solution_tags('%%');
		$this->data['federaltags'] = $process->get_federal_tags('%%');
		$this->load->view('template',$this->data);
	}
	/*ajax*/
	
	function list_products(){
		$this->import('entities/paging');
		$this->import('process/productprocess');
		$form = new Paging();
		$this->bind($form,'GET');
		if(!$form->validate()){
			echo 'error bind paging'.$form->error_messages();
		}else{
			$cols = array('ProductID','ProductID','Name','CreatedDate','Active','Url');
			$form->sortby = $cols[$form->sortby];
			
			$process = new ProductProcess($this->Gateway);
			$products = $process->get_products_by_vendor($form,$this->session->userdata('userid'));
			if($products->data!=NULL){
				foreach($products->data as &$prod){					
					$prod->Url = sanitize_filename($prod->Name);
				}
			}
			$this->to_datatables_json($products,$cols);
		}	
	}
	function upload_image_file(){		
		$file_element_name = 'imagefile';
		$status = "";
		$msg = "";
		$upload_path = $this->product_images_dir();	
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777);			
		}   
		if (!file_exists($this->product_thumbs_dir())) {
			mkdir($this->product_thumbs_dir(), 0777);			
		}   
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] =  $this->config->item('image_upload_file_type');
		$config['max_size']  = $this->config->item('image_max_size');
		$config['encrypt_name'] = FALSE;
		$config['overwrite'] = false;		
		$config['file_name'] = $_FILES['imagefile']['name'];		
		
		$this->load->library('upload', $config);
 
		if (!$this->upload->do_upload($file_element_name))
		{
			$status = 'error';
			$msg = $this->upload->display_errors('', '');
		}
		else
		{
			$data = $this->upload->data();			
			$image = $data['full_path'];
			$status = "success";
			//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
			$msg = $data['file_name'];
			//@unlink($image);					

		}
		@unlink($_FILES[$file_element_name]);
	   
		echo json_encode(array('status' => $status, 'msg' => $msg,'originalfname'=>$data['orig_name']));
	}
	function upload_video_file(){		
		
		$file_element_name = 'videofile';
		$status = "";
		$msg = "";
		$upload_path = $this->product_videos_dir();	
		if (!file_exists($upload_path)) {			
			mkdir($upload_path, 0777);			
		}  	   
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] =  $this->config->item('video_upload_file_type');
		$config['max_size']  = $this->config->item('video_max_size');
		$config['encrypt_name'] = FALSE;
		$config['overwrite'] = FALSE;		
		$config['file_name'] = $_FILES['videofile']['name'];		
		
		$this->load->library('upload', $config);
		$fname = '';
		$ret = $this->upload->do_upload($file_element_name);
		$data = $this->upload->data();			
		if (!$ret)
		{
			$status = 'error';
			$msg = $data['file_name'].' : '.$this->upload->display_errors('', '');
		}
		else
		{			
			$image = $data['full_path'];
			$status = "success";
			//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
			$msg = $data['file_name'];
			//@unlink($image);				
			$fname  = $data['orig_name'];	

		}
		@unlink($_FILES[$file_element_name]);
	   
		echo json_encode(array('status' => $status, 'msg' => $msg,'originalfname'=>$fname));
	}
	function upload_doc_file(){		
		
		$file_element_name = 'docfile';
		$status = "";
		$msg = "";
		$upload_path = $this->product_docs_dir();	
		if (!file_exists($upload_path)) {
			mkdir($upload_path, 0777);			
		}	   
		$config['upload_path'] = $upload_path;
		$config['allowed_types'] =  $this->config->item('doc_upload_file_type');
		$config['max_size']  = $this->config->item('doc_max_size');
		$config['encrypt_name'] = FALSE;
		$config['overwrite'] = FALSE;		
		$config['file_name'] = $_FILES['docfile']['name'];		
		
		$this->load->library('upload', $config);
		$fname = '';
		if (!$this->upload->do_upload($file_element_name))
		{
			$status = 'error';
			$msg = $this->upload->display_errors('', '');
		}
		else
		{
			$data = $this->upload->data();			
			$image = $data['full_path'];
			$status = "success";
			//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
			$msg = $data['file_name'];
			//@unlink($image);				
			$fname  = $data['orig_name'];	

		}
		@unlink($_FILES[$file_element_name]);
	   
		echo json_encode(array('status' => $status, 'msg' => $msg,'originalfname'=>$fname));
	}
	function delete_image_file(){
		$fname = $this->input->post('filename');
		$productid = $this->input->post('productid');
		$status = '';
		$msg = '';
		$this->import('process/productprocess');
		$upload_path = $this->product_images_dir();	
		$process = new ProductProcess($this->Gateway);
		if(!empty($productid)){
			$product = $process->get_product_by_id($productid);			
			if($product==NULL || ($product!=NULL && $product->VendorID!=$this->session->userdata('userid')))
			{
				echo json_encode(array('status' => 'error', 'msg' => 'Product not found'));
				return;
			}else{
				$ret = $process->delete_product_image($product->ProductID,$fname);
				/*if(!$ret){
					echo json_encode(array('status' => 'error', 'msg' => 'error deleting file from db'));
					return;
				}*/
			}
		}
		$ret = $process->delete_file($fname,$this->product_images_dir());
		
		if($ret){
			if(!file_exists($upload_path.'/productimage.png')){
				copy( $this->config->item('vendor_product_images_dir').'/productimage.png',$upload_path.'/productimage.png');
			}
			$status = 'success';	
		}else{
			$status = 'error';
			$msg = $process->error_messages();
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
	function delete_video_file(){
		$fname = $this->input->post('filename');
		$productid = $this->input->post('productid');
		$status = '';
		$msg = '';
		$this->import('process/productprocess');
		$upload_path = $this->product_docs_dir();	
		$process = new ProductProcess($this->Gateway);
		if(!empty($productid)){
			$product = $process->get_product_by_id($productid);			
			if($product==NULL || ($product!=NULL && $product->VendorID!=$this->session->userdata('userid')))
			{
				echo json_encode(array('status' => 'error', 'msg' => 'Product not found'));
				return;
			}else{
				$ret = $process->delete_product_video($product->ProductID,$fname);
				/*if(!$ret){
					echo json_encode(array('status' => 'error', 'msg' => 'error deleting file from db'));
					return;
				}*/
			}
		}
		$ret = $process->delete_file($fname,$this->product_videos_dir());
		
		if($ret){
			$status = 'success';	
		}else{
			$status = 'error';
			$msg = $process->error_messages();
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
	function delete_doc_file(){
		$fname = $this->input->post('filename');
		$productid = $this->input->post('productid');
		$status = '';
		$msg = '';
		$this->import('process/productprocess');
		$upload_path = $this->product_docs_dir();	
		$process = new ProductProcess($this->Gateway);
		if(!empty($productid)){
			$product = $process->get_product_by_id($productid);			
			if($product==NULL || ($product!=NULL && $product->VendorID!=$this->session->userdata('userid')))
			{
				echo json_encode(array('status' => 'error', 'msg' => 'Product not found'));
				return;
			}else{
				$ret = $process->delete_product_doc($product->ProductID,$fname);
				/*if(!$ret){
					echo json_encode(array('status' => 'error', 'msg' => 'error deleting file from db'));
					return;
				}*/
			}
		}
		$ret = $process->delete_file($fname,$this->product_docs_dir());
		
		if($ret){
			$status = 'success';	
		}else{
			$status = 'error';
			$msg = $process->error_messages();
		}
		echo json_encode(array('status' => $status, 'msg' => $msg));
	}
}
