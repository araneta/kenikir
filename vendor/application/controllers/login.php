<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {  
	
	public function __construct() {
		parent::__construct();	         
		$this->set_lang_file('login');				
	}
	public function index() {		
		$this->check_remember_me();		
		
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'login';		
		$this->load->view('template',$this->data);		
	}
	private function set_identity($v){
		$this->session->set_userdata('userid',$v->VendorID);
		$this->session->set_userdata('username',$v->VendorID);
		$this->session->set_userdata('mediaid',$v->MediaID);
		$this->session->set_userdata('vendoralias',$v->Alias);
		$this->session->set_userdata('companyname',$v->CompanyName);
		$this->session->set_userdata('usertype','vendor');
	}
	private function delete_identity(){
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('mediaid');
		$this->session->unset_userdata('usertype');
		$this->session->unset_userdata('vendoralias');
		$this->session->unset_userdata('companyname');		
		$this->session->sess_destroy();
	}
	public function verify() 
	{
		$this->import('process/vendorprocess');
		$this->import('process/vendorremembermeprocess');
		$this->import('entities/vendorloginform');
		
		$form = new VendorLoginForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Please fill all fields.');
			redirect('login','refresh');	
		}else{					
			$process = new VendorProcess($this->Gateway);
			if($process->login($form)){
				$v = $process->vendor();
				$this->set_identity($v);
				
				//echo 'remember'.$form->rememberme;
				if($form->rememberme==1){
					$rememberprocess = new VendorRememberMeProcess($this->Gateway);
					$rememberprocess->remember($v->VendorID);
				}
				redirect('products','refresh');	
			}else
			{
				$this->session->set_flashdata('message','Invalid Username or Password.');
				redirect('login','refresh');	
			}	
		}
	}
	
	function check_remember_me(){
		$this->import('process/vendorremembermeprocess');
		$this->import('process/vendorprocess');
		
		$rememberprocess = new VendorRememberMeProcess($this->Gateway);
		$userid = $rememberprocess->check_remember_me();		
		if(!empty($userid)){		
			$vendorprocess = new VendorProcess($this->Gateway);
			$v = $vendorprocess->load_vendor($userid);
			if($v==NULL){
				die('vendor not found');
			}
			$this->set_identity($v);
			redirect('products','refresh');	
		}	
	}
	function logout(){		
		$this->import('process/vendorremembermeprocess');
		$rememberprocess = new VendorRememberMeProcess($this->Gateway);
		$userid = $rememberprocess->check_remember_me();		
		if(!empty($userid)){					
			$rememberprocess->forget_me($userid);
		}else
		{
			//die('usernotfound');	
		}
		$this->delete_identity();
		redirect('login','refresh');	
	}
}	
?>
