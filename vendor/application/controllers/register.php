<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        	
		$this->set_lang_file('register');
	}
	
	function index()
	{				 
		//$this->loadCaptcha();   	    	
		$this->data['title'] = $this->lang->line('signup');		
		$this->data['main'] = 'register/signup';
		
		$this->load->view('template',$this->data);
	}
	
	function create()
	{	
		//$this->loadCaptcha();				
		$this->import('process/vendorprocess');
		$this->import('entities/vendorregistrationform');
		
		$form = new VendorRegistrationForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Invalid email address. Please check the spelling and try again.');
			redirect('register','refresh');	
		}else{
						
			$process = new VendorProcess($this->Gateway);
			if($process->register($form)){
				$this->session->set_flashdata('email',$form->email);
				redirect('register/entercode?email='.$form->email,'refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register','refresh');	
			}	
		}
		
	}
	function entercode(){
		$this->data['title'] = 'Enter Verification Code';		
		$this->data['main'] = 'register/entercode';
		$this->data['email'] = filter_var($this->input->get('email'), FILTER_SANITIZE_STRING);
		
		$this->load->view('template',$this->data);
	}
	function verify(){
		$this->import('process/vendorprocess');
		$this->import('entities/vendorregistrationform');
		
		$form = new VendorVerificationCodeForm();
		if($this->input->post('email'))
			$this->bind($form,'POST');
		else	
			$this->bind($form,'GET');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Invalid Verification Code. Please check the spelling and try again.');
			//$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/entercode?email='.$form->email,'refresh');	
		}else{					
			$process = new VendorProcess($this->Gateway);
			if($process->check_activation_code($form)){
				$v = $process->vendor();
				$this->session->set_userdata('tempuserid',$v->VendorID);
				$this->session->set_userdata('mediaid',$v->MediaID);
				/*$this->session->set_userdata('userid',$v->VendorID);
				$this->session->set_userdata('username',$v->VendorID);
				$this->session->set_userdata('usertype','vendor');*/
				redirect('info/add_id','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				//$this->session->set_flashdata('message','Invalid Verification Code. Please check the spelling and try again.');
				redirect('register/entercode?email='.$form->email,'refresh');	
			}	
		}
	}
	function entercodeemail(){
		$this->data['title'] = 'Enter Verification Code';		
		$this->data['main'] = 'register/entercodeemail';
		$this->data['email'] = filter_var($this->input->get('email'), FILTER_SANITIZE_STRING);
		$this->load->view('template',$this->data);
	}
	function verify_new_email(){
		$this->import('process/vendorprocess');
		$this->import('entities/vendorregistrationform');
		
		$form = new VendorVerificationCodeForm();
		if($this->input->post('email'))
			$this->bind($form,'POST');
		else	
			$this->bind($form,'GET');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Invalid Verification Code. Please check the spelling and try again.');
			//$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/entercode?email='.$form->email,'refresh');	
		}else{					
			$process = new VendorProcess($this->Gateway);
			if($process->activate_new_email($form)){
				$v = $process->vendor();				
				$this->session->set_userdata('userid',$v->VendorID);
				$this->session->set_userdata('username',$v->VendorID);
				$this->session->set_userdata('usertype','vendor');				
				$this->session->set_flashdata('message','Info updated');	
				redirect('info','refresh');	
			}else
			{
				die('You have already verified your email address and/or this link is no longer valid.');
				$this->session->set_flashdata('message',$process->error_messages());
				//$this->session->set_flashdata('message','Invalid Verification Code. Please check the spelling and try again.');
				redirect('register/entercodeemail?email='.$form->email,'refresh');	
			}	
		}
	}
}
?>
