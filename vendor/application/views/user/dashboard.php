<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">	      
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/reset.css" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />
	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/form.js"></script>
	
	<title>GOVonomy</title>
</head>

<body>
	<div id="page-wrap">		
		<?php $this->load->view($main);?>
                       
		<?php $this->load->view('footer');?>
	</div>
</body>
</html>
