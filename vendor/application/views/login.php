	<div id="govonomygreybox">
			<?php $this->load->view('headerlogo');?>
			<div id="vendorloginform">
				<p class="title grey" style="margin-top:90px;">Login</p>
				<br />
				<div id="errmsg" class="red">
					<?php  
					if ($this->session->flashdata('message')){
						echo $this->session->flashdata('message');
					}    
					?>    
				</div>
				<?php echo form_open('login/verify',array('name'=>'vendorloginform')); ?>		
					<div class="col">
						<div class="formitem">
							<label>User ID</label>
							<input type="text" name="username" style="width:230px;height:24px;margin:0 auto;display:block;" class="required" />
						</div>
						<div class="clear"></div>
						<div class="formitem">
							<label>Password</label>
							<input type="password" name="password" style="width:230px;height:24px;margin:0 auto;display:block;" class="required" />
						</div>
						<div class="clear"></div>
						<br />
						<div class="colspan">
							<input type="checkbox" name="rememberme" id="rememberme" value="1" />
							<label style="font-weight:normal;display:inline;" class="grey" for="rememberme">Remember me on this computer</label>
						</div>
						<div class="clear"></div>
						<div id="errmsg" class="red"></div>
						
						<div class="formbutton" style="margin-top:20px;">							
							<input type="submit" class="btn" value="Login" />
							<a href="<?php echo base_url('forgot');?>" style="margin-left:10px" >Forgot Password?</a>						
						</div>
					</div>
					<div class="coldivider" style="height:200px;"></div>
					<div class="col">
						<div style="margin:50px 20px">
							<p class="subtitle grey">No account yet?</p>			
							<br />		
							<input type="button" class="btn" onclick="window.location='register'" value="Become a Vendor" />
						</div>
					</div>	
				<?php echo form_close();?>
				<br /><br />
				<div class="clear"></div>
			</div>		
			
		</div>

<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="vendorloginform"]',function(){
			$('#errmsg').html('Please fill all fields.');	
		});
	});
	
</script>
