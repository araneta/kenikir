	<div id="govonomygreybox">
			<?php $this->load->view('headerlogo');?>
			<div id="vendorloginform">
				<p class="title grey" style="margin-top:90px;">Forgot Password</p>
				<br />
				<div id="errmsg" class="red">
					<?php  
					if ($this->session->flashdata('message')){
						echo $this->session->flashdata('message');
					}    
					?>    
				</div>
				<?php echo form_open('forgot/verify_email',array('name'=>'vendorloginform')); ?>		
					<div class="col">
						<div class="formitem">
							<label>Primary Email</label>
							<input type="text" name="PrimaryEmail" style="width:230px;height:24px;margin:0 auto;display:block;" class="required" />
						</div>
						<div class="clear"></div>						
						<div id="errmsg" class="red"></div>
						
						<div class="formbutton" style="margin-top:20px;">							
							<input type="submit" class="btn" value="Next" />							
						</div>
					</div>						
				<?php echo form_close();?>
				<br /><br />
				<div class="clear"></div>
			</div>		
			
		</div>

<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="vendorloginform"]',function(){
			$('#errmsg').html('Please fill all fields.');	
		});
	});
	
</script>
