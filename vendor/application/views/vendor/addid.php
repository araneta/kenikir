<div id="govonomygreybox">
	<p align="center"><img src="<?php echo base_url();?>images/logo.png" /></p>
	<p align="center" id="federal">	
		<b>Federal – State – Local – Education</b>
	</p>
	<br /><br />
	<div id="newwvendorform">			
			<?php echo form_open('info/add_id_save',array('name'=>'vendorinfoform')); ?>									
			<div class="colspan">
			<p align="center" class="title">Enter User ID</p>			
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){							
							echo '<br />'.$this->session->flashdata('message');
						}    
						?></div>
				<div class="formlegend" style="float:none;width:500px;margin-left:auto;margin-right:auto;">
					<label class="legend">Please enter a User ID that that is 7 or more characters, using letter and numbers only.</label>
				</div>
				<div class="formitem" style="float:none;width:200px;margin-left:auto;margin-right:auto;">					
					<input type="text" name="VendorID" class="required" />
				</div>
				
				<div class="formbutton">					
					<input type="submit" class="btn center" value="Next" />
					
				</div>				
			</div>	
			<?php echo form_close();?>
		
	</div>				
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="vendorinfoform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
		
	});
	
</script>
