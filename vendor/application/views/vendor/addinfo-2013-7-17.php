<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('vendor/leftmenu'); ?>
		<div id="vendorinfoform">
			<p class="title">Enter Vendor Information</p>
			<?php echo form_open('info/add_save',array('name'=>'vendorinfoform')); ?>									
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo $this->session->flashdata('message');
						}    
						?></div>
				<div class="formlegend">
					<label class="legend">Security Information</label>
				</div>
				
				<div class="formitem" style="width:100%">
					<label>Company Name</label>
					<input type="text" name="CompanyName" class="required" />
				</div>
				<div class="formitem" style="width:100%">
					<label>Public Info</label>
					<textarea name="PublicInfo" style="width:96%"></textarea>
				</div>
				<div class="formitem" style="width:100%">
					<label>Upload a Photo</label>
					<div id="imgarea">
						<div class="vendimgcont"></div>
						<div class="formupload">
							<input type="button" class="btnUpload btn" value="Browse" />
							<input type="file" onchange="uploadimg(this)" id="img1" name="imagefile"  class="img" style="display:none">																 
							<input type="text" name="ImagePath" id="ImagePath" class="required" value="<?php echo $vendor->ImagePath;?>" />					
						</div>	
					</div>	
					
				</div>
				<div class="formitem">
					<label>Password</label>
					<input type="password" name="Password_1" class="required" />
				</div>
				<div class="formitem">
					<label>Confirm Password</label>
					<input type="password" name="Password_2" class="required" />
				</div>
				<div class="formitem">
					<label>Security Question 1</label>
					<input type="text" name="Question1" class="required" value="<?php echo $vendor->Question1; ?>" /> 
				</div>
				<div class="formitem">
					<label>Answer 1</label>
					<input type="text" name="Answer1" class="required" value="<?php echo $vendor->Answer1; ?>" />
				</div>
				<div class="formitem">
					<label>Security Question 2</label>
					<input type="text" name="Question2" class="required" value="<?php echo $vendor->Question2; ?>" />
				</div>
				<div class="formitem">
					<label>Answer 2</label>
					<input type="text" name="Answer2" class="required" value="<?php echo $vendor->Answer2; ?>" />
				</div>
				
				<div class="formlegend">
					<label>Personal Information</label>
				</div>
				<div class="formitem">
					<label>First Name</label>
					<input type="text" name="FirstName" class="required" value="<?php echo $vendor->FirstName ?>" />
				</div>
				<div class="formitem">
					<label>Last Name</label>
					<input type="text" name="LastName" class="required" value="<?php echo $vendor->LastName; ?>" />
				</div>
				<div class="formitem">
					<label>Phone</label>
					<input type="text" name="Phone" class="required" value="<?php echo $vendor->Phone; ?>" />
				</div>
				<div class="formitem">
					<label>Phone 2</label>
					<input type="text" name="Phone2" class="required" value="<?php echo $vendor->Phone2; ?>" />
				</div>
				<div class="formitem">
					<label>Email</label>
					<input type="text" name="Email" class="email required" value="<?php echo $vendor->Email; ?>" />
				</div>
				<div class="formitem">
					<label>Email 2</label>
					<input type="text" name="Email2" class="email required" value="<?php echo $vendor->Email2; ?>" />
				</div>
				<div class="formitem full">
					<label style="float:left">Do you want to receive GOVonomy Newsletters?</label>
					<div class="formradio"><input type="radio" name="SubscribeNewsletter" <?php if($vendor->SubscribeNewsletter==1) echo 'checked="checked"';?> value="1" class="required" id="newsletteryes"/><label for="newsletteryes">Yes</label></div>
					<div class="formradio"><input type="radio" name="SubscribeNewsletter" <?php if($vendor->SubscribeNewsletter==0) echo 'checked="checked"';?> value="0" class="required" /><label id="newsletterno">No</label></div>
				</div>
				<div class="formitem">
					<label>URL</label>
					<input type="text" name="URL" class="required" value="<?php echo $vendor->URL; ?>" />
				</div>
				<div class="formitem">
					<label>URL 2</label>
					<input type="text" name="URL2" class="required" value="<?php echo $vendor->URL2; ?>" />
				</div>
				<div class="formitem">
					<label>Address</label>
					<input type="text" name="Address" class="required" value="<?php echo $vendor->Address; ?>" />
				</div>
				<div class="formitem">
					<label>City</label>
					<input type="text" name="City" class="required" value="<?php echo $vendor->City; ?>" />
				</div>
				<div class="formitem">
					<label>State</label>
					<input type="text" name="State" class="required" value="<?php echo $vendor->State; ?>" />
				</div>
				<div class="formitem">
					<label>Zip</label>
					<input type="text" name="Zip" class="required" value="<?php echo $vendor->Zip; ?>" />
				</div>
				<div class="formitem">
					<label>Address 2</label>
					<input type="text" name="Address2" class="required" value="<?php echo $vendor->Address2; ?>" />
				</div>
				<div class="formitem">
					<label>City 2</label>
					<input type="text" name="City2" class="required" value="<?php echo $vendor->City2; ?>" />
				</div>
				<div class="formitem">
					<label>State 2</label>
					<input type="text" name="State2" class="required" value="<?php echo $vendor->State2; ?>" />
				</div>
				<div class="formitem">
					<label>Zip 2</label>
					<input type="text" name="Zip2" class="required" value="<?php echo $vendor->Zip2; ?>" />
				</div>
				<div class="formbutton">
					<div class="col">
						<input type="submit" class="btn center" value="Save" />
					</div>
					<div class="col">
						<input type="button" class="btn center" value="Cancel" />
					</div>
					
				</div>
				<input type="hidden" name="OldVendorID" value="<?php echo $vendor->VendorID; ?>" >
			<?php echo form_close();?>
		</div>
	</div><!--end innerwrap-->					
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="vendorinfoform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
		if($('input[name="ImagePath"]').val()!=''){			
			$('.vendimgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url');?>/'+$('input[name="ImagePath"]').val()+'" />');						   		
		}else{
			$('.vendimgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url');?>/anonymouse.png" />');						   		
		}	
		$('.btnUpload').click(function(e){			
			$(e.target).parent().find('input[type="file"]').click();
		});
	});
	function uploadimg(e){
		var caller = $(e);
		var idx = caller.attr('id');			
		var statusbox = caller.parent().find('#ImagePath');	
		statusbox.val('Uploading...');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('info/upload_image_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);
				   $('.vendimgcont').html('<img style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url');?>/'+data.msg+'">');
				}else{
					alert(data.msg);
					statusbox.val('');
				}
			 },
			 error:function(){
				 
			 }
		  });	
	}
</script>
