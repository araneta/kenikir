<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('vendor/leftmenu'); ?>
		<div id="vendorinfoform">
			<p class="title">View Vendor Information</p>
			
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo $this->session->flashdata('message');
						}    
						?></div>
				<br />		
				<a target="_blank" href="<?php echo $this->config->item('main_site').'v/'.$this->session->userdata('vendoralias');?>">View Public Vendor Profile</a>
				<br />
				<table class="tblinfo">
					<tr>
						<td width="200" class="infotblcaption">Vendor ID</td>
						<td  width="200" ><?php echo $vendor->VendorID; ?></td>
					</tr>	
					<tr>
						<td class="infotblcaption">Company Name</td>
						<td><?php echo $vendor->CompanyName; ?></td>
					</tr>					
					<tr>
						<td class="infotblcaption">Public Info</td>
						<td><?php echo $vendor->PublicInfo; ?></td>
					</tr>	
					<tr>
						<td class="infotblcaption">First Name</td>
						<td><?php echo $vendor->FirstName; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Last Name</td>
						<td><?php echo $vendor->LastName; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Phone</td>
						<td><?php echo $vendor->Phone; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Phone 2</td>
						<td><?php echo $vendor->Phone2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Email</td>
						<td><?php echo $vendor->Email; ?></td>
					</tr>
					<tr> 
						<td class="infotblcaption">Email 2</td>
						<td><?php echo $vendor->Email2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Receive Newsletter?</td>
						<td><?php if($vendor->SubscribeNewsletter==1){echo 'Yes';}else{echo 'No';} ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">URL</td>
						<td><?php echo $vendor->URL; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">URL 2</td>
						<td><?php echo $vendor->URL2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Address</td>
						<td><?php echo $vendor->Address; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">City</td>
						<td><?php echo $vendor->City; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">State</td>
						<td><?php echo $vendor->State; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Zip</td>
						<td><?php echo $vendor->Zip; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Address 2</td>
						<td><?php echo $vendor->Address2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">City 2</td>
						<td><?php echo $vendor->City2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">State 2</td>
						<td><?php echo $vendor->State2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Zip 2</td>
						<td><?php echo $vendor->Zip2; ?></td>
					</tr>
				</table>		
			
		</div>
	</div><!--end innerwrap-->					
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="vendorinfoform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
	});
	
</script>
