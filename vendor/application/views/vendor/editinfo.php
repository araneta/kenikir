<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('vendor/leftmenu'); ?>
		<div id="vendorinfoform">
			<p class="title">Edit Vendor Information</p>
			<?php echo form_open('info/edit_save',array('name'=>'vendorinfoform')); ?>									
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo $this->session->flashdata('message');
						}    
						?></div>
				<br /><br />
				<div class="formitem" style="width:200px;float:right">
					<a href="<?php echo base_url('info/edit_pass');?>"></a>
				</div>
				<div class="formitem">
					<label style="float:left">Vendor ID</label>
					<label style="float:left;margin:4px 20px 0;"><?php echo $vendor->VendorID; ?></label>
				</div>				
				<div class="formitem">
					<a style="float:left;margin:4px 0px;" href="<?php echo base_url('info/edit_pass');?>">Edit Password &amp; Security Questions</a>
				</div>				
				<div class="clear"></div>
				<br />				
				<div class="formitem" style="width:100%">
					<label>Company Name</label>
					<input type="text" style="width:96%" name="CompanyName" class="required" value="<?php echo $vendor->CompanyName; ?>" />
				</div>
				<div class="formitem" style="width:100%">
					<label>Vendor Info / Description  <br/><span style="font-weight:normal">(Please enter relevant information, users will be able to search based on this information)</span></label>
					<textarea name="PublicInfo" style="width:96%"><?php echo $vendor->PublicInfo; ?></textarea>
				</div>
				<div class="formitem" style="width:100%">
					<label>Upload a Photo</label>
					<div id="imgarea">
						<div class="vendimgcont"></div>
						<div class="formupload">
							<input type="button" class="btnUpload btn" value="Browse" />
							<a href="#" class="lnkDelete">Delete</a>
							<input type="file" onchange="uploadimg(this)" id="img1" name="imagefile"  class="img" style="display:none">																 
							<input type="text" name="ImagePath" id="ImagePath" class="required" value="<?php echo $vendor->ImagePath;?>" />					
						</div>	
					</div>	
					
				</div>
				<div class="formitem">
					<label>First Name</label>
					<input type="text" name="FirstName" class="required" value="<?php echo $vendor->FirstName ?>" />
				</div>
				<div class="formitem">
					<label>Last Name</label>
					<input type="text" name="LastName" class="required" value="<?php echo $vendor->LastName; ?>" />
				</div>
				<div class="formitem">
					<label>Phone</label>
					<input type="text" name="Phone" class="required" value="<?php echo $vendor->Phone; ?>" />
				</div>
				<div class="formitem">
					<label>Phone 2</label>
					<input type="text" name="Phone2" class="" value="<?php echo $vendor->Phone2; ?>" />
				</div>
				<div class="formitem">
					<label>Email</label>
					<input type="text" name="Email" class="email required notequal" value="<?php echo $vendor->Email; ?>" />
				</div>
				<div class="formitem">
					<label>Email 2</label>
					<input type="text" name="Email2" class="email" value="<?php echo $vendor->Email2; ?>" />
				</div>
				<div class="formitem full">
					<label style="float:left">Do you want to receive GOVonomy Newsletters?</label>
					<div class="formradio"><input type="radio" name="SubscribeNewsletter" <?php if($vendor->SubscribeNewsletter==1) echo 'checked="checked"';?> value="1" class="required" id="newsletteryes"/><label for="newsletteryes">Yes</label></div>
					<div class="formradio"><input type="radio" name="SubscribeNewsletter" <?php if($vendor->SubscribeNewsletter==0) echo 'checked="checked"';?> value="0" class="required" /><label id="newsletterno">No</label></div>
				</div>
				<div class="formitem">
					<label>URL</label>
					<input type="text" name="URL" class="required" value="<?php echo $vendor->URL; ?>" />
				</div>
				<div class="formitem">
					<label>URL 2</label>
					<input type="text" name="URL2" class="" value="<?php echo $vendor->URL2; ?>" />
				</div>
				<div class="formitem">
					<label>Address</label>
					<input type="text" name="Address" class="required" value="<?php echo $vendor->Address; ?>" />
				</div>
				<div class="formitem">
					<label>City</label>
					<input type="text" name="City" class="required" value="<?php echo $vendor->City; ?>" />
				</div>
				<div class="formitem">
					<label>State</label>
					<input type="text" name="State" class="required" value="<?php echo $vendor->State; ?>" />
				</div>
				<div class="formitem">
					<label>Zip</label>
					<input type="text" name="Zip" class="required" value="<?php echo $vendor->Zip; ?>" />
				</div>
				<div class="formitem">
					<label>Address 2</label>
					<input type="text" name="Address2" class="" value="<?php echo $vendor->Address2; ?>" />
				</div>
				<div class="formitem">
					<label>City 2</label>
					<input type="text" name="City2" class="" value="<?php echo $vendor->City2; ?>" />
				</div>
				<div class="formitem">
					<label>State 2</label>
					<input type="text" name="State2" class="" value="<?php echo $vendor->State2; ?>" />
				</div>
				<div class="formitem">
					<label>Zip 2</label>
					<input type="text" name="Zip2" class="" value="<?php echo $vendor->Zip2; ?>" />
				</div>
				<br />
				<div class="formitem" style="width:100%">
					<label>Videos <span style="font-weight:normal">(acceptable extensions: .swf, .mp4, .avi, .mov, .flv; max. size allowed: <?php echo $this->config->item('video_max_size')/1024;?> MB)</span></label>
					<legend>
						<div class="fromgrpitem" id="vendorvideos">
							<?php
							$maxvid = 2;
							for($vid=0;$vid<$maxvid;$vid++){
							?>
								<div class="formupload">
									<label class="vidlabel">Title:</label>
									<input type="text" name="VideoTitle[]" class="vidtitle" value="<?php if(!empty($vendor->VideoTitle[$vid])){echo $vendor->VideoTitle[$vid];} ?>" />
									<div class="clear"></div>									
									<input type="button" class="btnUpload btn" value="Browse" />
									<a href="#" class="lnkDelete">Delete</a>
									<input type="file" onchange="uploadvid(this)" id="vid<?php echo $vid+1;?>" name="videofile" class="vid" style="display:none">																 
									<input type="text" readonly="readonly" name="VideoFileName[]" value="<?php if(empty($vendor->VideoFileName[$vid])){echo '';}else{echo $vendor->VideoFileName[$vid];} ?>" class="vidfname" />					
								</div>
							<?php 							
								if($vid<($maxvid-1))
									echo '<hr />';
														
							}
							?>
						</div>
					</legend>
				</div>		
				<br />	
				<div class="formitem" style="width:100%">					
					<label>Additional Product Info <br /><span style="font-weight:normal">(acceptable extensions: .pdf, .txt, .doc, .ppt, .rtf; max. size allowed: <?php echo $this->config->item('doc_max_size')/1024;?> MB)</span></label>
					<legend>
						<div class="fromgrpitem" id="vendorfiles">
							<?php
							$maxdoc= 10;
							for($doc=0;$doc<$maxdoc;$doc++){
							?>
							<div class="formupload upload<?php echo $doc;?>">
								<label class="prodlabel">Title:</label>
								<input type="text" name="DocTitle[]" class="doctitle" value="<?php if(!empty($vendor->DocTitle[$doc])){echo $vendor->DocTitle[$doc];} ?>" />
								<div class="clear"></div>
								<input type="button" class="btnUpload btn" value="Browse" />
								<a href="#" class="lnkDelete">Delete</a>
								<input type="file" onchange="uploaddoc(this)" id="doc<?php echo $doc+1;?>" name="docfile" class="doc" style="display:none">																 
								<input type="text" readonly="readonly" name="DocFileName[]" value="<?php if(empty($vendor->DocFileName[$doc])){echo '';}else{echo $vendor->DocFileName[$doc];} ?>" class="docfname" />					
							</div>
							<?php
								if($doc<($maxdoc-1))
									echo '<hr class="hrupload'.$doc.'"/>';
							}
							?>		
							<a id="adddoc" href="#" onclick="return addformupload();">Add More</a>											
						</div>
					</legend>
				</div>
				
				<div id="errmsg2" class="red" style="margin-top:20px;float:left"></div>
				<div class="formbutton">
					<div class="col">
						<input type="submit" class="btn center" value="Save" />
					</div>
					<div class="col">
						<input type="button" class="btn center" value="Cancel" onclick="window.location='<?php echo base_url('info');?>'"/>
					</div>
					
				</div>
				<input type="hidden" name="VendorID" value="<?php echo $vendor->VendorID; ?>" >
			<?php echo form_close();?>
		</div>
	</div><!--end innerwrap-->					
	<div class="clear"></div>
</div>
<script type="text/javascript">
	var iupload = 0;
	
	jQuery(document).ready(function($){ 
		$('form[name="vendorinfoform"]').submit(function(){			
			var valid = validateForm2('form[name="vendorinfoform"]');			
			if(!valid){
				$('#errmsg2').html('Please enter missing fields');
				return false;
			}
			if($('input[name="Email"]').val()==$('input[name="Email2"]').val()){				
				$('#errmsg2').html('Invalid Email 2');
				$('input[name="Email2"]').addClass('error').removeClass('valid');
				return false;
			}
			var msg = null;
			$('.formupload').each(function(k,v){
				var fname = $(v).find('.vidfname,.docfname');
				var titlebox = $(v).find('.vidtitle,.doctitle');
				if(!fname.is(':disabled')){
					//user uploaded a file
					if(fname.val()!=''){						
						if(titlebox.val()==''){
							msg = 'Please enter a title to go with the uploaded video/file.';
							titlebox.addClass('error').removeClass('valid');
						}
					}else{
						//user only enter a title
						if(titlebox.val()!=''){							
							msg = 'Please upload a video/file to go with the entered title.';
							titlebox.addClass('error').removeClass('valid');
						}
					}					
				}else{					
					msg = 'Please wait until your video/file is finished uploading.';
					titlebox.addClass('error').removeClass('valid');
				}				
			});
			if(msg!=null){
				$('#errmsg2').html(msg);
				return false;
			}
			return true;
		});
		
		if($('input[name="ImagePath"]').val()!=''){			
			$('.vendimgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url').'/'.$this->session->userdata('mediaid');?>/'+$('input[name="ImagePath"]').val()+'" />');						   		
		}else{
			$('.vendimgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url').'/'.$this->session->userdata('mediaid');?>/anonymouse.png" />');						   		
		}	
		$('.btnUpload').click(function(e){			
			$(e.target).parent().find('input[type="file"]').click();
		});
		//show only 2 doc upload form
		for(iupload=2;iupload<10;iupload++){
			$('.upload'+iupload).hide();
			$('.hrupload'+(iupload-1)).hide();
		}
		iupload=2;
		$('#imgarea .lnkDelete').click(function(e){	
			e.preventDefault();		
			var fnamebox = $('#ImagePath');			
			if(fnamebox.val()=='')return;
						
			$(e.target).html('Deleting...');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('info/delete_image_file');?>",
				data: { 
					<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
					filename: fnamebox.val()					
				},
				success: function (data, status,x){
					var y = jQuery.parseJSON(data);
					if(y.status != 'error')
					{					   			  
					   fnamebox.val('anonymouse.png');
					   $('.vendimgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url').'/'.$this->session->userdata('mediaid');?>/anonymouse.png" />');						   		
					}else{										
						alert(y.msg);					
					}
				}
			}).done(function( data ) {
				$(e.target).html('Delete');	
			});
		});
		$('#vendorvideos .lnkDelete').click(function(e){	
			e.preventDefault();		
			var fnamebox = $(e.target).parent().find('.vidfname');	
			var titlebox = $(e.target).parent().find('.vidtitle');			
			if(fnamebox.val()=='')return;
			
			var prodid = $('#ProductID').val();
			$(e.target).html('Deleting...');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('info/delete_video_file');?>",
				data: { 
					<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
					filename: fnamebox.val(),
					productid:prodid 
				},
				success: function (data, status,x){
					var y = jQuery.parseJSON(data);
					if(y.status != 'error')
					{					   			  
					   fnamebox.val('');
					   titlebox.val('');
					}else{										
						alert(y.msg);					
					}
				}
			}).done(function( data ) {
				$(e.target).html('Delete');	
			});
		});
		$('#vendorfiles .lnkDelete').click(function(e){	
			e.preventDefault();		
			var fnamebox = $(e.target).parent().find('.docfname');			
			var titlebox = $(e.target).parent().find('.doctitle');	
			if(fnamebox.val()=='')return;
			
			var prodid = $('#ProductID').val();
			$(e.target).html('Deleting...');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('info/delete_doc_file');?>",
				data: { 
					<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
					filename: fnamebox.val(),
					productid:prodid 
				},
				success: function (data, status,x){
					var y = jQuery.parseJSON(data);
					if(y.status != 'error')
					{					   			  
					   fnamebox.val('');
					   titlebox.val('');
					}else{										
						alert(y.msg);					
					}
				}
			}).done(function( data ) {
				$(e.target).html('Delete');	
			});
		});
	});
	function uploadimg(e){
		var caller = $(e);
		var idx = caller.attr('id');			
		var statusbox = caller.parent().find('#ImagePath');	
		statusbox.val('Uploading...');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('info/upload_image_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);
				   $('.vendimgcont').html('<img style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_images_url').'/'.$this->session->userdata('mediaid');?>/'+data.msg+'">');
				}else{
					alert(data.msg);
					statusbox.val('');
				}
			 },
			 error:function(){
				 
			 }
		  });	
	}
	function uploadvid(e){
		
		if((e.files[0].size/1024)><?php echo $this->config->item('video_max_size');?>){
			alert($(e).val()+' could not be uploaded because the file size is too big');
			return;
		}
		
		var caller = $(e);
		var idx = caller.attr('id');
		var statusbox = caller.parent().find('.vidfname');	
		var titlebox = caller.parent().find('.vidtitle');
		statusbox.val('Uploading...');
		statusbox.attr('disabled','disabled');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('info/upload_video_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
				statusbox.removeAttr('disabled');
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);
				   if(titlebox.val()=='')
						titlebox.val('Untitled');
				}else{
					alert(data.msg);
					statusbox.val('');
				}
				caller.replaceWith('<input type="file" id="vid1" name="videofile" class="vid" style="display:none">');
			 },
			 error:function(){
				 
			 }
		  });	
	}
	function uploaddoc(e){
		if((e.files[0].size/1024)><?php echo $this->config->item('doc_max_size');?>){
			alert($(e).val()+' could not be uploaded because the file size is too big');
			return;
		}
		var caller = $(e);
		var idx = caller.attr('id');
		var statusbox = caller.parent().find('.docfname');	
		var titlebox = caller.parent().find('.doctitle');
		statusbox.val('Uploading...');
		statusbox.attr('disabled','disabled');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('info/upload_doc_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
			    statusbox.removeAttr('disabled');
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);				   
					if(titlebox.val()=='')
						titlebox.val('Untitled');
				}else{										
					alert(data.msg);
					statusbox.val('');
				}
				
			 },
			 error:function(){
				 
			 }
		  });			
				
	}
	function addformupload(){
		//show only 2 doc upload form		
		var n =(iupload+2);
		for(;iupload<n;iupload++){		
			$('.upload'+iupload).show();
			$('.hrupload'+(iupload-1)).show();
		}
		if(iupload==10)
			$('#adddoc').hide();
		return false;
	}
</script>
