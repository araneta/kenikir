<div id="govonomygreybox">
	<p align="center"><img src="<?php echo base_url();?>images/logo.png" /></p>
	<p align="center" id="federal">	
		<b>Federal – State – Local – Education</b>
	</p>
	<br /><br />
	<div id="newwvendorform">	
		<?php echo form_open('info/add_pass_save',array('name'=>'vendorinfoform')); ?>									
			<div style="width:400px;margin-left:auto;margin-right:auto">
				<p class="title">Enter Security Information</p>
				
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo '<br />'.$this->session->flashdata('message');
						}    
						?></div>
				<br />		
				<div class="formitem">
					<label>Password</label>
					<input type="password" id="Password_1" name="Password_1" class="required" />
				</div>
				<div class="formitem">
					<label>Confirm Password</label>
					<input type="password" id="Password_2" name="Password_2" class="required" />
				</div>
				<div class="formitem">
					<label>Security Question 1</label>
					<input type="text" name="Question1" class="required" value="<?php echo $vendor->Question1; ?>" /> 
				</div>
				<div class="formitem">
					<label>Answer 1</label>
					<input type="text" name="Answer1" class="required" value="<?php echo $vendor->Answer1; ?>" />
				</div>
				<div class="formitem">
					<label>Security Question 2</label>
					<input type="text" name="Question2" class="required" value="<?php echo $vendor->Question2; ?>" />
				</div>
				<div class="formitem">
					<label>Answer 2</label>
					<input type="text" name="Answer2" class="required" value="<?php echo $vendor->Answer2; ?>" />
				</div>
				
				<div class="formbutton">					
					<input type="submit" class="btn center" value="Next" />
					
				</div>				
			</div>	
		<?php echo form_close();?>		
	</div><!--end innerwrap-->					
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="vendorinfoform"]',function(){
			var msg = 'Please enter missing fields';			
			var pass1 = jQuery.trim(""+$('#Password_1').val());
			var pass2 = jQuery.trim(""+$('#Password_2').val());
			if(pass2=='' && pass1!=''){
				msg = 'Please re-enter password to update your password.';
			}else if(pass2!='' && pass1!='' && pass2!=pass1){
				msg = 'Passwords do not match.';			
			}
			$('#errmsg').html('<br />'+msg);	
		});
		
	});
	
</script>
