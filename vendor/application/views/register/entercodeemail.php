<div id="govonomygreybox">
	<p align="center"><img src="<?php echo base_url();?>images/logo.png" /></p>
	<p align="center" id="federal">	
		<b>Federal – State – Local – Education</b>
	</p>
	
	<div id="newwvendorform">
		<p align="center" class="subtitle grey" style="margin-top:100px;">Please check your email &lt;<?php echo $email; ?>&gt; <br />and enter the verification code<br /> or click on the verification link in the email.</p>
		<br />
		<?php echo form_open('register/verify_new_email',array('name'=>'newvendorform')); ?>									
			<div class="colspan">
				<?php echo form_input('code',set_value('code'),'style="width:230px;height:24px;margin:0 auto;display:block;" class="required"');?>
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo $this->session->flashdata('message');
						}    
						?></div>
			</div>	
			<div class="formbutton">							
				<div class="colspan">
					<input type="submit" class="btn center" value="Verify" />
					<br /><br />
					<p align="center"><a href="#">Need Help?</a></p>
				</div>
			</div>
			<input type="hidden" value="<?php echo $email; ?>" name="email" />
		<?php echo form_close();?>
	</div>		
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newvendorform"]',function(){
			$('#errmsg').html('<br />Invalid email address. Please check the spelling and try again.');	
		});
	});
	
</script>
