
	<div id="govonomygreybox">
		<p align="center"><img src="<?php echo base_url();?>images/logo.png" /></p>
		<p align="center" id="federal">	
			<b>Federal – State – Local – Education</b>
		</p>
		
		<div id="newwvendorform">
			<p align="center" class="title grey" style="margin-top:90px;">Thank you for your interest <br />in becoming a Vendor at GOVonomy.com</p>
			<br />
			<p align="center" class="grey subtitle" style="margin-top:50px;">Please enter your email address below to get started.</p>
			<br />
			<?php echo form_open('register/create',array('name'=>'newvendorform')); ?>												
				<div class="colspan">
					<div class="formitem" style="width:100%;float:none;">
						<?php echo form_input('email',set_value('email'),'style="width:230px;height:24px;margin:0 auto;display:block;" class="required email" '); ?>
					</div>
					<div id="errmsg" class="red">
						<?php  
						if ($this->session->flashdata('message')){
							echo '<br />';
							echo $this->session->flashdata('message');
						}    
						?>    
					</div>
				</div>	
				<div class="formbutton">							
					<div class="colspan">
						<input type="submit" class="btn center" value="Become a Vendor" />
						<br /><br />
						<p align="center"><a href="#">Need Help?</a></p>
					</div>
				</div>
			<?php echo form_close();?>
		</div>		
		<div class="clear"></div>
	</div>

<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newvendorform"]',function(){
			$('#errmsg').html('<br />Invalid email address. Please check the spelling and try again.');	
		});
	});
	
</script>
