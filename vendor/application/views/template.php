<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">	      
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/reset.css" />	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css" />
	<?php
	if (isset($cssfiles) && count($cssfiles)){		
		foreach ($cssfiles as $css){
			echo "<link href=\"". base_url(). "css/$css\" rel=\"stylesheet\" type=\"text/css\" />\r\n";			
		}		
	} 
	?>          

	<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.8.3.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/form.js"></script>
	<?php
	if (isset($jsfiles) && count($jsfiles)){		
		foreach ($jsfiles as $js){
			echo "<script type=\"text/javascript\" src=\"". base_url(). "js/$js\" ></script>\r\n";			
		}		
	} 
	?>          

	<title>GOVonomy</title>
</head>

<body>
	<div id="page-wrap">		
		<?php $this->load->view($main);?>
                       
		<?php $this->load->view('footer');?>
	</div>
</body>
</html>
