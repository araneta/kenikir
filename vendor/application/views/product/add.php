<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('vendor/leftmenu'); ?>
		<div id="addproductform">
			<p class="title"><?php echo $title;?></p>
			<br />
			<?php echo form_open('products/save',array('name'=>'addproductform','id'=>'addproductform','enctype'=>'multipart/form-data')); ?>									
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo $this->session->flashdata('message');
						}    
						?></div>
				
				<div class="formitem">
					<label>Product Name</label>
					<input type="text" name="Name" class="required" value="<?php echo $product->Name;?>" />
				</div>	
				<div class="formitem">
					<label>Upload a Photo</label>
					<div id="imgarea">
						<div id="imgcont"></div>
						<div class="formupload">
							<input type="button" class="btnUpload btn" value="Browse" />
							<a href="#" class="lnkDelete">Delete</a>
							<input type="file" onchange="uploadimg(this)" id="img1" name="imagefile"  class="img" style="display:none">																 
							<input type="text" readonly="readonly" name="ProductImage" id="ProductImage" class="required" value="<?php echo $product->ProductImage;?>" />					
						</div>	
					</div>	
					
				</div>
				<div class="formitem">
					<label>Product Info <br /><span style="font-weight:normal">(Please enter relevant information, users will be able to search based on this information)</span></label>
					<textarea id="ProductInfo" name="ProductInfo" onblur="checkValidHtml();"><?php echo $product->ProductInfo;?></textarea>					
				</div>								
				<div class="formitem">
					<label>Videos <span style="font-weight:normal">(acceptable extensions: .swf, .mp4, .avi, .mov, .flv; max. size allowed: <?php echo $this->config->item('video_max_size')/1024;?> MB)</span></label>
					<legend>
						<div class="fromgrpitem" id="productvideos">
							<?php
							$maxvid = 2;
							for($vid=0;$vid<$maxvid;$vid++){
							?>
								<div class="formupload">
									<label class="vidlabel">Title:</label>
									<input type="text" name="VideoTitle[]" class="vidtitle" value="<?php if(!empty($product->VideoTitle[$vid])){echo $product->VideoTitle[$vid];} ?>" />
									<div class="clear"></div>									
									<input type="button" class="btnUpload btn" value="Browse" />
									<a href="#" class="lnkDelete">Delete</a>
									<input type="file" onchange="uploadvid(this)" id="vid<?php echo $vid+1;?>" name="videofile" class="vid" style="display:none">																 
									<input type="text" readonly="readonly" name="VideoFileName[]" value="<?php if(empty($product->VideoFileName[$vid])){echo '';}else{echo $product->VideoFileName[$vid];} ?>" class="vidfname" />					
								</div>
							<?php 							
								if($vid<($maxvid-1))
									echo '<hr />';
														
							}
							?>
						</div>
					</legend>
				</div>			
				<div class="formitem">					
					<label>Additional Product Info <br /><span style="font-weight:normal">(acceptable extensions: .pdf, .txt, .doc, .ppt, .rtf; max. size allowed: <?php echo $this->config->item('doc_max_size')/1024;?> MB)</span></label>
					<legend>
						<div class="fromgrpitem" id="productfiles">
							<?php
							$maxdoc= 10;
							for($doc=0;$doc<$maxdoc;$doc++){
							?>
							<div class="formupload upload<?php echo $doc;?>">
								<label class="prodlabel">Title:</label>
								<input type="text" name="DocTitle[]" class="doctitle" value="<?php if(!empty($product->DocTitle[$doc])){echo $product->DocTitle[$doc];} ?>" />
								<div class="clear"></div>
								<input type="button" class="btnUpload btn" value="Browse" />
								<a href="#" class="lnkDelete">Delete</a>
								<input type="file" onchange="uploaddoc(this)" id="doc<?php echo $doc+1;?>" name="docfile" class="doc" style="display:none">																 
								<input type="text" readonly="readonly" name="DocFileName[]" value="<?php if(empty($product->DocFileName[$doc])){echo '';}else{echo $product->DocFileName[$doc];} ?>" class="docfname" />					
							</div>
							<?php
								if($doc<($maxdoc-1))
									echo '<hr class="hrupload'.$doc.'"/>';
							}
							?>	
							<a id="adddoc" href="#" onclick="return addformupload();">Add More</a>						
						</div>
					</legend>
				</div>							
				<div class="formitem">
					<div class="col">
						<label>Solution Tags <br /><span style="font-weight:normal">(Choose all that apply)</span></label>						
						<select id="SolutionTags" name="SolutionTags[]" multiple="multiple">
							<?php							
							if(!empty($product->SolutionTags)){
								$tags = explode(',',$product->SolutionTags);								
							}
							foreach($solutiontags as $tag){
								$c = '';
								if($tags!=NULL && count($tags)>0){
									if(in_array($tag,$tags))	
										$c = 'selected="selected"';
								}
								echo '<option '.$c.' value="'.$tag.'">'.$tag.'</option>';
							}	
							?>														
						</select>						
					</div>
					<div class="col" style="margin-left:20px;">
						<label>Federal Regulation Tags <br /><span style="font-weight:normal">(Choose all that apply)</span></label>
						<select id="FederalTags" name="FederalTags[]" multiple="multiple">
							<?php
							if(!empty($product->FederalTags)){
								$tags = explode(',',$product->FederalTags);								
							}
							foreach($federaltags as $tag){
								$c = '';
								if($tags!=NULL && count($tags)>0){
									if(in_array($tag,$tags))	
										$c = 'selected="selected"';
								}
								echo '<option '.$c.' value="'.$tag.'">'.$tag.'</option>';
							}	
							?>														
						</select>	
					</div>
				</div>
				<div class="formitem">
					<label style="float:left;margin-right:20px;">Active</label>
					<input style="float:left;" type="radio" name="Active" value="1" <?php if($product->Active==1) echo 'checked="checked"';?> id="activeyes" />
					<label style="float:left;margin-right:20px;" for="activeyes">Yes</label>
					<input style="float:left;" type="radio" name="Active" value="0" <?php if($product->Active==0) echo 'checked="checked"';?> id="activeno" / >
					<label style="float:left;margin-right:20px;" for="activeno">No</label>
				</div>				
				<div id="errmsg2" class="red"></div>
				<div class="formbutton">
					
					<div class="col">
						<input type="submit" class="btn center" value="Save" />
					</div>
					<div class="col">
						<input type="button" class="btn center" value="Cancel" onclick="window.location='<?php echo base_url();?>products/'" />
					</div>
					
				</div>
				<div id="upload-form" title="Upload"></div>
				<input type="hidden" id="ProductID" name="ProductID" value="<?php echo $product->ProductID;?>" />
			<?php echo form_close();?>
		</div>
	</div><!--end innerwrap-->					
	<div class="clear"></div>
</div>
<script type="text/javascript">	
	var iupload = 0;
	function uploadimg(e){
		if((e.files[0].size/1024)><?php echo $this->config->item('image_max_size');?>){
			alert($(e).val()+' could not be uploaded because the file size is too big');
			return;
		}
		var caller = $(e);
		var idx = caller.attr('id');			
		var statusbox = caller.parent().find('#ProductImage');	
		statusbox.val('Uploading...');
		statusbox.attr('disabled','disabled');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('products/upload_image_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
				statusbox.removeAttr('disabled');
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);
				   $('#imgcont').html('<img style="max-height: 100%; max-width: 100%" src="<?php echo $this->config->item('vendor_product_images_url').'/'. $this->session->userdata('mediaid');?>/'+data.msg+'">');
				}else{
					alert(data.msg);
					statusbox.val('');
				}
			 },
			 error:function(){
				 
			 }
		  });	
	}
	function uploadvid(e){
		
		if((e.files[0].size/1024)><?php echo $this->config->item('video_max_size');?>){
			alert($(e).val()+' could not be uploaded because the file size is too big');
			return;
		}
		
		var caller = $(e);
		var idx = caller.attr('id');
		var statusbox = caller.parent().find('.vidfname');	
		var titlebox = caller.parent().find('.vidtitle');
		statusbox.val('Uploading...');
		statusbox.attr('disabled','disabled');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('products/upload_video_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
				statusbox.removeAttr('disabled');
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);
				   if(titlebox.val()=='')
						titlebox.val('Untitled');
				}else{
					alert(data.msg);
					statusbox.val('');
				}
				caller.replaceWith('<input type="file" id="vid1" name="videofile" class="vid" style="display:none">');
			 },
			 error:function(){
				 
			 }
		  });	
	}
	function uploaddoc(e){
		if((e.files[0].size/1024)><?php echo $this->config->item('doc_max_size');?>){
			alert($(e).val()+' could not be uploaded because the file size is too big');
			return;
		}
		var caller = $(e);
		var idx = caller.attr('id');
		var statusbox = caller.parent().find('.docfname');	
		var titlebox = caller.parent().find('.doctitle');
		statusbox.val('Uploading...');
		statusbox.attr('disabled','disabled');
		$.ajaxFileUpload({
			 url         :'<?php echo base_url('products/upload_doc_file');?>',
			 secureuri      :false,
			 fileElementId  :idx,
			 dataType    : 'json',
			 data        : {
				<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>'						
			 },
			 success  : function (data, status)
			 {
			    statusbox.removeAttr('disabled');
				if(data.status != 'error')
				{					   			  
				   statusbox.val(data.msg);				   
					if(titlebox.val()=='')
						titlebox.val('Untitled');
				}else{										
					alert(data.msg);
					statusbox.val('');
				}
				
			 },
			 error:function(){
				 
			 }
		  });			
				
	}
	
	//http://www.raymondcamden.com/index.cfm/2012/1/23/Detecting-invalid-HTML-with-JavaScript
	function checkValidHtml(e){		
		var code = $.trim($('#ProductInfo').val());
        if(code == '') return true;
        
        var regex = /<.*?>/g;
        var matches = code.match(regex);
        if(matches==null || !matches.length) return;
        
        var invalidtags = new Array();
        var validtags = ["b","i","ul","li","ol","p"];
        $.each(matches, function(idx,itm) {
           //if the tag is, <..../>, it's self closing
            if (itm.substr(itm.length - 2, itm.length) != "/>") {
            
                //strip out any attributes
                var tag = itm.replace(/[<>]/g, "").split(" ")[0];                
                //start or end tag?
                if (tag.charAt(0) != "/") {
                   
                }
                else {
                    var realTag = tag.substr(1, tag.length);                    
                    if(validtags.indexOf(realTag)==-1)
                    {
						invalidtags.push(realTag);
					}
                   
                }
            }
        });
		if(invalidtags.length>0){
			alert('HTML code : '+invalidtags.join()+' is not allowed');
			 setTimeout(function() { $('#ProductInfo').focus(); }, 10);
			return false;
		}
        return true;
	}
	function addformupload(){
		//show only 2 doc upload form
		var n =(iupload+2);
		for(;iupload<n;iupload++){
			$('.upload'+iupload).show();
			$('.hrupload'+(iupload-1)).show();
		}
		if(iupload==10)
			$('#adddoc').hide();
		return false;
	}
	jQuery(document).ready(function($){ 
		
		$('form[name="addproductform"]').submit(function(){			
			var valid = validateForm2('form[name="addproductform"]');			
			if(!valid){
				$('#errmsg2').html('Please enter missing fields');
				return false;
			}
			var msg = null;
			$('.formupload').each(function(k,v){
				var fname = $(v).find('.vidfname,.docfname');
				var titlebox = $(v).find('.vidtitle,.doctitle');
				if(!fname.is(':disabled')){
					//user uploaded a file
					if(fname.val()!=''){						
						if(titlebox.val()==''){
							msg = 'Please enter a title to go with the uploaded video/file.';
							titlebox.addClass('error').removeClass('valid');
						}
					}else{
						//user only enter a title
						if(titlebox.val()!=''){							
							msg = 'Please upload a video/file to go with the entered title.';
							titlebox.addClass('error').removeClass('valid');
						}
					}					
				}else{					
					msg = 'Please wait until your video/file is finished uploading.';
					titlebox.addClass('error').removeClass('valid');
				}				
			});
			if(msg!=null){
				$('#errmsg2').html(msg);
				return false;
			}
			return true;
		});		
		//show only 2 doc upload form
		for(iupload=2;iupload<10;iupload++){
			$('.upload'+iupload).hide();
			$('.hrupload'+(iupload-1)).hide();
		}
		iupload=2;
		
		$("select").multiselect({
		   //selectedText: "# of # selected"
		   height:122
		});
		
		if($('input[name="ProductImage"]').val()!=''){			
			$('#imgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php  echo $this->config->item('vendor_product_images_url').'/'. $this->session->userdata('mediaid');?>/'+$('input[name="ProductImage"]').val()+'" />');						   		
		}else{
			$('#ProductImage').val('productimage.png');
			$('#imgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo base_url('images/productimage.png');?>" />');						   		
		}	
		$('.btnUpload').click(function(e){			
			$(e.target).parent().find('input[type="file"]').click();
		});
		$('#imgarea .lnkDelete').click(function(e){	
			e.preventDefault();		
			var fnamebox = $('#ProductImage');			
			if(fnamebox.val()=='')return;
			
			var prodid = $('#ProductID').val();
			$(e.target).html('Deleting...');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('products/delete_image_file');?>",
				data: { 
					<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
					filename: fnamebox.val(),
					productid:prodid 
				},
				success: function (data, status,x){
					var y = jQuery.parseJSON(data);
					if(y.status != 'error')
					{					   			  
					   fnamebox.val('productimage.png');
					   $('#imgcont').html('<img  style="max-height: 100%; max-width: 100%" src="<?php echo base_url('images/productimage.png');?>" />');						   		
					}else{										
						alert(y.msg);					
					}
				}
			}).done(function( data ) {
				$(e.target).html('Delete');	
			});
		});
		$('#productvideos .lnkDelete').click(function(e){	
			e.preventDefault();		
			var fnamebox = $(e.target).parent().find('.vidfname');	
			var titlebox = $(e.target).parent().find('.vidtitle');		
			if(fnamebox.val()=='')return;
			
			var prodid = $('#ProductID').val();
			$(e.target).html('Deleting...');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('products/delete_video_file');?>",
				data: { 
					<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
					filename: fnamebox.val(),
					productid:prodid 
				},
				success: function (data, status,x){
					var y = jQuery.parseJSON(data);
					if(y.status != 'error')
					{					   			  
					   fnamebox.val('');
					   titlebox.val('');
					}else{										
						alert(y.msg);					
					}
				}
			}).done(function( data ) {
				$(e.target).html('Delete');	
			});
		});
		$('#productfiles .lnkDelete').click(function(e){	
			e.preventDefault();		
			var fnamebox = $(e.target).parent().find('.docfname');	
			var titlebox = $(e.target).parent().find('.doctitle');		
			if(fnamebox.val()=='')return;
			
			var prodid = $('#ProductID').val();
			$(e.target).html('Deleting...');
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('products/delete_doc_file');?>",
				data: { 
					<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
					filename: fnamebox.val(),
					productid:prodid 
				},
				success: function (data, status,x){
					var y = jQuery.parseJSON(data);
					if(y.status != 'error')
					{					   			  
					   fnamebox.val('');
					   titlebox.val('');
					}else{										
						alert(y.msg);					
					}
				}
			}).done(function( data ) {
				$(e.target).html('Delete');	
			});
		});
		
	});
	
</script>
