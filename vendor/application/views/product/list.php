<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('vendor/leftmenu'); ?>
		<div id="productslist">
			<p class="title">View Products List</p>
			<br />
			<div id="errmsg" class="red"><?php  
			if ($this->session->flashdata('message')){
				echo $this->session->flashdata('message');
			}    
			?></div>
			<table id="tblproductslist" class='display' width="540">
			  <thead>
				<tr>
				  <th width="10"></th>
				  <th width="10"></th>
				  <th width="290">Name</th>
				  <th width="140">Date Created</th>				  
				</tr>
			  </thead>
			  <tbody>
				
			  </tbody>
			</table>
		</div>	
	</div>	
	<div class="clear"></div>
</div>	
<script type="text/javascript">
  /* Table initialisation */
  $(document).ready(function() {
	$('#tblproductslist').dataTable( {
	  "aoColumnDefs": [
			{ 
				"mRender": function ( data, type, row ) {					
					var html = '';
					if(row[4]=='0')
						html+='Inactive';
					else if(row[4]=='1')
						html+='<a target="_blank" href="<?php echo $this->config->item('main_site');?>v/<?php echo $this->session->userdata('vendoralias');?>/'+data+'/'+row[5]+'">View</a>';					
                    return html;
                },
                "aTargets": [0],
                "bSortable": false
			},
			{ 
				"mRender": function ( data, type, row ) {					
					var html = '';					
					html+='<a href="<?php echo base_url();?>products/edit/'+data+'">Edit</a>';
                    return html;
                },
                "aTargets": [1],
                "bSortable": false
			}
	  ],
	  "bFilter":false,
	  "bProcessing": true,
	  "aaSorting": [[ 3, "desc" ]],
	  "bServerSide": true,
	  "bJQueryUI": true,	  
	  "sPaginationType": "full_numbers",
	  "sAjaxSource": "<?php echo base_url();?>products/list_products",
	  "oLanguage": {						
			"sEmptyTable": "<a href=\"products/add\">Click here to add your first product</a>",
		}
	});	
	$('#tblproductslist_length').css("display", "none");
  });
</script>
