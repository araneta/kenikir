<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
	//current language: english, indonesia,..
	protected $m_current_lang;
	//data to be passed to view
	protected $m_data = array();
	
    function __construct()
    {
        parent::__construct();
		if(!$this->session->userdata('language'))
		{
			$this->session->set_userdata('language','english');
		}
		$this->m_current_lang = $this->session->userdata('language');		
		
    }
	function bind($model,$method){
		$props = get_object_vars($model);
		foreach($props as $k=>$v){
			if($method=='POST'){
				if(is_array($this->input->post($k))){
					$model->$k = array();
					foreach($this->input->post($k) as $input){
						array_push($model->$k, db_clean($input));
					}
				}else{
					$model->$k = db_clean($this->input->post($k));
				}
			}else if($method=='GET'){
				if(is_array($this->input->get($k))){
					$model->$k = array();
					foreach($this->input->get($k) as $input){
						array_push($model->$k, db_clean($input));
					}
				}else{
					$model->$k = db_clean($this->input->get($k));
				}
			}
		}
		return $model;
	}
	function set_lang_file($file){
		$this->lang->load($file,$this->m_current_lang);
	}
	function need_login(){		
		if ($this->session->userdata('userid')==FALSE ||
			$this->session->userdata('usertype')==FALSE||
			$this->session->userdata('usertype')!='admin'){			
			redirect('login','refresh');
			return;
		}
		
	}
	function import($ns){
		require_once (dirname(dirname(BASEPATH))).'/application/domain-model/'.$ns.'.php';	
	}
	function to_datatables_json($pagingresult,$cols){
		$ret = array();
		$ret['sEcho'] = $this->input->get('sEcho');
		$ret['iTotalRecords'] = $pagingresult->totalrecords;
		$ret['iTotalDisplayRecords'] = $pagingresult->totaldisplayrecords;
		$data = array();
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as $v){
				$item = array();
				foreach($cols as $c){
					$item[] = $v->$c;
				}
				$data[] = $item;
			}
		}
		$ret['aaData'] = $data;
		echo json_encode($ret);
	}
}
?>
