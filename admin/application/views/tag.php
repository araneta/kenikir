<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('leftmenu'); ?>
		<div id="vendorbox">
			<p class="title"><?php echo $title?></p>			
			<div id="errmsg" class="red"><?php  
			if ($this->session->flashdata('message')){
				echo $this->session->flashdata('message');
			}    
			?></div>
			<br /><br />
			<script type="text/javascript">
			  /* Table initialisation */
			  $(document).ready(function() {
				$('#taglist').dataTable( {
					"aoColumnDefs": [
						{ 
							"mRender": function ( data, type, row ) {					
								var html = '';								
								html+='<a href="#" onclick="edittag(\''+row[0]+'\');return false;">Edit</a>';																	
								return html;
							},
							"aTargets": [2],
							"bSortable": false
						},
						{ 
							"mRender": function ( data, type, row ) {					
								var html = '';								
								html+='<a href="#" onclick="deletetag(\''+row[0]+'\');return false;">Delete</a>';																	
								return html;
							},
							"aTargets": [3],
							"bSortable": false
						}
						
				  ],
				  "bProcessing": true,
				  "aaSorting": [[ 0, "asc" ]],
				  "bServerSide": true,
				  "bJQueryUI": true,
				  "sPaginationType": "full_numbers",
				  "sAjaxSource": "<?php echo base_url('tags/list_tags/'.$type);?>",
				  "sDom": '<"toolbar">frtip',	
				        
				});	    
			  });
			  function savetag(){
				  $.ajax({
					type: "POST",
					url: "<?php echo base_url('tags/save');?>",
					data: { 
						<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',
						oritagvalueid: $('#oritagvalueid').val(),
						tagvalueid: $('#tagvalueid').val(),
						tagtype:'<?php echo $type;?>'
					},
					success: function (data, status,x){
						var y = jQuery.parseJSON(data);
						if(y.status != 'error')
						{					   			  
						   $('#taglist').dataTable()._fnAjaxUpdate();
						   $('#tagvalueid').val('');
						   $('#oritagvalueid').val('');
						   $('#btnsave').val('Add New Tag');
						}else{										
							alert(y.msg);					
						}
					}
				}).done(function( data ) {
					
				});
			  }
			  function edittag(tag){
				$('#tagvalueid').val(tag);
				$('#oritagvalueid').val(tag);
				$('#btnsave').val('Save');
			  }
			  function deletetag(tag){
				$.ajax({
					type: "POST",
					url: "<?php echo base_url('tags/delete');?>",
					data: { 
						<?php echo $this->security->get_csrf_token_name();?>:'<?php echo $this->security->get_csrf_hash();?>',						
						tagvalueid: tag,
						tagtype:'<?php echo $type;?>'
					},
					success: function (data, status,x){
						var y = jQuery.parseJSON(data);
						if(y.status != 'error')
						{					   			  
						   $('#taglist').dataTable()._fnAjaxUpdate();						   
						}else{										
							alert(y.msg);					
						}
					}
				}).done(function( data ) {
					
				});
			  }
			</script>
			<div class="formitem" style="width:100%">
				<input type="text" id="tagvalueid" />
				<input type="hidden" id="oritagvalueid" />
				<input id= "btnsave" type="button" class="btn" value="Add New Tag" onclick="savetag();" />
			</div>
			<table id="taglist" class='display' width="540">
			  <thead>
				<tr>					
					<th>Tag Name</th>
					<th>Date Created</th>
					<th width="10"></th>								
					<th width="10"></th>
				</tr>
			  </thead>
			  <tbody>
				
			  </tbody>
			</table>
			<div class="clear"></div>						
		</div>	
	</div>	
	<div class="clear"></div>
</div>	



	 
	
