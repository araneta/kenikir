<?php
$uri  = $this->uri->uri_string();
$base = base_url();
$menu = array(
		'vendors'=>'View Vendors'
		,'customers'=>'View Customers'
		,'tags/view/solutions'=>'View Solutions Tags'
		,'tags/view/federal'=>'View Federal Requirements'
		,'sendmail/vendors'=>'Email All Vendors'
		,'sendmail/customers'=>'Email All Customers'		
		,'login/logout'=>'Log-Off'
		);		

?>

<div id="leftmenu">
	<ul>
		<li class="menuitem">
			<span class="lmtitle">Admin</span>
			<ul>
				<?php
				foreach($menu as $k=>$v){
					$c = '';
					$t = '';
					if(strpos($k,'http://')>-1)
						$t .= ' target="_blank"';
					if($uri==$k){
						$c .=' class="active"';
					}
					
					echo '<li '.$c.'><a '.$t.' href="'.(strpos($k,'http://')>-1?'':$base).$k.'">'.$v.'</a></li>';
				}		
				?>				
			</ul>
		</li>
		<li  class="menuitem">
			<span class="lmtitle">Need Help?</span>
			<ul>
				<li><a href="#">FAQs</a></li>
				<li><a href="#">Contact Us</a></li>
			</ul>
		</li>
	</ul>
</div>
