<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('leftmenu'); ?>
		<div id="vendorinfoform">
			<p class="title">Enter Security Information</p>
			<?php echo form_open('vendorinfo/edit_pass_save',array('name'=>'vendorinfoform')); ?>									
				<div id="errmsg" class="red"><?php  
						if ($this->session->flashdata('message')){
							echo $this->session->flashdata('message');
						}    
						?></div>		
				<br />
				<div class="formlegend">
					<label id="passwarn" class="legend">Passwords must be at least 7 characters in length and contain at least 1 letter, 1 number, and no spaces.</label>
				</div>
				<div class="formitem">
					<label>Password</label>
					<input type="password" id="Password_1" name="Password_1" class="required" />
				</div>
				<div class="formitem">
					<label>Confirm Password</label>
					<input type="password" id="Password_2" name="Password_2" class="required" />
				</div>
				<div class="formitem">
					<label>Security Question 1</label>
					<input type="text" name="Question1" class="required" value="<?php echo $vendor->Question1; ?>" /> 
				</div>
				<div class="formitem">
					<label>Answer 1</label>
					<input type="text" name="Answer1" class="required" value="<?php echo $vendor->Answer1; ?>" />
				</div>
				<div class="formitem">
					<label>Security Question 2</label>
					<input type="text" name="Question2" class="required" value="<?php echo $vendor->Question2; ?>" />
				</div>
				<div class="formitem">
					<label>Answer 2</label>
					<input type="text" name="Answer2" class="required" value="<?php echo $vendor->Answer2; ?>" />
				</div>
				
				<div class="formbutton">
					<div class="col">
						<input type="submit" class="btn center" value="Save" />
					</div>
					<div class="col">
						<input type="button" class="btn center" value="Cancel" onclick="window.location='<?php echo base_url('vendorinfo/edit/'.$vendor->VendorID);?>'" />
					</div>
					
				</div>
				<input type="hidden" name="VendorID" value="<?php echo $vendor->VendorID; ?>" >
			<?php echo form_close();?>
		</div>
	</div><!--end innerwrap-->					
	<div class="clear"></div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){ 		
		$('form[name="vendorinfoform"]').submit(function(){
			var msg = 'Please enter missing fields';			
			var pass1 = jQuery.trim(""+$('#Password_1').val());
			var pass2 = jQuery.trim(""+$('#Password_2').val());
			if(pass2=='' && pass1=='')
				return true;;
			if(pass2=='' && pass1!=''){
				msg = 'Please re-enter password to update your password.';
				$('#errmsg').html(msg);	
				return false;
			}else if(pass2!='' && pass1!='' && pass2!=pass1){
				msg = 'Passwords do not match.';			
				$('#errmsg').html(msg);	
				return false;
			}else{
				var valid = true;
				var l = pass1.length;				
				if(l<7)
					valid = false;
				var matches = pass1.match(/\d+/g);
				var matches2 = pass2.match(/\d+/g);
				if (matches == null||matches2 == null) {				
					valid= false;
				}
				matches = pass1.match(/^[a-zA-Z]+/g);
				matches2 = pass2.match(/^[a-zA-Z]+/g);
				if (matches == null||matches2 == null) {				
					valid= false;
				}
				if(!valid){				
					 $( "#passwarn" ).css("color","#f00");
				}else
				{
					$( "#passwarn" ).css("color","#606060");
				}
				return valid;
			}
			
		});	
			
	});
	
</script>
