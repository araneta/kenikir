<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('leftmenu'); ?>
		<div id="vendorbox">
			<p class="title">Vendors</p>			
			<div id="errmsg" class="red"><?php  
			if ($this->session->flashdata('message')){
				echo $this->session->flashdata('message');
			}    
			?></div>
			<div class="formlegend" style="text-align:center">
				<label>Inactive Vendors</label>
			</div>
			
			<script type="text/javascript">
			  /* Table initialisation */
			  $(document).ready(function() {
				$('#inactivevendorlist').dataTable( {
					"aoColumnDefs": [
						{ 
							"mRender": function ( data, type, row ) {					
								var html = '';
								if(row[5]=='0')
									html+='<a href="<?php echo base_url('vendors/activate');?>/'+row[0]+'">Activate</a>';																	
								else if(row[5]=='1')
									html+='Active';						
								return html;
							},
							"aTargets": [0],
							"bSortable": false
						},
						{ 
							"mRender": function ( data, type, row ) {	
								var html = '';				
								if(row[5]=='0')
									html+='View/Edit';					
								else if(row[5]=='1')
									html+='<a target="_blank" href="<?php echo $this->config->item('main_site');?>v/'+row[1]+'/'+row[2]+'">View</a>/<a href="">Edit</a>';					
								return html;
							},
							"aTargets": [1],
							"bSortable": false
						}
				  ],
				  "bProcessing": true,
				  "aaSorting": [[ 4, "desc" ]],
				  "bServerSide": true,
				  "bJQueryUI": true,
				  "sPaginationType": "full_numbers",
				  "sAjaxSource": "<?php echo base_url();?>vendors/ajax_inactive_vendors",
				  "sDom": '<"toolbar">frtip',	
				        
				});	    
			  });
			</script>
			<table id="inactivevendorlist" class='display' width="540">
			  <thead>
				<tr>
					<th width="10"></th>
					<th width="10"></th>
					<th>Company</th>					
					<th>User ID</th>
					<th>Date Created</th>
				</tr>
			  </thead>
			  <tbody>
				
			  </tbody>
			</table>
			<div class="clear"></div>			
			<div class="formlegend" style="text-align:center;margin-top:40px;">
				<label>Active Vendors</label>
			</div>
			<script type="text/javascript">
			  /* Table initialisation */
			  $(document).ready(function() {
				$('#activevendorlist').dataTable( {
					"aoColumnDefs": [
						{ 
							"mRender": function ( data, type, row ) {					
								var html = '';
								if(row[5]=='1')
									html+='<a href="<?php echo base_url('vendors/deactivate');?>/'+row[0]+'">De-activate</a>';																	
								return html;
							},
							"aTargets": [0],
							"bSortable": false
						},
						{ 
							"mRender": function ( data, type, row ) {	
								var html = '';				
								if(row[5]==0)
									html+='View/Edit';					
								else if(row[5]=='1')
									html+='<a target="_blank" href="<?php echo $this->config->item('main_site');?>v/'+row[1]+'/'+row[2]+'">View</a>/<a href="<?php echo base_url('vendorinfo/edit');?>/'+row[0]+'">Edit</a>';					
								return html;
							},
							"aTargets": [1],
							"bSortable": false
						}
				  ],
				  "bProcessing": true,
				  "aaSorting": [[ 4, "desc" ]],
				  "bServerSide": true,
				  "bJQueryUI": true,
				  "sPaginationType": "full_numbers",
				  "sAjaxSource": "<?php echo base_url();?>vendors/ajax_active_vendors",
				  "sDom": '<"toolbar">frtip',	      
				});	    
			  });
			</script>
			<table id="activevendorlist" class='display' width="540">
			  <thead>
				<tr>
					<th width="10"></th>
					<th width="10"></th>
					<th>Company</th>					
					<th>User ID</th>
					<th>Date Created</th>
				</tr>
			  </thead>
			  <tbody>
				
			  </tbody>
			</table>
		</div>	
	</div>	
	<div class="clear"></div>
</div>	



	 
	
