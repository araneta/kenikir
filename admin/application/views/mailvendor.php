<div id="govonomywhitebox">
	<?php $this->load->view('headerlogo');?>
	<div id="inner-wrap">
		<?php $this->load->view('leftmenu'); ?>
		<div id="vendorbox">
			<p class="title"><?php echo $title?></p>			
			<?php echo form_open('sendmail/send',array('name'=>'emailform')); ?>
			<div id="errmsg" class="red"><?php  
			if ($this->session->flashdata('message')){
				echo $this->session->flashdata('message');
			}    
			?></div>
			<br />
			<div class="formitem" style="width:100%">
				<label>From</label>				
				<input type="text" name="from" value="&quot;Govonomy.com&quot; &lt;admin@govonomy.com&gt;" style="width:300px" readonly="readonly" />				
			</div>
			<div class="formitem" style="width:100%">
				<label>Subject</label>				
				<input type="text" class="required" name="subject" value="" style="width:300px" />				
			</div>
			<div class="formitem" style="width:100%">
				<label>Message</label>				
				<textarea name="message" class="required" style="width:300px"></textarea>
			</div>
			<div class="formbutton" style="width:300px">
				<div class="col">
					<input name="sendtest" type="submit" class="btn center" value="Send Test" />
				</div>
				<div class="col">
					<input name="send" type="submit" class="btn center" value="Send" />
				</div>
				
			</div>
			<?php echo form_close();?>
			<div class="clear"></div>						
		</div>	
	</div>	
	<div class="clear"></div>
</div>	
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		$('form[name="emailform"]').submit(function(){			
			var valid = validateForm2('form[name="emailform"]');			
			if(!valid){
				$('#errmsg').html('Please enter missing fields');
				return false;
			}
			return true;
		});
	});
</script>	
