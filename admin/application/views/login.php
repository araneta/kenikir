	<div id="govonomygreybox">
			<?php $this->load->view('headerlogo');?>
			<div id="adminloginform">
				<p class="title grey" style="margin-top:90px;">Login</p>
				<br />
				<div id="errmsg" class="red">
					<?php  
					if ($this->session->flashdata('message')){
						echo $this->session->flashdata('message');
					}    
					?>    
				</div>
				<?php echo form_open('login/verify',array('name'=>'adminloginform')); ?>							
						<div class="formitem">
							<label>User ID</label>							
							<?php echo form_input('username',set_value('username'),'size=54 style="width:230px;height:24px;margin:0 auto;display:block;" class="required"'); ?>
						</div>
						<div class="clear"></div>
						<div class="formitem">
							<label>Password</label>							
							<?php echo form_password('password',set_value('password'),'size=54 style="width:230px;height:24px;margin:0 auto;display:block;" class="required"'); ?>
						</div>
						<div class="clear"></div>
						<br />
						
						<div class="clear"></div>
						<div id="errmsg" class="red"></div>
						
						<div class="formbutton" style="margin-top:20px;">							
							<input type="submit" class="btn" value="Login" />
							<a href="<?php echo base_url('forgot');?>" style="margin-left:10px" >Forgot Password?</a>						
						</div>
							
					
				<?php echo form_close();?>
				<br /><br />
				<div class="clear"></div>
			</div>		
			
		</div>

<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="adminloginform"]',function(){
			$('#errmsg').html('Please fill all fields.');	
		});
	});
	
</script>
