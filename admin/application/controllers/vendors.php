<?php
	class Vendors extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();
			//$this->set_lang_file('user/profile');
		}
		function index()
		{			
			$data['title'] = 'Vendors';		
			$data['main'] = 'vendor/list';
			$data['jsfiles'] = array('jquery.dataTables.min.js');
			$data['cssfiles'] = array('demo_table_jui.css');			
			$this->load->view('template',$data);	
		}		
		private function list_vendors($status){
			$this->import('entities/paging');
			$this->import('process/vendorprocess');
			$form = new Paging();
			$this->bind($form,'GET');
			if(!$form->validate()){
				echo 'error bind paging'.$form->error_messages();
			}else{
				$cols = array('VendorID','Alias','CompanyName','VendorID','CreatedDate','PublicStatus');
				$form->sortby = $cols[$form->sortby];
				
				$process = new VendorProcess($this->Gateway);				
				$vendors = $process->get_all($form,$status);
				
				$this->to_datatables_json($vendors,$cols);
			}		
		}
		function ajax_active_vendors(){
			$this->list_vendors(1);
		}
		function ajax_inactive_vendors(){
			$this->list_vendors(0);
		}
		function activate($vendorID){
			$this->import('process/vendorprocess');
			$process = new VendorProcess($this->Gateway);
			$msg  = 'Vendor Activated';
			$status = 'success';
			if(!$process->update_public_status($vendorID,TRUE))
			{
				$status = 'error';
				$msg = $process->error_messages();				
			}
			$this->session->set_flashdata('message',$msg);	
			redirect('vendors');
		}
		function deactivate($vendorID){
			$this->import('process/vendorprocess');
			$process = new VendorProcess($this->Gateway);
			$msg  = 'Vendor Deactivated';
			$status = 'success';
			if(!$process->update_public_status($vendorID,FALSE))
			{
				$status = 'error';
				$msg = $process->error_messages();				
			}
			$this->session->set_flashdata('message',$msg);	
			redirect('vendors');
		}
	}
?>
