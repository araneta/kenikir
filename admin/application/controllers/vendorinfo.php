<?php
//warning: this controller must be synchronized with vendor/info controller
	class VendorInfo extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();			
		}
		private function get_user_media_id(){
			if($this->session->userdata('selectedvendormediaid'))
				return $this->session->userdata('selectedvendormediaid');
		
			return NULL;	
		}
		function edit($vendorID){					
			
			$this->import('process/vendorprocess');
			$this->import('entities/vendorinfoform');
			$process = new VendorProcess($this->Gateway);
			$info = $process->get_vendor_info_and_media($vendorID);
			if($info==NULL){
				$this->session->set_flashdata('message','VendorID '.$vendorID.' Not Found');	
				$this->data['vendor'] = new EditVendorInfoForm();
			}else{				
				$this->data['vendor'] = $info;	
				//save it to session
				 $this->session->set_userdata('selectedvendorid',$info->VendorID);
				 $this->session->set_userdata('selectedvendormediaid',$info->MediaID);
			}			 
			$this->data['title'] = 'Vendor Information';		
			$this->data['main'] = 'vendor/editinfo';	
			$this->data['cssfiles'] = array($this->config->item('jquery.fileupload-ui.css'));
			$this->data['jsfiles'] = array('ajaxfileupload.js');				
			$this->load->view('template',$this->data);
		}
		function edit_save(){			
			$this->import('process/vendorprocess');
			$this->import('entities/vendorinfoform');
			
			$form = new EditVendorInfoForm();
			$this->bind($form,'POST');
			$form->VendorID = $this->session->userdata('selectedvendorid');
			if(!$form->validate()){
				$this->session->set_flashdata('message',$form->error_messages());
				redirect('vendorinfo/edit/'.$this->session->userdata('selectedvendorid'),'refresh');	
			}else{
				$process = new VendorProcess($this->Gateway);
				$vendor = $process->load_vendor($form->VendorID);
				if($vendor==NULL){
					$this->session->set_flashdata('message','Vendor Not Found');
					redirect('vendorinfo/edit/'.$this->session->userdata('selectedvendorid'),'refresh');	
				}else{
					$msg ='';
					//if user change the primary email
					if($vendor->Email != $form->Email){					
						$code = md5( rand(0,1000) ); //activation code							
						$form->NewEmail = $form->Email;
						$form->HashNewEmail = $code;
						$form->Email = $vendor->Email;					
					}
					if($process->update_info($form)){
						$this->session->set_userdata('vendoralias',$form->Alias);
						$msg .= 'Info updated';
						if(!empty($form->NewEmail)){
							if(!$process->send_new_email_activation($form->NewEmail,$form->HashNewEmail)){						
								$msg .= '<br />Failed to send email to this email:'.$form->NewEmail;							
							}else{
								$msg .= '<br />A verification email has been sent to '.$form->NewEmail.'. Please click on the verification link to update your email address.<br />';
							}			
						}
						//delete other image files
						
						$process->clean_vendor_dirs(
							$this->session->userdata('selectedvendorid'),
							$this->vendor_images_dir(),						
							$this->vendor_videos_dir(),
							$this->vendor_docs_dir()
						);
						$this->session->set_flashdata('message',$msg);		
									
						redirect('vendorinfo/edit/'.$this->session->userdata('selectedvendorid'),'refresh');	
					}else
					{					
						$this->session->set_flashdata('message',$process->error_messages());
						redirect('vendorinfo/edit/'.$this->session->userdata('selectedvendorid'),'refresh');	
					}	
				}			
			}	
		}
		function edit_pass(){			
			$vendor_id =$this->session->userdata('selectedvendorid');
			
			$this->import('process/vendorprocess');
			$this->import('entities/vendorinfoform');
			$process = new VendorProcess($this->Gateway);
			$info = $process->get_vendor_info($vendor_id);
			if($info==NULL){
				$this->session->set_flashdata('message','VendorID '.$vendor_id.' Not Found');	
				$this->data['vendor'] = new EditVendorInfoForm();
			}else{
				$this->data['vendor'] = $info;	
			}			 
			$this->data['title'] = 'Vendor Information';		
			$this->data['main'] = 'vendor/editpass';		
			$this->data['cssfiles'] = array($this->config->item('jquery.fileupload-ui.css'));
			$this->data['jsfiles'] = array('ajaxfileupload.js');		
			$this->load->view('template',$this->data);
		}
		function edit_pass_save(){			
			$this->import('process/vendorprocess');
			$this->import('entities/vendorinfoform');
			
			$form = new EditVendorSecurityInfoForm();
			$this->bind($form,'POST');
			$form->VendorID = $this->session->userdata('selectedvendorid');
			if(!$form->validate()){
				$this->session->set_flashdata('message',$form->error_messages());
				redirect('vendorinfo/edit/'.$this->session->userdata('selectedvendorid'),'refresh');	
			}else{
				$process = new VendorProcess($this->Gateway);
				$vendor = $process->load_vendor($form->VendorID);
				if($vendor==NULL){
					$this->session->set_flashdata('message','Vendor Not Found');
					redirect('vendorinfo/edit/'.$this->session->userdata('selectedvendorid'),'refresh');	
				}else{
					$msg ='';
					
					if($process->update_security_info($form)){
						$msg .= 'Info updated';
						
						$this->session->set_flashdata('message',$msg);					
						redirect('vendorinfo/edit_pass','refresh');	
					}else
					{					
						$this->session->set_flashdata('message',$process->error_messages());
						redirect('vendorinfo/edit_pass','refresh');	
					}	
				}			
			}	
		}
		private function vendor_videos_dir(){		
			return $this->config->item('vendor_videos_dir').'/'.$this->get_user_media_id();
		}
		private function vendor_docs_dir(){
			return $this->config->item('vendor_docs_dir').'/'.$this->get_user_media_id();
		}
		private function vendor_images_dir(){
			return $this->config->item('vendor_images_dir').'/'.$this->get_user_media_id();
		}
		//add info for the first time
		private function create_media_dirs(){
			$upload_path = $this->vendor_images_dir();	
			if (!file_exists($upload_path)) {			
				mkdir($upload_path, 0777);			
				if(!file_exists($upload_path.'/anonymouse.png')){
					copy($this->config->item('vendor_images_dir').'/anonymouse.png',$upload_path.'/anonymouse.png');
				}
			}	
			$upload_path = $this->vendor_videos_dir();	
			if (!file_exists($upload_path)) {
				mkdir($upload_path, 0777);			
			} 
			$upload_path = $this->vendor_docs_dir();	
			if (!file_exists($upload_path)) {
				mkdir($upload_path, 0777);			
			}	
		}
		//ajax
		function upload_image_file(){		
			if($this->get_user_media_id()==NULL)
				die('hard');
			$file_element_name = 'imagefile';
			$status = "";
			$msg = "";
			$upload_path = $this->vendor_images_dir();	
			
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] =  $this->config->item('image_upload_file_type');
			$config['max_size']  = $this->config->item('max_size');
			$config['encrypt_name'] = FALSE;
			$config['overwrite'] = false;		
			$config['file_name'] = $_FILES['imagefile']['name'];		
			
			$this->load->library('upload', $config);
	 
			if (!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}
			else
			{
				$data = $this->upload->data();			
				$image = $data['full_path'];
				//create thumbnail
				$this->import('helpers/imagehelper');
				ImageHelper::create_thumb($image,$image,FALSE,190);
				$status = "success";
				//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
				$msg = $data['file_name'];
				//@unlink($image);					

			}
			@unlink($_FILES[$file_element_name]);
		   
			echo json_encode(array('status' => $status, 'msg' => $msg,'originalfname'=>$data['orig_name']));
		}
		function upload_video_file(){		
			if($this->get_user_media_id()==NULL)
				die('hard');
			$file_element_name = 'videofile';
			$status = "";
			$msg = "";
			$upload_path = $this->vendor_videos_dir();	
				   
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] =  $this->config->item('video_upload_file_type');
			$config['max_size']  = $this->config->item('video_max_size');
			$config['encrypt_name'] = FALSE;
			$config['overwrite'] = FALSE;		
			$config['file_name'] = $_FILES['videofile']['name'];		
			
			$this->load->library('upload', $config);
			$fname = '';
			$ret = $this->upload->do_upload($file_element_name);
			$data = $this->upload->data();			
			if (!$ret)
			{
				$status = 'error';
				$msg = $data['file_name'].' : '.$this->upload->display_errors('', '');
			}
			else
			{			
				$image = $data['full_path'];
				$status = "success";
				//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
				$msg = $data['file_name'];
				//@unlink($image);				
				$fname  = $data['orig_name'];	

			}
			@unlink($_FILES[$file_element_name]);
		   
			echo json_encode(array('status' => $status, 'msg' => $msg,'originalfname'=>$fname));
		}
		function upload_doc_file(){		
			if($this->get_user_media_id()==NULL)
				die('hard');
			
			$file_element_name = 'docfile';
			$status = "";
			$msg = "";
			$upload_path = $this->vendor_docs_dir();	
				   
			$config['upload_path'] = $upload_path;
			$config['allowed_types'] =  $this->config->item('doc_upload_file_type');
			$config['max_size']  = $this->config->item('doc_max_size');
			$config['encrypt_name'] = FALSE;
			$config['overwrite'] = FALSE;		
			$config['file_name'] = $_FILES['docfile']['name'];		
			
			$this->load->library('upload', $config);
			$fname = '';
			if (!$this->upload->do_upload($file_element_name))
			{
				$status = 'error';
				$msg = $this->upload->display_errors('', '');
			}
			else
			{
				$data = $this->upload->data();			
				$image = $data['full_path'];
				$status = "success";
				//$msg = base_url(basename($this->config->item('upload_dir')).'/'.$data['file_name']);			
				$msg = $data['file_name'];
				//@unlink($image);				
				$fname  = $data['orig_name'];	

			}
			@unlink($_FILES[$file_element_name]);
		   
			echo json_encode(array('status' => $status, 'msg' => $msg,'originalfname'=>$fname));
		}
		function delete_video_file(){
			if($this->get_user_media_id()==NULL)
				die('hard');
			$fname = $this->input->post('filename');		
			$status = '';
			$msg = '';
			$this->import('process/vendorprocess');		
			$process = new VendorProcess($this->Gateway);				
			$ret = $process->delete_vendor_video($this->get_user_id(),$fname);
			/*if(!$ret){
				echo json_encode(array('status' => 'error', 'msg' => 'error deleting file from db'));
				return;
			}*/		
			$ret = $process->delete_file($fname,$this->vendor_videos_dir());
			
			if($ret){
				$status = 'success';	
			}else{
				$status = 'error';
				$msg = $process->error_messages();
			}
			echo json_encode(array('status' => $status, 'msg' => $msg));
		}
		function delete_image_file(){
			if($this->get_user_media_id()==NULL)
				die('hard');
			$fname = $this->input->post('filename');		
			$status = '';
			$msg = '';
			$this->import('process/vendorprocess');
			$upload_path = $this->vendor_images_dir();	
			$process = new VendorProcess($this->Gateway);
			
			$ret = $process->delete_vendor_image($this->get_user_id(),$fname);
			/*if(!$ret){
				echo json_encode(array('status' => 'error', 'msg' => 'error deleting file from db'));
				return;
			}*/		
			$ret = $process->delete_file($fname,$upload_path);
			
			if($ret){
				//using default image
				if(!file_exists($upload_path.'/anonymouse.png')){
					copy($this->config->item('vendor_images_dir').'/anonymouse.png',$upload_path.'/anonymouse.png');
				}
				$status = 'success';	
			}else{
				$status = 'error';
				$msg = $process->error_messages();
			}
			echo json_encode(array('status' => $status, 'msg' => $msg));
		}
		function delete_doc_file(){
			if($this->get_user_media_id()==NULL)
				die('hard');
			$fname = $this->input->post('filename');		
			$status = '';
			$msg = '';
			$this->import('process/vendorprocess');
			
			$process = new VendorProcess($this->Gateway);				
			$ret = $process->delete_vendor_doc($this->get_user_id(),$fname);
			/*if(!$ret){
				echo json_encode(array('status' => 'error', 'msg' => 'error deleting file from db'));
				return;
			}*/
			$ret = $process->delete_file($fname,$this->vendor_docs_dir());
			
			if($ret){
				$status = 'success';	
			}else{
				$status = 'error';
				$msg = $process->error_messages();
			}
			echo json_encode(array('status' => $status, 'msg' => $msg));
		}
	}
?>
