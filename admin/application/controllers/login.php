<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {  
	
	public function __construct() {
		parent::__construct();	         
		$this->set_lang_file('login');				
	}
	public function index() {		
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'login';		
		$this->load->view('template',$this->data);		
	}

	public function verify() 
	{
		$this->import('process/adminprocess');
		
		$form = new AdminLoginForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
		}else{			
			$process = new AdminProcess($this->Gateway);
			if($process->verify($form)){
				$admin = $process->admin();
				$this->set_identity($admin);
				
				redirect('vendors');
				return;
			}else
			{
				//echo 'notfound';
				$this->session->set_flashdata('message',$this->lang->line('wronglogin'));
			}
		}
		
		redirect('login','refresh');	
	}
	function logout(){		
		//$this->import('process/vendorremembermeprocess');
		//$rememberprocess = new VendorRememberMeProcess($this->Gateway);
		//$userid = $rememberprocess->check_remember_me();		
		//if(!empty($userid)){					
			//$rememberprocess->forget_me($userid);
		//}else
		//{
			//die('usernotfound');	
		//}
		$this->delete_identity();
		redirect('login','refresh');	
	}
	private function set_identity($admin){
		$this->session->set_userdata('userid',$admin->AdminID);
		$this->session->set_userdata('username',$admin->AdminID);
		$this->session->set_userdata('usertype','admin');
	}
	private function delete_identity(){
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('username');		
		$this->session->unset_userdata('usertype');
		$this->session->sess_destroy();
	}
}
?>
