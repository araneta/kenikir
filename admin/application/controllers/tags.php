<?php
	class Tags extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();			
		}
		function view($type)
		{			
			$validkeys = array('solutions'=>'Solutions Tags','federal'=>'Federal Requirement Tags');			
			if(!array_key_exists($type,$validkeys))
				die('invalid type');
				
			$this->data['title'] = 'View '.$validkeys[$type];		
			$this->data['main'] = 'tag';
			$this->data['type'] = $type;
			$this->data['jsfiles'] = array('jquery.dataTables.min.js');
			$this->data['cssfiles'] = array('demo_table_jui.css');			
			$this->load->view('template',$this->data);	
		}		
		//ajax
		public function list_tags($type){
			$validkeys = array('solutions','federal');			
			if(!in_array($type,$validkeys))
				die('hard');
			$this->import('entities/paging');
			$this->import('process/tagprocess');
			$form = new Paging();
			$this->bind($form,'GET');
			if(!$form->validate()){
				echo 'error bind paging'.$form->error_messages();
			}else{
				$cols = array('TagValueID','CreatedDate','TagValueID','TagValueID');
				$form->sortby = $cols[$form->sortby];
				
				$process = new TagProcess($this->Gateway);				
				if($type=='solutions')
					$tags = $process->get_all_solution($form);
				else if($type=='federal')
					$tags = $process->get_all_federal($form);
				$this->to_datatables_json($tags,$cols);
			}		
		}
		public function save(){
			$originaltagvalue = db_clean($this->input->post('oritagvalueid'));
			$tagvalueid = db_clean($this->input->post('tagvalueid'));
			$type = db_clean($this->input->post('tagtype'));
			
			if(empty($tagvalueid))
				die('missing parameter tagvalueid');
			$validkeys = array('solutions','federal');			
			if(!in_array($type,$validkeys))
				die('hard');
					
			$this->import('process/tagprocess');
			$process = new TagProcess($this->Gateway);	
			$msg = 'Tag Saved';
			$status = 'success';
			if($type=='solutions'){
				if(empty($originaltagvalue)){				
					if(!$process->add_solution_tag($tagvalueid))
						$status = $process->error_messages();					
				}else{
					if(!$process->update_solution_tag($originaltagvalue,$tagvalueid))
						$status = $process->error_messages();					
				}
			}else if($type=='federal'){
				if(empty($originaltagvalue)){				
					if(!$process->add_federal_tag($tagvalueid))
						$status = $process->error_messages();					
				}else{
					if(!$process->update_federal_tag($originaltagvalue,$tagvalueid))
						$status = $process->error_messages();					
				}
			}
			echo json_encode(array('status'=>$status,'msg'=>$msg));
		}
		public function delete(){			
			$tagvalueid = db_clean($this->input->post('tagvalueid'));
			$type = db_clean($this->input->post('tagtype'));
			
			if(empty($tagvalueid))
				die('missing parameter tagvalueid');
			$validkeys = array('solutions','federal');			
			if(!in_array($type,$validkeys))
				die('hard');
					
			$this->import('process/tagprocess');
			$process = new TagProcess($this->Gateway);	
			$msg = 'Tag Deleted';
			$status = 'success';
			if($type=='solutions'){
				if(!$process->delete_solution_tag($tagvalueid)){
					$status = $process->error_messages();					
				}
			}else if($type=='federal'){
				if(!$process->delete_federal_tag($tagvalueid)){
					$status = $process->error_messages();					
				}
			}
			echo json_encode(array('status'=>$status,'msg'=>$msg));
		}
	}
?>
