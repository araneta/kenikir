<?php
	class SendMail extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->need_login();			
		}
		function vendors()
		{							
			$this->data['title'] = 'Email All Vendors';		
			$this->data['main'] = 'mailvendor';			
			$this->load->view('template',$this->data);	
		}		
		function send(){
			$ret = TRUE;
			$this->import('process/sendmailprocess');
			if($this->input->post('sendtest')){
				$process = new SendMailProcess($this->Gateway);
				$ret = $process->send_to_all_admins(
					db_clean($this->input->post('from')),
					db_clean($this->input->post('subject')),
					db_clean($this->input->post('message'))
				);
				$msg = $process->error_messages();
			}else if($this->input->post('send')){
				$process = new SendMailProcess($this->Gateway);
				$ret = $process->send_to_all_vendors(
					db_clean($this->input->post('from')),
					db_clean($this->input->post('subject')),
					db_clean($this->input->post('message'))
				);
				$msg = $process->error_messages();
			}
			if($ret)
				$this->session->set_flashdata('message','Email sent');	
			else
				$this->session->set_flashdata('message',$msg);	
				
			redirect('sendmail/vendors');
		}
		
	}
?>
