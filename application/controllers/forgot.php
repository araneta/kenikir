<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Forgot extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        			
	}
	
	function index()
	{				 
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/signup';
		$this->data['filter'] = 'all';
		$this->load->view('template',$this->data);
	}
	function select_agency(){
		$email = db_clean($this->input->get('email'));
		if(empty($email))
		{
			redirect('register','refresh');
			return;
		}
		$this->import('process/memberprocess');
		$process = new MemberProcess($this->Gateway);
		$this->data['alphabet'] = $process->get_all_agency();
		$this->data['email'] = $email;
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/selectagency';
		$this->data['filter'] = 'all';		
		$this->data['jsfiles'] = array('jquery-ui-1.9.2.custom.min.js');
		$this->data['cssfiles'] = array('smoothness/jquery-ui-1.9.2.custom.min.css');
		$this->load->view('template',$this->data);
	}
	function verify(){
		$email = db_clean($this->input->get('email'));
		if(empty($email))
		{
			redirect('register','refresh');
			return;
		}		
		$this->data['filter'] = 'all';
		$this->data['email'] = $email;
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/verify';						
		$this->load->view('template',$this->data);
	}
	public function check_email()
	{			
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberRegistrationForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Invalid email address. Please check the spelling and try again.');
			redirect('register','refresh');	
		}else{						
			$process = new MemberProcess($this->Gateway);
			if($process->check_email($form)){				
				redirect('register/select_agency?email='.$form->email,'refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register','refresh');	
			}	
		}
		
	}
	public function create(){
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberAgencyRegistrationForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/select_agency?email='.$form->Email,'refresh');	
		}else{						
			$process = new MemberProcess($this->Gateway);
			if($process->register($form)){				
				redirect('register/verify?email='.$form->Email,'refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/select_agency?email='.$form->Email,'refresh');	
			}	
		}
	}
	public function verify_email(){
		
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberVerificationCodeForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/verify?email='.$form->Email,'refresh');	
		}else{						
			$process = new MemberProcess($this->Gateway);
			if($process->activate($form)){				
				$this->session->set_userdata('userid',$process->member()->MemberID);
				$this->session->set_userdata('username',$process->member()->MemberID);
				redirect('members/enterid','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/verify?email='.$form->Email,'refresh');	
			}	
		}
	}
	
}
?>
