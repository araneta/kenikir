<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {  
	
	public function __construct() {
		parent::__construct();	         
		//$this->set_lang_file('login');	
		$this->check_remember_me();				
	}
	public function index() {				
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'login';		
		$this->data['filter'] = 'all';		
		$this->load->view('template',$this->data);		
	}
	public function verify() 
	{
		$this->import('process/memberprocess');
		$this->import('process/memberremembermeprocess');
		$this->import('entities/memberloginform');
		
		$form = new MemberLoginForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Please fill all fields.');
			redirect('login','refresh');	
		}else{					
			$process = new MemberProcess($this->Gateway);
			if($process->login($form)){
				$m = $process->member();
				$this->session->set_userdata('userid',$m->MemberID);
				$this->session->set_userdata('username',$m->MemberID);
				$this->session->set_userdata('firstname',$m->FirstName);
				$this->session->set_userdata('lastname',$m->LastName);
				$this->session->set_userdata('usertype','member');
				//echo 'remember'.$form->rememberme;
				if($form->RememberMe==1){
					$rememberprocess = new MemberRememberMeProcess($this->Gateway);
					$rememberprocess->remember($v->MemberID);
				}
				redirect(base_url(),'refresh');	
			}else
			{
				$this->session->set_flashdata('message','Invalid Username or Password.');
				redirect('login','refresh');	
			}	
		}
	}
	
	function check_remember_me(){
		$this->import('process/memberremembermeprocess');
		$rememberprocess = new MemberRememberMeProcess($this->Gateway);
		$userid = $rememberprocess->check_remember_me();		
		if(!empty($userid)){		
			$this->session->set_userdata('userid',$userid);
			$this->session->set_userdata('username',$userid);
			redirect(base_url(),'refresh');	
		}	
	}
	function logout(){
		$this->session->unset_userdata('userid');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('usertype');
		$this->session->sess_destroy();
		redirect('login','refresh');	
	}
}
?>
