<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//search controller
class S extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        			
	}
	function index(){
		$this->import('entities/paging');
		$this->import('process/searchproductprocess');
		
		$process = new SearchProductProcess($this->Gateway);
		/*search box*/
		$keywords = '';
		$keywords2 = '';
		if($this->input->get('keywords'))			 		
			$keywords = db_clean($this->input->get('keywords'));
		if($this->input->get('keywords2'))			 		
			$keywords2 = db_clean($this->input->get('keywords2'));
		$filter = 'all';	
		$filter2 = '';
		if($this->input->get('filter'))	
			$filter = db_clean($this->input->get('filter'));
		if($this->input->get('filter2'))	
			$filter2 = db_clean($this->input->get('filter2'));	
		if((!empty($filter2)&&empty($keywords2))||(!empty($keywords2)&&empty($filter2)))
			die('missing parameter');
		$form = new Paging();					
		if($form->iDisplayLength==0)
			$form->iDisplayLength = 100;		
		$form->sSearch = $keywords;		
		$form->sSearch2 = $keywords2;
		$form->iDisplayStart=0;
		if($this->input->get('page')){
			$form->iDisplayStart=(id_clean($this->input->get('page'))-1)*$form->iDisplayLength ;		
		}				
		$form->sSortDir_0 = 'asc';
		$validfilter = array('all'=>'All Categories','solutions'=>'Solutions','vendor'=>'Vendor','product'=>'Product','federal'=>'Federal Regulation');
		if(!array_key_exists($filter,$validfilter)||(!empty($filter2)&&!array_key_exists($filter2,$validfilter)))		
			die('die hard');		
		$this->data['filter'] = $filter;
		$this->data['filter2'] = $filter2;
		$this->data['filtertitle'] = $validfilter[$filter];
		$form->iSortCol_0 = 'Name';		
		if(!$form->validate()){
			die('error bind paging'.$form->error_messages());
		}else{
			if(empty($filter2))
				$searchresult = $process->search_product($form,$filter);
			else
				$searchresult = $process->search_product_with_filter($form,$filter,$filter2);	
			$this->data['searchresult'] = $searchresult;			
		}
		$this->data['title'] = 'Search Result for '.$keywords;		
		$this->data['main'] = 'searchresult';
		$this->data['keywords'] = $keywords;
		$this->data['keywords2'] = $keywords2;
		$ret = $process->get_filter_data($form->filter,$filter);	
		$this->data['vendors'] = $ret->Vendors;		
		$this->data['solutions'] = $ret->Solutions;
		$this->data['federals'] = $ret->Federals;	
		$this->data['products'] = $ret->Products;			
		$this->load->view('template',$this->data);
	}
	
}
