<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Members extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        	
		//$this->set_lang_file('member');
		$this->need_login();
	}
	
	function index()
	{				 				
		$this->import('process/memberprocess');		
					
		$process = new MemberProcess($this->Gateway);
		$member = $process->get_by_id($this->session->userdata('userid'));			
		
		$this->data['member'] = $member;
		
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'member/viewinfo';						
		$this->load->view('template',$this->data);
	}
	
	public function enterid(){				
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'member/enterid';						
		$this->load->view('template',$this->data);
	}
	public function setid(){
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberSetMemberIDForm();
		$this->bind($form,'POST');
		$form->OldMemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('members/enterid','refresh');	
		}else{						
			
			$process = new MemberProcess($this->Gateway);
			if($process->set_memberid($form)){	
				$this->session->set_userdata('userid',$process->member()->MemberID);
				$this->session->set_userdata('username',$process->member()->MemberID);			
				redirect('members/enterpass','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('members/enterid','refresh');	
			}	
		}
	}
	public function enterpass(){				
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'member/enterpass';						
		$this->load->view('template',$this->data);
	}
	public function setpass(){
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberSetPasswordForm();
		$this->bind($form,'POST');
		$form->MemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('members/enterpass?email='.$form->Email,'refresh');	
		}else{						
			
			$process = new MemberProcess($this->Gateway);
			if($process->set_password($form)){					
				redirect('members/enterinfo','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('members/enterpass','refresh');	
			}	
		}
	}
	public function enterinfo(){		
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
			
		$process = new MemberProcess($this->Gateway);
		$member = $process->get_by_id($this->session->userdata('userid'));			
		if($member==NULL){
			$member = new MemberSetInfoForm();
		}
		$this->data['member'] = $member;
		
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'member/enterinfo';						
		$this->load->view('template',$this->data);
	}
	public function editinfo(){		
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
			
		$process = new MemberProcess($this->Gateway);
		$member = $process->get_by_id($this->session->userdata('userid'));			
		if($member==NULL){
			$member = new MemberSetInfoForm();
		}
		$this->data['member'] = $member;
		
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'member/editinfo';						
		$this->load->view('template',$this->data);
	}
	public function setinfo(){		
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberSetInfoForm();
		$this->bind($form,'POST');
		$form->MemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('members/enterinfo','refresh');	
		}else{						
			
			$process = new MemberProcess($this->Gateway);
			if($process->set_info($form)){					
				$this->session->set_flashdata('message','Your Information Has Been Saved');
				redirect('members','refresh');	
			}else
			{
				//$this->session->set_flashdata('session',array('form'=>$form,'message'=>$process->error_messages()));
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('members/enterinfo','refresh');	
			}	
		}
	}
	
}
?>
