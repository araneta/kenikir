<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Player extends MY_Controller {  
	public function __construct() {		
		parent::__construct();	                				        			
	}
	
	function index()	
	{			
		$f = $this->input->get('f');
		if(empty($f))
			return;
		if(strpos($f,$this->config->base_url())!=0)
				die('hard');
		$this->data['title'] = $this->lang->line('login');		
		$this->data['url'] = $this->input->get('f');
		//check extension
		$ext = pathinfo($this->input->get('f'), PATHINFO_EXTENSION);
		$views = array(
			'swf'=>'player/swf',
			'pdf'=>'player/pdf',
			'txt'=>'player/pdf',
		);		
		if(!array_key_exists($ext,$views))		
			die('there is no view to view filetype:'.$ext);
		$this->data['main'] = $views[$ext];	
		$this->load->view('playertemplate',$this->data);		
	}
}
