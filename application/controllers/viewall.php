<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ViewAll extends MY_Controller {  
	public function __construct() {		
		parent::__construct();	                				        			
	}
	
	function index($tab)	
	{			
		$validkey = array('solutions','vendors','products','federals');
		if(!in_array($tab,$validkey))
			die('invalid url');
			
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'viewall';		
		$this->data['filter'] = 'all';						
		
		$this->import('process/searchproductprocess');
		$this->import('process/productprocess');
		$this->import('process/vendorprocess');
		
		$searchprocess = new SearchProductProcess($this->Gateway);		
		//sidebar data
		$ret = $searchprocess->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];	
		//
		$paging = new Paging();
		$paging->iDisplayLength = 100;				
		$paging->iDisplayStart=0;
		$paging->sSortDir_0 = 'asc';
		
		$ret = NULL;
		if($tab=='vendors')
		{
			$paging->iSortCol_0 = 'CompanyName';
			if(!$paging->validate()){
				die('error validating solution '.$paging->error_messages());
			}
			$ret = $searchprocess->get_all_vendor_name($paging);
			$this->data['linktitle'] = 'Vendors';
		}else if($tab=='solutions')
		{
			$paging->iSortCol_0 = 'TagValueID';
			if(!$paging->validate()){
				die('error validating solution '.$paging->error_messages());
			}
			$ret = $searchprocess->get_all_solution_name($paging);
			$this->data['linktitle'] = 'Solutions';
		}else if($tab=='products')
		{
			$paging->iSortCol_0 = 'Name';
			if(!$paging->validate()){
				die('error validating solution '.$paging->error_messages());
			}
			$ret = $searchprocess->get_all_product_name($paging);
			$this->data['linktitle'] = 'Products';
		}else if($tab=='federals')
		{
			$paging->iSortCol_0 = 'TagValueID';
			if(!$paging->validate()){
				die('error validating solution '.$paging->error_messages());
			}
			$ret = $searchprocess->get_all_federal_name($paging);
			$this->data['linktitle'] = 'Federal Requirements';
		}
		$this->data['links'] = $ret->data;
		$this->load->view('template',$this->data);		
	}
	
}
