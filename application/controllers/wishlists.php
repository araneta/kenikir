<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wishlists extends MY_Controller {  
	
	public function __construct() {
		parent::__construct();	         
		//$this->set_lang_file('login');	
		$this->need_login();				
	}
	public function index() {					
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'wishlist/list';		
		$this->data['filter'] = 'all';		
		$this->data['jsfiles'] = array('jquery.dataTables.min.js');
		//$this->data['cssfiles'] = array('demo_table_jui.css');	
		
		$this->import('process/searchproductprocess');
		$process = new SearchProductProcess($this->Gateway);
		$ret = $process->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];	
		
		$this->load->view('template',$this->data);		
	}
	public function create(){		
		$this->import('process/searchproductprocess');
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'wishlist/create';		
		$this->data['filter'] = 'all';		
		$process = new SearchProductProcess($this->Gateway);
		$ret = $process->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];		
		$this->load->view('template',$this->data);
	}
	public function edit($wishlistID){		
		
		$this->import('process/searchproductprocess');
		$this->import('process/wishlistprocess');
		
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'wishlist/edit';		
		$this->data['filter'] = 'all';		
		$process = new SearchProductProcess($this->Gateway);
		$ret = $process->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];		
		
		$wishprocess = new WishListProcess($this->Gateway);
		$wishlist = $wishprocess->get_wishlist($wishlistID,$this->session->userdata('userid'));
		
		if($wishlist==NULL){						
			$this->session->set_flashdata('message','Wishlist Not Found');
			redirect('wishlists','refresh');	
			exit(0);
		}else{
			$this->data['wishlist'] = $wishlist;
			$this->load->view('template',$this->data);
		}
		
	}
	public function save(){
		$this->import('process/wishlistprocess');
		$this->import('entities/wishlistform');
		
		$form = new WishListEntryForm();
		$this->bind($form,'POST');
		$form->MemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('wishlists/create','refresh');	
		}else{						
			
			$process = new WishListProcess($this->Gateway);
			if($form->WishlistID==NULL){
				if($process->add_wishlist($form)){					
					redirect('wishlists','refresh');	
				}else
				{
					$this->session->set_flashdata('message',$process->error_messages());
					redirect('wishlists/create','refresh');	
				}	
			}else{
				if($process->update_wishlist($form)){					
					redirect('wishlists','refresh');	
				}else
				{
					$this->session->set_flashdata('message',$process->error_messages());
					redirect('wishlists/edit/'.$form->WishlistID,'refresh');	
				}		
			}
		}
	}
	public function delete_item($id){
		$this->import('process/wishlistprocess');
		
		$process = new WishListProcess($this->Gateway);	
		$wishlist = $process->get_wishlist_by_item($id,$this->session->userdata('userid'));
		if($wishlist==NULL){
			$this->session->set_flashdata('message','Wishlist Not Found');
			redirect('wishlists','refresh');				
		}
		if($process->delete_item($id)){					
			redirect('wishlists/view/'.$wishlist->WishlistID,'refresh');	
		}else
		{
			$this->session->set_flashdata('message',$process->error_messages());
			redirect('wishlists/view/'.$wishlist->WishlistID,'refresh');				
		}
	}
	public function add($productid){
		$this->import('process/wishlistprocess');
		$this->import('process/productprocess');
		$process = new WishListProcess($this->Gateway);
		$default_wishlist = $process->get_default_wishlist($this->session->userdata('userid'));
		if($default_wishlist==NULL)//no wishlist created yet
		{
			redirect('wishlists/create','refresh');	
		}else{
			//check product id
			$productprocess = new ProductProcess($this->Gateway);
			$product = $productprocess->get_product_by_id($productid);
			if($product==NULL || ($product!=NULL && $product->Active==0)){
				$this->session->set_flashdata('message','Product Not Found');
				redirect('wishlists','refresh');	
			}else{
				if($process->add_product_to_wishlist($product,$default_wishlist))	{
					$this->session->set_flashdata('message','You\'ve added an item to your Wish List');
					redirect('wishlists','refresh');	
				}else
				{
					$this->session->set_flashdata('message',$process->error_messages());
					redirect('wishlists','refresh');	
				}	
			}
		}
		
		
	}
	public function view($wishlistID) {					
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'wishlist/listproducts';		
		$this->data['filter'] = 'all';		
		$this->data['jsfiles'] = array('jquery.dataTables.min.js');
		//$this->data['cssfiles'] = array('demo_table_jui.css');	
		
		$this->import('process/searchproductprocess');
		$this->import('process/wishlistprocess');
		$process = new SearchProductProcess($this->Gateway);
		$ret = $process->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];	
		
		$wishprocess = new WishListProcess($this->Gateway);
		$wishlist = $wishprocess->get_wishlist($wishlistID,$this->session->userdata('userid'));
		if($wishlist==NULL){			
			$this->session->set_flashdata('message','Wishlist Not Found');
			redirect('wishlists','refresh');	
			exit(0);
		}else{
			$this->data['wishlist'] = $wishlist;			
		}
		$this->load->view('template',$this->data);		
	}
	//ajax
	function list_wish(){
		$this->import('entities/paging');
		$this->import('process/wishlistprocess');
		$form = new Paging();
		$this->bind($form,'GET');
		if(!$form->validate()){
			echo 'error bind paging'.$form->error_messages();
		}else{
			//$cols = array('Name','CreatedDate','LastItemID','LastItemName');
			$cols = array('Name','CreatedDate','LastItem','WishlistID');
			$form->sortby = $cols[$form->sortby];
			
			$process = new WishListProcess($this->Gateway);
			$products = $process->get_wishlist_by_member($form,$this->session->userdata('userid'));
			
			$this->to_datatables_json($products,$cols);
		}	
	}
	function list_products($wishlistID){
		$this->import('entities/paging');
		$this->import('process/wishlistprocess');
		$form = new Paging();
		$this->bind($form,'GET');
		if(!$form->validate()){
			echo 'error bind paging'.$form->error_messages();
		}else{
			//$cols = array('Name','CreatedDate','LastItemID','LastItemName');
			$cols = array('Name','CompanyName','DateAdded','ProductID','VendorID','ItemID');
			$form->sortby = $cols[$form->sortby];
			
			$process = new WishListProcess($this->Gateway);
			$products = $process->get_wishlist_products($form,$wishlistID,$this->session->userdata('userid'));
			
			$this->to_datatables_json($products,$cols);
		}	
	}
}
?>
