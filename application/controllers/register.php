<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Register extends MY_Controller {  
	public function __construct() {
		parent::__construct();	                				        	
		$this->set_lang_file('register');
	}
	
	function index()
	{				 
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/signup';
		$this->data['filter'] = 'all';
		$this->load->view('template',$this->data);
	}
	function select_agency(){
		$email = db_clean($this->input->get('email'));
		if(empty($email))
		{
			redirect('register','refresh');
			return;
		}
		$this->import('process/memberprocess');
		$process = new MemberProcess($this->Gateway);
		$this->data['alphabet'] = $process->get_all_agency();
		$this->data['email'] = $email;
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/selectagency';
		$this->data['filter'] = 'all';		
		$this->data['jsfiles'] = array('jquery-ui-1.9.2.custom.min.js');
		$this->data['cssfiles'] = array('smoothness/jquery-ui-1.9.2.custom.min.css');
		$this->load->view('template',$this->data);
	}
	function verify(){
		$email = db_clean($this->input->get('email'));
		if(empty($email))
		{
			redirect('register','refresh');
			return;
		}		
		$this->data['filter'] = 'all';
		$this->data['email'] = $email;
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/verify';						
		$this->load->view('template',$this->data);
	}
	public function check_email()
	{			
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberRegistrationForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message','Invalid email address. Please check the spelling and try again.');
			redirect('register','refresh');	
		}else{						
			$process = new MemberProcess($this->Gateway);
			if($process->check_email($form)){				
				redirect('register/select_agency?email='.$form->email,'refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register','refresh');	
			}	
		}
		
	}
	public function create(){
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberAgencyRegistrationForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/select_agency?email='.$form->Email,'refresh');	
		}else{						
			$process = new MemberProcess($this->Gateway);
			if($process->register($form)){				
				redirect('register/verify?email='.$form->Email,'refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/select_agency?email='.$form->Email,'refresh');	
			}	
		}
	}
	public function verify_email(){
		
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberVerificationCodeForm();
		$this->bind($form,'POST');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/verify?email='.$form->Email,'refresh');	
		}else{						
			$process = new MemberProcess($this->Gateway);
			if($process->activate($form)){				
				$this->session->set_userdata('userid',$process->member()->MemberID);
				$this->session->set_userdata('username',$process->member()->MemberID);
				redirect('members/enterid','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/verify?email='.$form->Email,'refresh');	
			}	
		}
	}
	/*
	public function enterid(){
			
		if(!$this->session->userdata('userid'))
		{
			die('Userid empty');
		}
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/enterid';						
		$this->load->view('template',$this->data);
	}
	public function setid(){
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberSetMemberIDForm();
		$this->bind($form,'POST');
		$form->OldMemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/enterid','refresh');	
		}else{						
			
			$process = new MemberProcess($this->Gateway);
			if($process->set_memberid($form)){	
				$this->session->set_userdata('userid',$process->member()->MemberID);
				$this->session->set_userdata('username',$process->member()->MemberID);			
				redirect('register/enterpass','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/enterid','refresh');	
			}	
		}
	}
	public function enterpass(){		
		if(!$this->session->userdata('userid'))
		{
			die('Userid empty');
		}
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/enterpass';						
		$this->load->view('template',$this->data);
	}
	public function setpass(){
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberSetPasswordForm();
		$this->bind($form,'POST');
		$form->MemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/enterpass?email='.$form->Email,'refresh');	
		}else{						
			
			$process = new MemberProcess($this->Gateway);
			if($process->set_password($form)){					
				redirect('register/enterinfo','refresh');	
			}else
			{
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/enterpass','refresh');	
			}	
		}
	}
	public function enterinfo(){		
		if(!$this->session->userdata('userid'))
		{
			die('Userid empty');
		}
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
			
		$process = new MemberProcess($this->Gateway);
		$member = $process->get_by_id($this->session->userdata('userid'));			
		if($member==NULL){
			$member = new MemberSetInfoForm();
		}
		$this->data['member'] = $member;
		
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/enterinfo';						
		$this->load->view('template',$this->data);
	}
	public function setinfo(){
		if(!$this->session->userdata('userid'))
		{
			die('Userid empty');
		}
		$this->import('process/memberprocess');
		$this->import('entities/memberregistrationform');
		
		$form = new MemberSetInfoForm();
		$this->bind($form,'POST');
		$form->MemberID = $this->session->userdata('userid');
		if(!$form->validate()){
			$this->session->set_flashdata('message',$form->error_messages());
			redirect('register/enterinfo','refresh');	
		}else{						
			
			$process = new MemberProcess($this->Gateway);
			if($process->set_info($form)){					
				$this->session->set_flashdata('message','Your Information Has Been Saved');
				redirect('register/viewinfo','refresh');	
			}else
			{
				//$this->session->set_flashdata('session',array('form'=>$form,'message'=>$process->error_messages()));
				$this->session->set_flashdata('message',$process->error_messages());
				redirect('register/enterinfo','refresh');	
			}	
		}
	}
	public function viewinfo(){
		if(!$this->session->userdata('userid'))
		{
			die('Userid empty');
		}
		$this->import('process/memberprocess');		
					
		$process = new MemberProcess($this->Gateway);
		$member = $process->get_by_id($this->session->userdata('userid'));			
		
		$this->data['member'] = $member;
		
		$this->data['filter'] = 'all';		
		$this->data['title'] = 'Become a GOVonomy Member';		
		$this->data['main'] = 'register/viewinfo';						
		$this->load->view('template',$this->data);
	}*/
}
?>
