<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {  
	
	public function __construct() {
		parent::__construct();	         				
	}
	public function index() {			
		//$this->output->cache(60);	
		$this->import('process/searchproductprocess');
				
		$this->data['title'] = $this->lang->line('login');		
		$this->data['main'] = 'home';		
		$this->data['filter'] = 'all';
		//sidebar data
		$process = new SearchProductProcess($this->Gateway);	
		$ret = $process->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];		
		
		$ismember = FALSE;
		if($this->session->userdata('userid')&&$this->session->userdata('usertype')=='member')
			$ismember = TRUE;
		$this->data['ismember'] = $ismember;
		//wishlist
		if($ismember){			
			$this->import('process/wishlistprocess');	
			$wishprocess = new WishListProcess($this->Gateway);
			$this->data['wishlist'] = $wishprocess->get_wishlist_count_by_member($this->session->userdata('userid'));
		}
		$this->load->view('template',$this->data);						
	}

}
?>
