<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendors extends MY_Controller {  
	public function __construct() {		
		parent::__construct();	                				        			
	}
	
	function index($vendorAlias)	
	{							
		$this->data['filter'] = 'all';						
		
		$this->import('process/searchproductprocess');
		$this->import('process/productprocess');
		$this->import('process/vendorprocess');
		
		$searchprocess = new SearchProductProcess($this->Gateway);
		$productprocess = new ProductProcess($this->Gateway);
		$vendorprocess = new VendorProcess($this->Gateway);
		//sidebar data
		$ret = $searchprocess->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];	
		//vendor public info
		$info = $vendorprocess->get_vendor_info_by_alias($vendorAlias);		
		if($info==NULL){
			die('Vendor not found');				
		}
		if(empty($info->PublicStatus)){
			$this->data['main'] = 'vendorinactive';		
			$this->data['title'] = 'Vendor Approval In Progress';							
		}else{
			$this->data['main'] = 'vendor';		
			$this->data['title'] = $info->CompanyName;				
			$this->data['description'] = $info->PublicInfo;
			$vendorID = $info->VendorID;
			$vm = $vendorprocess->get_vendor_media($info);		
			$this->data['vendor'] = $vm;
			//vendor's product		
			$vendorproducts = array();
			$vendorvideos = array();
			$vendordocs = array();
			$maxvendproducts = 6;
			$maxvendvideos = 4;
			$maxvenddocs = 10;
			$ppaging = new Paging();
			$ppaging->iDisplayLength = 20;				
			$ppaging->iDisplayStart=0;
			$ppaging->sSortDir_0 = 'asc';
			$ppaging->iSortCol_0 = 'Name';
			if(!$ppaging->validate()){
				die('error validating vendor products '.$ppaging->error_messages());
			}else{			
				$pagingresult = $productprocess->get_active_products_and_media_by_vendor($ppaging,$vendorID);				
				if($pagingresult->data!=NULL){
					//var_dump($pagingresult->data);
					foreach($pagingresult->data as &$p){
						if(count($vendorproducts)<$maxvendproducts){
							$vendorproducts[] = $p;					
						}
					}
				}		
			}	
				
			$this->data['vendorproducts'] = $vendorproducts;
			//$this->data['vendorvideos'] = VideoFileName;
			//$this->data['vendordocs'] = $vendordocs;		
		}				
		$this->load->view('template',$this->data);		
	}
	function product($vendorAlias,$productID,$productName)	
	{						
		$this->data['main'] = 'product';		
		$this->data['filter'] = 'all';						
		
		$this->import('process/searchproductprocess');
		$this->import('process/productprocess');
		$this->import('process/vendorprocess');
		
		$searchprocess = new SearchProductProcess($this->Gateway);
		$productprocess = new ProductProcess($this->Gateway);
		$vendorprocess = new VendorProcess($this->Gateway);
		//sidebar data
		$ret = $searchprocess->get_sidebar_data();	
		$this->data['vendors'] = $ret['vendors'];		
		$this->data['solutions'] = $ret['solutions'];
		$this->data['federals'] = $ret['federals'];	
		$this->data['products'] = $ret['products'];	
		//vendor public info
		$info = $vendorprocess->get_vendor_info_by_alias($vendorAlias);		
		if($info==NULL || ($info!=NULL && $info->Active==0)){
			die('Vendor not found');	
			//redirect('p/'.$productID.'/'.$productName,'refresh');			
		}
		//product data
		$product = $productprocess->get_product_and_media_by_id($productID);
		if($product==NULL||($product!=NULL && $product->Active==0)){
			die('Product not found');	
			//redirect('p/'.$productID.'/'.$productName,'refresh');			
		}
		if(sanitize_filename($product->Name)!=$productName)
			die('Product not found');
		$this->data['product'] = $product;
		$this->data['title'] = $product->Name;				
		$this->data['description'] = $product->ProductInfo;
		//vendor
		$vendor = $vendorprocess->load_vendor($product->VendorID);
		$this->data['vendor'] = $vendor;
		//more products
		$moreproducts = array();
		$ppaging = new Paging();
		$ppaging->iDisplayLength = 7;				
		$ppaging->iDisplayStart=0;
		$ppaging->sSortDir_0 = 'asc';
		$ppaging->iSortCol_0 = 'Name';
		if(!$ppaging->validate()){
			die('error validating product vendor '.$ppaging->error_messages());
		}else{			
			$pagingresult = $productprocess->get_active_products_and_media_by_vendor($ppaging,$product->VendorID);	
			
			if($pagingresult->data!=NULL){
				foreach($pagingresult->data as &$p){
					if($p->ProductID!=$productID){
						$moreproducts[] = $p;
					}
				}
			}		
		}	
		$this->data['moreproducts'] = $moreproducts;
		//recently viewed product
		$this->data['recentproducts'] = $productprocess->get_last_seen_products();
		//add to recently viewed
		$productprocess->add_last_seen_product($productID);
				
		$this->load->view('template',$this->data);		
	}
}
