<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gateway extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	function get_rows($sql,$prm){
		$query = $this->db->query($sql,$prm);
		echo $this->db->last_query(); 
		if ($query->num_rows() > 0)
		{
			return $query->result();
		}
		return NULL;
	}
	function execute($sql,$prm){
		$this->db->query($sql,$prm);
		//echo $this->db->last_query(); 
		if($this->db->affected_rows()>0)
			return TRUE;
		return FALSE;
	}
	function get_scalar($sql,$prm){
		$rcounts = $this->get_rows($sql,$prm);		
		$count = $rcounts[0];
		return $count->c;
	}
	function get_last_id(){
		return $this->db->insert_id();
	}
	function trans_start(){
		$this->db->trans_start();
	}
	function trans_complete(){
		$this->db->trans_complete();
	}
	function trans_status(){
		$this->db->trans_status();
	}
}
?>
