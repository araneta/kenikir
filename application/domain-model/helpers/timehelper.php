<?php
class TimeHelper{
	/*
	 * convert string time to utc string
	 * */
	public function convert_to_utc($localtime){
		$UTC = new DateTimeZone("UTC");
		$serverTZ = new DateTimeZone(date_default_timezone_get());
		$date = new DateTime( $localtime, $serverTZ);
		$date->setTimezone( $UTC );
		return $date->format('Y-m-d H:i:s');
	}	
	public function get_time_in_utc(){
		$utc = new DateTimeZone("UTC");
		$date = new DateTime();
		$date->setTimezone( $utc );
		return $date->format('Y-m-d H:i:s');
	}
	//return 2003 January 1
	public function convert_to_string_1($utctime){		
		$newTZ = new DateTimeZone(date_default_timezone_get());
		$date = new DateTime($utctime, $newTZ );
		return $date->format('Y F d');
	}
}
?>
