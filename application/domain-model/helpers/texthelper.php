<?php
class TextHelper{
	public function Cut($text,$maxchar){	
		$result = $text;	
		$ntext = strlen($text);
		if($ntext>$maxchar){
			$last = $maxchar;
			for($j=$maxchar;$j>0;$j--){
				if($text[$j]==' '){
					$last = $j;
					break;
				}
			}
			$result = substr($text,0,$last);
			$result .= " ...";
		}	
		return $result;
	}
	//from http://stackoverflow.com/a/2668953
	public function sanitize($string, $force_lowercase = true, $anal = false) {
		$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
					   "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
					   "â€”", "â€“", ",", "<", ".", ">", "/", "?");
		$clean = trim(str_replace($strip, "", strip_tags($string)));
		$clean = preg_replace('/\s+/', "-", $clean);
		$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;
		return ($force_lowercase) ?
			(function_exists('mb_strtolower')) ?
				mb_strtolower($clean, 'UTF-8') :
				strtolower($clean) :
			$clean;
	}
}
?>
