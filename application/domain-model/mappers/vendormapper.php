<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class VendorMapper extends BaseMapper{
	private $get_all_filter_sql = 'select * from vendor where VendorID like ? and PublicStatus=? order by %s %s limit ?,?';
	private $get_all_count_filter_sql = 'select count(*) as c from vendor where VendorID like ? and PublicStatus=?';
	
	private $get_all_sql = 'select * from vendor where PublicStatus=? order by %s %s limit ?,?';
	private $get_all_count_sql = 'select count(*) as c from vendor where PublicStatus=? ';
	
	private $check_email_sql = 'select count(*) as c from vendor where Email = ? or Email2 = ?';
	private $check_email_vendor_sql = 'select count(*) as c from vendor where (Email = ? or Email2 = ?) and VendorID!=?';
	
	private $add_vendor_sql = 'insert into vendor(VendorID, Password, Question1, Answer1, Question2, 
			Answer2, CompanyName, FirstName, LastName, Phone,
			Email, Address, City, State, Zip, 
			ImagePath, CreatedDate, Active, Hash, MediaID) 
			values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ';
			
	private $check_code_sql = 'select * from vendor where Email = ? and Hash = ?';
	
	//private $activate_vendor_sql = 'update vendor set Active = 1, UpdatedDate = ? where VendorID = ? and Active=0';
	
	private $get_vendor_info_sql = 'select VendorID,  Question1, Question2, Answer1,  
			Answer2, CompanyName, FirstName, LastName, Phone, Phone2,
			Email, Email2, URL, URL2, Address,Address2, City,City2, 
			State,State2, Zip, Zip2,PublicInfo, SubscribeNewsletter,ImagePath,Alias, Active, MediaID, PublicStatus
			from vendor where VendorID = ?';
	private $get_vendor_info_by_alias_sql = 'select VendorID,  Question1, Question2, Answer1,  
			Answer2, CompanyName, FirstName, LastName, Phone, Phone2,
			Email, Email2, URL, URL2, Address,Address2, City,City2, 
			State,State2, Zip, Zip2,PublicInfo, SubscribeNewsletter,ImagePath,Alias, Active, MediaID, PublicStatus
			from vendor where Alias = ?';
			
	private $check_login_sql = 'select VendorID,CompanyName,Alias,MediaID from vendor where VendorID = ? and Password = ? and Active = 1';	
	private $update_login_time_sql = 'update vendor set LogInDate = ?, UpdatedDate = ? where VendorID = ?';
	
	private $add_info_sql = 'update vendor set CompanyName = ?, ImagePath = ?, FirstName = ?, LastName = ?, Phone = ?, Phone2 = ?,
			Email = ?, Email2 = ?, URL = ?, URL2 = ?, Address = ?,Address2 = ?, City = ?,City2 = ?, 
			State = ?,State2 = ?, Zip = ?, Zip2 = ?,PublicInfo = ?, SubscribeNewsletter = ?,
			Active = ?, Alias = ?, PublicStatus = ?, 
			UpdatedDate = ?
			where VendorID = ?';
			
	private $update_info_sql = 'update vendor set CompanyName = ?, PublicInfo = ?, ImagePath = ?, FirstName = ?, LastName = ?, Phone = ?, Phone2 = ?,
			Email = ?, Email2 = ?, URL = ?, URL2 = ?, Address = ?,Address2 = ?, City = ?,City2 = ?, 
			State = ?,State2 = ?, Zip = ?, Zip2 = ?, SubscribeNewsletter = ?, NewEmail = ?, HashNewEmail =?,
			Alias = ?, UpdatedDate = ?
			where VendorID = ?';	
	private $update_security_info_sql = 'update vendor set %s Question1 = ?, Question2 = ?, Answer1 = ?,  
			Answer2 = ?, 
			UpdatedDate = ?
			where VendorID = ?';	
					
	private $load_vendor_sql = 'select * from vendor where VendorID=?';					
	
	private $check_code_email_sql = 'select * from vendor where NewEmail = ? and HashNewEmail = ?';
	private $activate_vendor_email_sql = 'update vendor set Email = ?, NewEmail=?, HashNewEmail=?, UpdatedDate = ? where VendorID = ?';
	
	private $check_security_questions_sql = 'select * from vendor where Email = ? and Answer1 = ? and Answer2 = ?';
	private $get_vendor_by_email_sql = 'select * from vendor where Email = ?';
	private $reset_password_sql = 'update vendor set Password=? ,UpdatedDate=? where VendorID = ?';
	
	private $check_existing_company_name_sql = 'select count(*) as c from vendor where CompanyName = ? and VendorID != ?';
	
	private $set_vendor_id_sql = 'update vendor set VendorID=? where VendorID=?';
	
	//vendor product and video
	private $add_vendor_video_sql = 'insert into vendorvideos(VendorID,Title, VideoFileName)
			values(?,?,?)
			';				
	private $delete_vendor_videos_sql = 'delete from vendorvideos where VendorID = ?';
	
	private $add_vendor_doc_sql = 'insert into vendordocs(VendorID,Title, FileName)
			values(?,?,?)
			';			
	private $delete_vendor_docs_sql = 'delete from vendordocs where VendorID = ?';	
	private $get_vendor_videos_sql = 'select * from vendorvideos where VendorID = ?';
	private $get_vendor_docs_sql = 'select * from vendordocs where VendorID = ?';
	private $delete_vendor_video_sql = 'delete from vendorvideos where VendorID=? and VideoFileName=?';
	private $delete_vendor_doc_sql = 'delete from vendordocs where VendorID=? and FileName=?';
	private $delete_vendor_image_sql = 'update vendor set ImagePath=NULL where VendorID=? and ImagePath=?';
	
	private $update_vendor_public_status_sql = 'update vendor set PublicStatus=?, UpdatedDate = ? where VendorID=?';
	
	public function __construct($gt){
		parent::__construct($gt);
	}
	
	public function get_all($paging,$active){
		
		$res = new PagingResult();
		
		if(empty($paging->filter)){
			$sql = sprintf($this->get_all_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array($active,$paging->start,$paging->pagesize));
			
			$res->totalrecords = intval($this->get_scalar($this->get_all_count_sql,array($active)));
		}else{
			$sql = sprintf($this->get_all_filter_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array('%'.$paging->filter.'%',$active,$paging->start,$paging->pagesize));
			
			$res->totalrecords = intval($this->get_scalar($this->get_all_count_sql,array('%'.$paging->filter.'%',$active)));
		}		
		
		$res->totaldisplayrecords = count($rows);
		$res->data = array();
		if($rows!=NULL){
			foreach($rows as $r){
				$v = new Vendor();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}
	public function email_exists($email){
		if(intval($this->get_scalar($this->check_email_sql,array($email,$email)))>0)
			return TRUE;
		return FALSE;	
	}
	public function add_vendor($v){
		$prm = array($v->VendorID, $v->Password, $v->Question1, $v->Answer1, $v->Question2,
				 $v->Answer2, $v->CompanyName, $v->FirstName, $v->LastName, $v->Phone, 
				 $v->Email, $v->Address, $v->City, $v->State, $v->Zip, 
				$v->ImagePath, $v->CreatedDate, $v->Active, $v->Hash, $v->MediaID
		);
		return $this->execute($this->add_vendor_sql,$prm);
	}
	//return if code n email is match and still inactive
	public function check_code($VendorID,$code){
		$rows = $this->get_rows($this->check_code_sql,array($VendorID,$code));
		if($rows==NULL)
			return NULL;
		$v = new Vendor();
		$v->bind($rows[0]);
		return $v;	
	}
	/*
	public function activate($v){
		return $this->execute($this->activate_vendor_sql,array($v->UpdatedDate,$v->VendorID));
	}*/
	public function get_vendor_info_by_alias($vendor_alias){
		$rows = $this->get_rows($this->get_vendor_info_by_alias_sql,array($vendor_alias));
		if($rows==NULL)
			return NULL;
		$r = $rows[0];	
		$v = new Vendor();
		$v->bind($r);
		return $v;	
	}
	public function get_vendor_info($vendor_id){
		$rows = $this->get_rows($this->get_vendor_info_sql,array($vendor_id));
		if($rows==NULL)
			return NULL;
		$r = $rows[0];	
		$v = new Vendor();
		$v->bind($r);
		return $v;	
	}
	public function get_vendor_info_and_media($vendor_id){
		$v = $this->get_vendor_info($vendor_id);
		if($v==NULL)
			return NULL;
		return $this->get_vendor_media($v);	
	}
	public function check_login($email,$password){
		$rows = $this->get_rows($this->check_login_sql,array($email,$password));
		if($rows==NULL)
			return FALSE;
		$v = new Vendor();
		$v->bind($rows[0]);
		return $v;			
	}
	public function update_login_time($vendor){
		$data = array($vendor->LogInDate,$vendor->UpdatedDate,$vendor->VendorID);
		return $this->execute($this->update_login_time_sql,$data);
	}
	public function add_info($v){
		$data = array($v->CompanyName, $v->ImagePath, $v->FirstName, $v->LastName, $v->Phone, $v->Phone2,
			$v->Email, $v->Email2, $v->URL, $v->URL2, $v->Address, $v->Address2, $v->City, $v->City2, 
			$v->State, $v->State2, $v->Zip, $v->Zip2, $v->PublicInfo, $v->SubscribeNewsletter, 
			$v->Active, $v->Alias, $v->PublicInfo, $v->UpdatedDate,
			$v->VendorID
		); 
		$this->execute($this->add_info_sql,$data);
		$this->trans_start();		
		//delete vid and doc			
		$this->save_video_and_doc($v);
		$this->trans_complete();
		
		if ($this->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}
	private function save_video_and_doc($v){
		$ret = TRUE;
		//add product video
		$n = count($v->VideoFileName);
		for($i=0;$i<$n;$i++){
			$video = $v->VideoFileName[$i];
			$videotitle = $v->VideoTitle[$i];				
			if(!empty($video)&&!empty($videotitle)){
				$vrmvid =array($v->VendorID, $videotitle,$video);
				$ret &= $this->execute($this->add_vendor_video_sql,$vrmvid);
			}
		}
		//add product doc
		$n = count($v->DocFileName);
		for($i=0;$i<$n;$i++){
			$doc = $v->DocFileName[$i];
			$doctitle = $v->DocTitle[$i];
			if(!empty($doc)&&!empty($doctitle)){
				$vrmdoc =array($v->VendorID, $doctitle,$doc);
				$ret &= $this->execute($this->add_vendor_doc_sql,$vrmdoc);
			}
		}
		return $ret;
	}
	public function update_info($v){
		$data = array($v->CompanyName,$v->PublicInfo, $v->ImagePath, $v->FirstName, $v->LastName, $v->Phone, $v->Phone2,
			$v->Email, $v->Email2, $v->URL, $v->URL2, $v->Address, $v->Address2, $v->City, $v->City2, 
			$v->State, $v->State2, $v->Zip, $v->Zip2, $v->SubscribeNewsletter,$v->NewEmail,$v->HashNewEmail,
			$v->Alias, $v->UpdatedDate,
			$v->VendorID
		); 
		$this->trans_start();
		$this->execute($this->update_info_sql,$data);
		//delete vid and doc			
		$this->execute($this->delete_vendor_videos_sql,array($v->VendorID));
		$this->execute($this->delete_vendor_docs_sql,array($v->VendorID));
		$this->save_video_and_doc($v);
		$this->trans_complete();
		
		if ($this->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}
	public function update_security_info($v){
		$data = array($v->Question1, $v->Question2, $v->Answer1, $v->Answer2, 
			$v->UpdatedDate,
			$v->VendorID
		); 
		$pass = '';
		if(!empty($v->Password))
		{
			$pass = 'Password=?,';	
			array_unshift($data,$v->Password);
		}
		$sql = sprintf($this->update_security_info_sql,$pass);		
		
		return $this->execute($sql,$data);
	}
	public function email_used_by_others($email,$vendorID){
		if(intval($this->get_scalar($this->check_email_vendor_sql,array($email,$email,$vendorID)))>0)
			return TRUE;
		return FALSE;	
	}
	public function load_vendor($VendorID){
		$rows = $this->get_rows($this->load_vendor_sql,array($VendorID));
		if($rows==NULL)
			return NULL;
		$row = $rows[0];
		$v = new Vendor();
		$v->bind($row);
		return $v;	
	}
	//primary email activation
	public function check_code_email($email,$code){
		$rows = $this->get_rows($this->check_code_email_sql,array($email,$code));
		if($rows==NULL)
			return NULL;
		$v = new Vendor();
		$v->bind($rows[0]);
		return $v;	
	}
	public function activate_new_email($v){
		return $this->execute($this->activate_vendor_email_sql,array($v->Email,$v->NewEmail,$v->HashNewEmail,$v->UpdatedDate,$v->VendorID));
	}
	public function get_vendor_by_email($email){
		$rows = $this->get_rows($this->get_vendor_by_email_sql,array($email));
		if($rows==NULL)
			return NULL;
		$v = new Vendor();
		$v->bind($rows[0]);
		return $v;	
	}
	public function check_security_questions($f){
		$rows = $this->get_rows($this->check_security_questions_sql,array($f->PrimaryEmail,$f->Answer1,$f->Answer2));
		if($rows==NULL)
			return NULL;
		$v = new Vendor();
		$v->bind($rows[0]);
		return $v;	
	}
	public function reset_password($v){
		return $this->execute($this->reset_password_sql,array($v->Password,$v->UpdatedDate,$v->VendorID));
	}
	//return true if company name is exist and used by other vendor
	public function check_existing_company_name($name,$vendorID){
		$n = $this->get_scalar($this->check_existing_company_name_sql,array($name,$vendorID));
		
		if($n>0)
			return TRUE;
		return FALSE;
	}
	public function set_vendor_id($oldVendorID,$newVendorID){
		return $this->execute($this->set_vendor_id_sql,array($newVendorID,$oldVendorID));
	}
	public function get_vendor_media($v){
		//get video
		$rows = $this->get_rows($this->get_vendor_videos_sql,array($v->VendorID));
		if($rows!=NULL){
			$v->VideoTitle = array();
			$v->VideoFileName = array();			
			foreach($rows as $r){
				array_push($v->VideoTitle,$r->Title);
				array_push($v->VideoFileName,$r->VideoFileName);
			}			
		}
		//get doc
		$rows = $this->get_rows($this->get_vendor_docs_sql,array($v->VendorID));
		if($rows!=NULL){
			$v->DocTitle = array();
			$v->DocFileName = array();			
			foreach($rows as $r){
				array_push($v->DocTitle,$r->Title);
				array_push($v->DocFileName,$r->FileName);
			}			
		}
		return $v;
	}
	public function delete_vendor_video($VendorID,$filename){
		return $this->execute($this->delete_vendor_video_sql,array($VendorID,$filename));
	}
	public function delete_vendor_doc($VendorID,$filename){
		return $this->execute($this->delete_vendor_doc_sql,array($VendorID,$filename));
	}
	public function delete_vendor_image($VendorID,$filename){
		return $this->execute($this->delete_vendor_image_sql,array($VendorID,$filename));
	}
	public function update_public_status($vendor){
		$data = array($vendor->PublicStatus,$vendor->UpdatedDate,$vendor->VendorID);
		return $this->execute($this->update_vendor_public_status_sql,$data);
	}
}
?>
