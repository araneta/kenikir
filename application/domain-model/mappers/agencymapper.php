<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class AgencyMapper extends BaseMapper{
		
	private $get_all_by_alphabet_sql = 'select AgencyName from memberagency order by AgencyName asc';	
	private $get_agency_by_name_sql = 'select * from memberagency where AgencyName=?';
	private $add_agency_sql = 'insert into memberagency(AgencyType,AgencyName,SubAgency,MemberSince,Active)
			values (?,?,?,?,?)';
	
	public function __construct($gt){
		parent::__construct($gt);		
	}
		
	public function get_all_by_alphabet(){
		$rows = $this->get_rows($this->get_all_by_alphabet_sql,null);
		$ret = array();
		if($rows!=NULL){			
			$curkey = '';
			foreach($rows as $agency){
				$name = $agency->AgencyName;
				$a = strtoupper(substr($name,0,1));
				if($curkey != $a){
					$ret[$a] = array();
					$curkey = $a;
				}
				array_push($ret[$a],$name);
			}			
		}
		return $ret;
	}
	public function get_agency_by_name($name){
		$rows = $this->get_rows($this->get_agency_by_name_sql,array($name));
		$ret = array();
		if($rows==NULL)
			return NULL;
		
		$row = $rows[0];
		$agency = new MemberAgency();
		$agency->bind($row);
		return $agency;
		
	}
	public function add_agency($agency){
		$data = array($agency->AgencyType,$agency->AgencyName,
			$agency->SubAgency,$agency->MemberSince,$agency->Active);
		$ret = $this->execute($this->add_agency_sql,$data);	
		if(!$ret)
			return FALSE;
		$agency->AgencyID = $this->get_last_id();
		return TRUE;	
	}
}
?>
