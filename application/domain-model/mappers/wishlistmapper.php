<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class WishListMapper extends BaseMapper{
	
	private $add_wishlist_sql = 'insert into wishlist (Name,MemberID,Access,Password,CustomerNotes,CreatedDate) values(?,?,?,?,?,?)';
	private $get_wishlist_by_member_sql = 'select * from wishlist where MemberID=? order by %s %s limit ?,?';
	private $get_all_wishlist_by_member_sql = 'SELECT WishlistID,Name, (SELECT COUNT( * ) FROM wishlistitems WHERE wishlistitems.WishlistID = WishlistID) AS cc 
	FROM wishlist where MemberID=? order by CreatedDate desc';
	private $get_wishlist_by_member_count_sql = 'select count(*) as c from wishlist where MemberID=? ';
	private $get_default_wishlist_by_member_sql = 'select * from wishlist where MemberID=? limit 1';
	private $add_product_to_wishlist_sql = 'insert into wishlistitems(WishlistID,ProductID,MemberNotes,AdminNotes,DateAdded) values(?,?,?,?,?)';
	private $get_last_item_sql = 'select product.ProductID, Name 
		from wishlistitems
		inner join product on wishlistitems.ProductID = product.ProductID
		where WishlistID=? order by wishlistitems.DateAdded desc limit 1';
	private $get_wishlist_sql = 'select * from wishlist where WishlistID=? and MemberID=?';	
	private $update_wishlist_sql = 'update wishlist set Name=?, Password=?,CustomerNotes=?,LastUpdated=?
		where WishlistID=? and MemberID=?';
		
	private $get_wishlist_products_sql = 'select product.ProductID, product.Name,
		vendor.VendorID, vendor.CompanyName, wishlistitems.DateAdded,wishlistitems.ItemID
		from wishlistitems
		inner join product on wishlistitems.ProductID = product.ProductID
		inner join vendor on product.VendorID = vendor.VendorID	
		inner join wishlist on wishlistitems.WishlistID = wishlist.WishlistID
		where wishlistitems.WishlistID=? and wishlist.MemberID=? order by %s %s limit ?,?';
	private $get_wishlist_products_count_sql = 'select count(*) as c 
		from wishlistitems
		inner join product on wishlistitems.ProductID = product.ProductID
		inner join vendor on product.VendorID = vendor.VendorID
		inner join wishlist on wishlistitems.WishlistID = wishlist.WishlistID
		where wishlistitems.WishlistID=? and wishlist.MemberID=?';
	private $get_wishlist_by_item_sql = 'select wishlist.* from wishlist 
		inner join wishlistitems on wishlist.WishlistID = wishlistitems.WishlistID
		where ItemID =? and MemberID=?';
	private $delete_item_sql = 'delete from wishlistitems where ItemID=?';	
			
	public function add_wishlist($wish){
		$data = array($wish->Name,$wish->MemberID,$wish->Access,$wish->Password,$wish->CustomerNotes,$wish->CreatedDate);
		return $this->execute($this->add_wishlist_sql,$data);	
	}
	public function get_wishlist_by_member($paging,$memberID){
		$res = new PagingResult();
			
		$sql = sprintf($this->get_wishlist_by_member_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($memberID,$paging->start,$paging->pagesize));
		
		$res->totalrecords = intval($this->get_scalar($this->get_wishlist_by_member_count_sql,array($memberID)));
		
		$res->totaldisplayrecords = count($rows);
		$res->data = array();
		if($rows!=NULL){
			foreach($rows as $r){
				$v = new WishList();
				$v->bind($r);
				$last = $this->get_last_item($v->WishlistID);
				if($last!=NULL){
					$v->LastItem = $last;
				}
				array_push($res->data,$v);
			}	
		}
		return $res;	
	}
	public function get_default_wishlist($memberID){
		$rows = $this->get_rows($this->get_default_wishlist_by_member_sql,array($memberID));
		if($rows==NULL){
			return NULL;
		}
		$w = new WishList();
		$w->bind($rows[0]);
		return $w;
	}
	public function add_product_to_wishlist($wishlistitem){
		$data = array($wishlistitem->WishlistID,$wishlistitem->ProductID,$wishlistitem->MemberNotes,$wishlistitem->AdminNotes,$wishlistitem->DateAdded);	
		return $this->execute($this->add_product_to_wishlist_sql,$data);
	}
	
	public function get_last_item($wishlistID){
		$rows = $this->get_rows($this->get_last_item_sql,array($wishlistID));
		if($rows==NULL)
			return NULL;
		return $rows[0];	
	}
	public function get_wishlist($wishlistID,$memberID){
		$rows = $this->get_rows($this->get_wishlist_sql,array($wishlistID,$memberID));
		if($rows==NULL)
			return NULL;
		$w = new WishList();
		$w->bind($rows[0]);
		return $w;		
	}
	public function update_wishlist($f){
		$data = array($f->Name,$f->Password,$f->CustomerNotes,$f->LastUpdated,$f->WishlistID,$f->MemberID);
		return $this->execute($this->update_wishlist_sql,$data);
	}
	public function get_wishlist_products($paging,$wishlistID,$memberID){
		$res = new PagingResult();
			
		$sql = sprintf($this->get_wishlist_products_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($wishlistID, $memberID, $paging->start,$paging->pagesize));
		
		$res->totalrecords = intval($this->get_scalar($this->get_wishlist_products_count_sql,array($wishlistID, $memberID)));
		
		$res->totaldisplayrecords = count($rows);
		$res->data = array();
		if($rows!=NULL){
			foreach($rows as $r){
				$v = new WishList();
				$v->bind($r);
				$last = $this->get_last_item($v->WishlistID);
				if($last!=NULL){
					$v->LastItem = $last;
				}
				array_push($res->data,$v);
			}	
		}
		return $res;	
	}
	public function get_wishlist_by_item($ItemID,$memberID){
		$rows = $this->get_rows($this->get_wishlist_by_item_sql,array($ItemID,$memberID));
		if($rows==NULL){
			return NULL;
		}
		$w = new WishList();
		$w->bind($rows[0]);
		return $w;
	}
	public function delete_item($itemID){
		return $this->execute($this->delete_item_sql,array($itemID));
	}
	public function get_wishlist_count_by_member($memberID){
		$rows = $this->get_rows($this->get_all_wishlist_by_member_sql,array($memberID));
		if($rows==NULL)
			return NULL;
		$ret = array();	
		foreach($rows as $row){
			$c = new WishListCount();
			$c->WishlistID = $row->WishlistID;
			$c->Name = $row->Name;
			$c->WishlistItemCount = $row->cc;
			array_push($ret,$c);
		}	
		return $ret;
	}
}
