<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class AdminMapper extends BaseMapper{
	private $find_login_sql = 'select AdminID from admin where AdminID = ? and Password = ? and Active = 1';
	private $update_login_time_sql = 'update admin set LogInDate = ?, UpdatedDate = ? where AdminID = ?';
	private $get_admin_email_sql = 'select Email as c from admin where Active=1 and Email is not null limit 1';
	public function __construct($gt){
		parent::__construct($gt);
	}
	public function find_admin($u,$p){
		$rows = $this->get_rows($this->find_login_sql,array($u,$p));
		if($rows==NULL)
			return NULL;
		$admin = new Admin();
		$admin->bind($rows[0]);
		return $admin;	
	}
	public function update_login_time($admin){
		$data = array($admin->LogInDate,$admin->UpdatedDate,$admin->AdminID);
		return $this->execute($this->update_login_time_sql,$data);
	}
	public function get_admin_email(){
		return $this->get_scalar($this->get_admin_email_sql,NULL);			
	}
}
?>
