<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaseMapper{
	//table gateway
	protected $gt;
	function __construct($gt){
		$this->gt = $gt;
	}	
	function get_rows($sql,$prm){
		return $this->gt->get_rows($sql,$prm);
	}
	function execute($sql,$prm){
		return $this->gt->execute($sql,$prm);
	}
	function get_scalar($sql,$prm){
		return $this->gt->get_scalar($sql,$prm);
	}
	function get_last_id(){
		return $this->gt->get_last_id();
	}
	function trans_start(){
		$this->gt->trans_start();
	}
	function trans_complete(){
		$this->gt->trans_complete();
	}
	function trans_status(){
		$this->gt->trans_status();
	}
}
?>
