<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class RememberMeMapper extends BaseMapper{
	protected $columnid;
	protected $tablename;
	
	private $get_id_by_token_sql = 'select %s as c from %s where RememberMeToken = ? and Active=1';
	private $set_id_token_sql = 'update %s set RememberMeToken = ? where %s = ?';
	
	public function __construct($gt){
		parent::__construct($gt);
	}
	public function check_token($token){
		$sql = sprintf($this->get_id_by_token_sql,$this->columnid, $this->tablename);
		//echo $sql;
		$ids = $this->get_rows($sql,array($token));
		if($ids!=NULL){
			$id = $ids[0];
			return $id->c;
		}
		//echo 'res'.$id;	
		return NULL;
	}
	public function set_table($tbl,$colid){
		$this->tablename = $tbl;
		$this->columnid = $colid;
	}
	public function save_token($id,$token){
		$sql = sprintf($this->set_id_token_sql, $this->tablename, $this->columnid);
		return $this->execute($sql, array($token,$id)); 
	}
	
}
