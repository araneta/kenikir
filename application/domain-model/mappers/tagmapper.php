<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';

class TagMapper extends BaseMapper{
	private $SOLUTION_TAG_TYPE =  0;
	private $FEDERAL_TAG_TYPE = 1;		
	private $get_all_tag_by_type_sql = 'select tagvalues.TagValueID, tagvalues.CreatedDate
		from tagvalues
		where tagvalues.Type = ?
		order by %s %s limit ?,?';
	private $get_all_tag_by_type_count_sql = 'select count(*) as c 
		from tag 
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = ?';		
	private $add_tag_sql = 'insert into tagvalues(TagValueID,Type,CreatedDate) values(?,?,?)';
	private $update_tag_sql = 'update tagvalues set TagValueID=? where TagValueID=? and Type=?';
	private $delete_tag_sql = 'delete from tagvalues where TagValueID=? and Type=?';
		
	public function __construct($gt){
		parent::__construct($gt);	
	}
	
	public function get_all_tag($type,$paging){
		$res = new PagingResult();				
		$sql = sprintf($this->get_all_tag_by_type_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($type,$paging->start,$paging->pagesize));			
		$res->totalrecords = intval($this->get_scalar($this->get_all_tag_by_type_count_sql,array($type,)));				
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new TagValues();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;
	}
	public function get_all_solution($paging){		
		return $this->get_all_tag($this->SOLUTION_TAG_TYPE,$paging);	
	}
	public function get_all_federal($paging){		
		return $this->get_all_tag($this->FEDERAL_TAG_TYPE,$paging);
	}
	public function add_solution_tag($tag){
		return $this->execute($this->add_tag_sql,array($tag->TagValueID,$this->SOLUTION_TAG_TYPE,$tag->CreatedDate));
	}
	public function update_solution_tag($oritag,$newtag){
		return $this->execute($this->update_tag_sql,array($newtag,$oritag,$this->SOLUTION_TAG_TYPE));
	}
	public function delete_solution_tag($tag){
		return $this->execute($this->delete_tag_sql,array($tag,$this->SOLUTION_TAG_TYPE));
	}
	public function add_federal_tag($tag){
		return $this->execute($this->add_tag_sql,array($tag->TagValueID,$this->FEDERAL_TAG_TYPE,$tag->CreatedDate));
	}
	public function update_federal_tag($oritag,$newtag){
		return $this->execute($this->update_tag_sql,array($newtag,$oritag,$this->FEDERAL_TAG_TYPE));
	}
	public function delete_federal_tag($tag){
		return $this->execute($this->delete_tag_sql,array($tag,$this->FEDERAL_TAG_TYPE));
	}
}
?>
