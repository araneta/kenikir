<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
//this class is separated because we need to optimize it later using cache or other memory db
class SearchProductMapper extends BaseMapper{
	private $SOLUTION_TAG_TYPE =  0;
	private $FEDERAL_TAG_TYPE = 1;	
	private $search_product_sql = 'select distinct(product.ProductID), product.Name, vendor.CompanyName, 
		product.ProductInfo, vendor.Alias, vendor.MediaID,
		(select FileName from productimages where productimages.ProductID=product.ProductID limit 1) as ProductImage		
		from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where (%s)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		order by %s %s limit ?,?';
	private $search_product_count_sql = 'select count(distinct(product.ProductID)) as c 
		from product 
		inner join vendor on product.VendorID = vendor.VendorID
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where (%s)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1 ';
	private $search_product_filter_sql = 'select product.ProductID, product.Name, vendor.Alias,vendor.CompanyName,
		tag.TagValueID,tagvalues.Type
		from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where (product.Name like ? or tag.TagValueID like ? or vendor.CompanyName like ?)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1';	
	private $search_product_by_vendor_sql ='select distinct(product.ProductID), product.Name, vendor.CompanyName, 
		product.ProductInfo, vendor.Alias, vendor.MediaID,
		(select FileName from productimages where productimages.ProductID=product.ProductID limit 1) as ProductImage		
		from product 
		inner join vendor on product.VendorID = vendor.VendorID		
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where  %s vendor.CompanyName like ?
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		order by %s %s limit ?,?';	
	private $search_product_by_vendor_count_sql ='select count(distinct(product.ProductID)) as c
		from product 		
		inner join vendor on product.VendorID = vendor.VendorID		
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where %s vendor.CompanyName like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		';		
	private $search_product_by_vendor_filter_sql = 'select product.ProductID, product.Name, vendor.Alias,vendor.CompanyName,
		tag.TagValueID,tagvalues.Type
		from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where (vendor.CompanyName like ?)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1';		
	private $search_product_by_solution_sql ='select distinct(product.ProductID), product.Name, vendor.CompanyName, 
		product.ProductInfo, vendor.Alias, vendor.MediaID,
		(select FileName from productimages where productimages.ProductID=product.ProductID limit 1) as ProductImage
		from product 
		inner join vendor on product.VendorID = vendor.VendorID		
		inner join tag on product.ProductID = tag.ProductID
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = 0 %s 
		and tag.TagValueID like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		order by %s %s limit ?,?';		
	private $search_product_by_solution_count_sql ='select count(distinct(product.ProductID)) as c
		from product 		
		inner join vendor on product.VendorID = vendor.VendorID		
		inner join tag on product.ProductID = tag.ProductID
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = 0 %s
		and tag.TagValueID like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		';	
	private $search_product_by_solution_filter_sql = 'select product.ProductID, product.Name, vendor.Alias,vendor.CompanyName,
		tag.TagValueID,tagvalues.Type
		from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where 
		(tag.TagValueID like ?)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1';		
	private $search_product_by_federal_sql ='select distinct(product.ProductID), product.Name, vendor.CompanyName, 
		product.ProductInfo, vendor.Alias, vendor.MediaID,
		(select FileName from productimages where productimages.ProductID=product.ProductID limit 1) as ProductImage		
		from product 
		inner join vendor on product.VendorID = vendor.VendorID		
		inner join tag on product.ProductID = tag.ProductID
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = 1 %s
		and tag.TagValueID like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		order by %s %s limit ?,?';		
	private $search_product_by_federal_count_sql ='select count(distinct(product.ProductID)) as c
		from product 
		inner join vendor on product.VendorID = vendor.VendorID
		inner join tag on product.ProductID = tag.ProductID
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = 1 %s
		and tag.TagValueID like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		';		
	private $search_product_by_federal_filter_sql = 'select product.ProductID, product.Name, vendor.Alias,vendor.CompanyName,
		tag.TagValueID,tagvalues.Type
		from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where 
		(tag.TagValueID like ?)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1';	
			
	private $search_product_by_product_sql = 'select distinct(product.ProductID), product.Name, vendor.CompanyName, 
		product.ProductInfo, vendor.Alias, vendor.MediaID,
		(select FileName from productimages where productimages.ProductID=product.ProductID limit 1) as ProductImage		
		from product 
		inner join vendor on product.VendorID = vendor.VendorID		
		inner join tag on product.ProductID = tag.ProductID
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where %s product.Name like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		order by %s %s limit ?,?';	
	private $search_product_by_product_count_sql = 'select count(distinct(product.ProductID)) as c
		from product 
		inner join vendor on product.VendorID = vendor.VendorID		
		inner join tag on product.ProductID = tag.ProductID
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where %s product.Name like ? 
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		';	
	private $search_product_by_product_filter_sql = 'select product.ProductID, product.Name, vendor.Alias,vendor.CompanyName,
		tag.TagValueID,tagvalues.Type
		from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where (product.Name like ?)
		and product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1';		
				
	private $get_all_vendor_name_sql = 'select Alias as k,CompanyName as v from vendor where Active=1 and vendor.PublicStatus = 1 order by %s %s limit ?,?';
	private $get_all_vendor_name_count_sql = 'select count(*) as c from vendor where Active=1 and vendor.PublicStatus = 1';		
	private $get_all_solution_name_sql = 'select tagvalues.TagValueID as k, tagvalues.TagValueID as v
		from tagvalues
		where tagvalues.Type = 0
		order by %s %s limit ?,?';
	private $get_all_solution_name_count_sql = 'select count(*) as c 
		from tag 
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = 0';		
	private $get_all_federal_name_sql = 'select tagvalues.TagValueID as k, tagvalues.TagValueID as v
		from tagvalues
		where tagvalues.Type = 1 
		order by %s %s limit ?,?';
	private $get_all_federal_name_count_sql = 'select count(*) as c 
		from tag 
		inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
		where tagvalues.Type = 1';		
	private $get_all_product_name_sql = 'select product.ProductID as k, product.Name as v
		from product 			
		inner join vendor on product.VendorID = vendor.VendorID	
		where product.Active=1 and vendor.Active=1	and vendor.PublicStatus = 1 
		order by %s %s limit ?,?';
	private $get_all_product_name_count_sql = 'select count(*) as c 
		from product 
		inner join vendor on product.VendorID = vendor.VendorID	
		where product.Active=1 and vendor.Active=1 and vendor.PublicStatus = 1
		';	
	
	public function __construct($gt){
		parent::__construct($gt);	
	}
	/*deprecated
	public function search_product($paging,$filter){
		var_dump($paging);
		$res = new PagingResult();
		$q = '%'.$paging->filter.'%';	
		$rows = NULL;
		if($filter=='all'){
			$sql = sprintf($this->search_product_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array($q,$q,$q,$paging->start,$paging->pagesize));	
			$res->totalrecords = intval($this->get_scalar($this->search_product_count_sql,array($q,$q,$q)));
		}else if($filter=='vendor'){
			$sql = sprintf($this->search_product_by_vendor_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array($q,$paging->start,$paging->pagesize));	
			$res->totalrecords = intval($this->get_scalar($this->search_product_by_vendor_count_sql,array($q)));
				
		}else if($filter=='solutions'){
			$sql = sprintf($this->search_product_by_solution_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array($q,$paging->start,$paging->pagesize));	
			$res->totalrecords = intval($this->get_scalar($this->search_product_by_solution_count_sql,array($q)));		
		}else if($filter=='product'){
			$sql = sprintf($this->search_product_by_product_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array($q,$paging->start,$paging->pagesize));	
			$res->totalrecords = intval($this->get_scalar($this->search_product_by_product_count_sql,array($q)));
		}else if($filter=='federal'){
			$sql = sprintf($this->search_product_by_federal_sql,$paging->sortby,$paging->order);
			$rows = $this->get_rows($sql,array($q,$paging->start,$paging->pagesize));	
			$res->totalrecords = intval($this->get_scalar($this->search_product_by_federal_count_sql,array($q)));
		}
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new SearchProductResult();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}*/
	public function search_product_with_filter($paging,$filter,$filter2){		
		$res = new PagingResult();
		$q = '%'.$paging->filter.'%';	
		$rows = NULL;
		if($filter=='all'){		
			$prm = array();
			$prm2 = array();
			$criteria = '';
			$q2 = '';
			
			if(empty($paging->filter2)){
				$criteria = ' product.Name like ? 
				or tag.TagValueID like ? 
				or vendor.CompanyName like ? ';
				$prm = array($q,$q,$q,$paging->start,$paging->pagesize);
				$prm2 = array($q,$q,$q);	
			}else{				
				$prm = array($q,$q,$paging->start,$paging->pagesize);
				$prm2 = array($q,$q);
				if($filter2=='solutions')
					$criteria = ' tag.TagValueID like ? and tagvalues.Type = '.$this->SOLUTION_TAG_TYPE 
					. ' and (product.Name like ? or vendor.CompanyName like ?)'	;				
				if($filter2=='vendor')
					$criteria = ' vendor.CompanyName like ? '		
					.' and (tag.TagValueID like ? or product.Name like ?)';	
				if($filter2=='product')
					$criteria = ' product.Name like ? '		
					.' and (tag.TagValueID like ? or vendor.CompanyName like ?)';		
				if($filter2=='federal')
					$criteria = ' tagvalues.Type = '.$this->FEDERAL_TAG_TYPE.' and tag.TagValueID like ? '
					. ' and (product.Name like ? or vendor.CompanyName like ?)'	;				
				$q2 = '%'.$paging->sSearch2.'%';							
				array_unshift($prm,$q2);
				array_unshift($prm2,$q2);
			}
			$sql = sprintf($this->search_product_sql,$criteria,$paging->sortby,$paging->order);			
			$sql2 = sprintf($this->search_product_count_sql,$criteria);
			$rows = $this->get_rows($sql,$prm);	
			$res->totalrecords = intval($this->get_scalar($sql2,$prm2)); 
		}else if($filter=='vendor'){			
			$prm = array($q,$paging->start,$paging->pagesize);
			$prm2 = array($q);
			$criteria = '';
			$q2 = '';
			if(!empty($paging->filter2)){
				if($filter2=='solutions')
					$criteria = ' tag.TagValueID like ? and tagvalues.Type = '.$this->SOLUTION_TAG_TYPE.' and ';				
				if($filter2=='vendor')
					$criteria = ' vendor.CompanyName like ? and ';				
				if($filter2=='product')
					$criteria = ' product.Name like ? and ';											
				if($filter2=='federal')
					$criteria = ' tagvalues.Type = '.$this->FEDERAL_TAG_TYPE.' and tag.TagValueID like ? and ';					
				$q2 = '%'.$paging->sSearch2.'%';							
				array_unshift($prm,$q2);
				array_unshift($prm2,$q2);
			}
			$sql = sprintf($this->search_product_by_vendor_sql,$criteria,$paging->sortby,$paging->order);			
			$sql2 = sprintf($this->search_product_by_vendor_count_sql,$criteria);
			$rows = $this->get_rows($sql,$prm);	
			$res->totalrecords = intval($this->get_scalar($sql2,$prm2));	
		}else if($filter=='solutions'){
			$prm = array($q,$paging->start,$paging->pagesize);
			$prm2 = array($q);
			$criteria = '';
			$q2 = '';
			if(!empty($paging->filter2)){
				if($filter2=='solutions')
					$criteria = ' and tag.TagValueID like ?';				
				if($filter2=='vendor')
					$criteria = ' and vendor.CompanyName like ?';				
				if($filter2=='product')
					$criteria = ' and product.Name like ? ';																
				if($filter2=='federal')
					$criteria = ' or tagvalues.Type = 1 and tag.TagValueID like ? ';					
				$q2 = '%'.$paging->sSearch2.'%';							
				array_unshift($prm,$q2);
				array_unshift($prm2,$q2);
			}
			$sql = sprintf($this->search_product_by_solution_sql,$criteria,$paging->sortby,$paging->order);			
			$sql2 = sprintf($this->search_product_by_solution_count_sql,$criteria);
			$rows = $this->get_rows($sql,$prm);	
			$res->totalrecords = intval($this->get_scalar($sql2,$prm2));
		}else if($filter=='product'){			
			$prm = array($q,$paging->start,$paging->pagesize);
			$prm2 = array($q);
			$criteria = '';
			$q2 = '';
			if(!empty($paging->filter2)){
				if($filter2=='solutions')
					$criteria = ' tag.TagValueID like ? and tagvalues.Type = '.$this->SOLUTION_TAG_TYPE.' and ';				
				if($filter2=='vendor')
					$criteria = ' vendor.CompanyName like ? and ';				
				if($filter2=='product')
					$criteria = ' product.Name like ? and ';											
				if($filter2=='federal')
					$criteria = ' tagvalues.Type = '.$this->FEDERAL_TAG_TYPE.' and tag.TagValueID like ? and ';					
				$q2 = '%'.$paging->sSearch2.'%';							
				array_unshift($prm,$q2);
				array_unshift($prm2,$q2);
			}
			$sql = sprintf($this->search_product_by_product_sql,$criteria,$paging->sortby,$paging->order);			
			$sql2 = sprintf($this->search_product_by_product_count_sql,$criteria);
			$rows = $this->get_rows($sql,$prm);	
			$res->totalrecords = intval($this->get_scalar($sql2,$prm2)); 
		}else if($filter=='federal'){			
			$prm = array($q,$paging->start,$paging->pagesize);
			$prm2 = array($q);
			$criteria = '';
			$q2 = '';
			if(!empty($paging->filter2)){
				if($filter2=='solutions')
					$criteria = ' and tag.TagValueID like ?';				
				if($filter2=='vendor')
					$criteria = ' and vendor.CompanyName like ?';				
				if($filter2=='product')
					$criteria = ' and product.Name like ? ';																
				if($filter2=='federal')
					$criteria = ' or tagvalues.Type = 0 and tag.TagValueID like ? ';					
				$q2 = '%'.$paging->sSearch2.'%';							
				array_unshift($prm,$q2);
				array_unshift($prm2,$q2);
			}
			$sql = sprintf($this->search_product_by_federal_sql,$criteria,$paging->sortby,$paging->order);			
			$sql2 = sprintf($this->search_product_by_federal_count_sql,$criteria);
			$rows = $this->get_rows($sql,$prm);	
			$res->totalrecords = intval($this->get_scalar($sql2,$prm2)); 
		}
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new SearchProductResult();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}
	public function get_search_product_filter($keywords,$filter){
		$ret = new SearchProductFilter();
		$ret->Solutions = array();
		$ret->Federals = array();
		$ret->Vendors = array();
		$ret->Products = array();
		$q = '%'.$keywords.'%';
		$rows = NULL;
		if($filter=='all'){
			$rows = $this->get_rows($this->search_product_filter_sql,array($q,$q,$q));			
		}else if($filter=='vendor'){
			$rows = $this->get_rows($this->search_product_by_vendor_filter_sql,array($q));					
		}else if($filter=='solutions'){
			$rows = $this->get_rows($this->search_product_by_solution_filter_sql,array($q));			
		}else if($filter=='product'){
			$rows = $this->get_rows($this->search_product_by_product_filter_sql,array($q));				
		}else if($filter=='federal'){
			$rows = $this->get_rows($this->search_product_by_federal_filter_sql,array($q));			
			//var_dump($rows);exit(00);
		}
		
		if($rows!=NULL){
			$vendoralias = array();
			$productids = array();
			foreach($rows as $row){
				//solutions
				if($row->Type==$this->SOLUTION_TAG_TYPE){
					if(!empty($row->TagValueID)&&!in_array($row->TagValueID,$ret->Solutions)){
						array_push($ret->Solutions,$row->TagValueID);
					}
				}
				//federal
				if($row->Type==$this->FEDERAL_TAG_TYPE){
					if(!empty($row->TagValueID)&&!in_array($row->TagValueID,$ret->Federals)){
						array_push($ret->Federals,$row->TagValueID);
					}
				}
				//vendor
				if(!in_array($row->Alias,$vendoralias)){
					$vendoralias[] = $row->Alias;
					$v = new KeyValue();
					$v->k = $row->Alias;
					$v->v = $row->CompanyName;
					array_push($ret->Vendors,$v);
				}
				//products
				if(!in_array($row->ProductID,$productids)){
					$productids[] = $row->ProductID;
					$v = new KeyValue();
					$v->k = $row->ProductID;
					$v->v = $row->Name;
					array_push($ret->Products,$v);
				}
			}
		}
		return $ret;
	}
	public function get_all_vendor_name($paging){		
		$res = new PagingResult();				
		$sql = sprintf($this->get_all_vendor_name_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($paging->start,$paging->pagesize));			
		$res->totalrecords = intval($this->get_scalar($this->get_all_vendor_name_count_sql,NULL));				
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new KeyValue();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}
	public function get_all_solution_name($paging){		
		$res = new PagingResult();				
		$sql = sprintf($this->get_all_solution_name_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($paging->start,$paging->pagesize));			
		$res->totalrecords = intval($this->get_scalar($this->get_all_solution_name_count_sql,NULL));				
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new KeyValue();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}
	public function get_all_federal_name($paging){		
		$res = new PagingResult();				
		$sql = sprintf($this->get_all_federal_name_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($paging->start,$paging->pagesize));			
		$res->totalrecords = intval($this->get_scalar($this->get_all_federal_name_count_sql,NULL));				
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new KeyValue();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}
	public function get_all_product_name($paging){		
		$res = new PagingResult();				
		$sql = sprintf($this->get_all_product_name_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($paging->start,$paging->pagesize));			
		$res->totalrecords = intval($this->get_scalar($this->get_all_product_name_count_sql,NULL));				
		$res->data = array();
		if($rows!=NULL){
			$res->totaldisplayrecords = count($rows);
			foreach($rows as $r){
				$v = new KeyValue();
				$v->bind($r);
				array_push($res->data,$v);
			}	
			$res->calculate($paging);
		}
		return $res;	
	}	
}
/*
class SearchProductBase extends BaseMapper{
	private $paging;
	
	abstract function get_custom_criteria_sql();	
	
	public function __construct($gt){
		parent::__construct($gt);	
	}
	public function set_filter($paging){
		$this->paging = $paging;
	}
	
	protected function get_active_product_criteria(){
		return ' and product.Active=1 and vendor.Active=1 ';
	}
	protected function get_column_sql(){
		return 'select distinct(product.ProductID), product.Name, 
		vendor.CompanyName, product.ProductInfo, vendor.Alias, 
		vendor.MediaID,
		(select FileName from productimages 
		where productimages.ProductID=product.ProductID limit 1)
		 as ProductImage ';		 		
	}
	protected function get_from_table_sql(){
		return ' from product 
		inner join vendor on product.VendorID = vendor.VendorID				
		left join tag on product.ProductID = tag.ProductID
		left join tagvalues on tag.TagValueID = tagvalues.TagValueID ';
	}
	protected get_paging_sql(){
		return ' order by %s %s limit ?,? ';
	}
	public function get_search_result(){
		$sql = $this->get_column_sql() . $this->get_from_table_sql() .' where '
			. $this->get_custom_criteria_sql().$this->get_paging_sql();
		
	}
}
class SearchProductByAllCategories extends SearchProductBase{
	public function __construct($gt){
		parent::__construct($gt);	
	}
	public function set_filter($arrFilter){
	}
	public function get_custom_criteria_sql(){
		$paging = $this->paging;
		$q = '%'.$paging->filter.'%';
		if(empty($paging->filter2)){
			$criteria = ' product.Name like ? 
			or tag.TagValueID like ? 
			or vendor.CompanyName like ? ';
			$prm = array($q,$q,$q,$paging->start,$paging->pagesize);
			$prm2 = array($q,$q,$q);	
		}else{				
			$prm = array($q,$q,$paging->start,$paging->pagesize);
			$prm2 = array($q,$q);
			if($filter2=='solutions')
				$criteria = ' tag.TagValueID like ? and tagvalues.Type = '.$this->SOLUTION_TAG_TYPE 
				. ' and (product.Name like ? or vendor.CompanyName like ?)'	;				
			if($filter2=='vendor')
				$criteria = ' vendor.CompanyName like ? '		
				.' and (tag.TagValueID like ? or product.Name like ?)';	
			if($filter2=='product')
				$criteria = ' product.Name like ? '		
				.' and (tag.TagValueID like ? or vendor.CompanyName like ?)';		
			if($filter2=='federal')
				$criteria = ' tagvalues.Type = '.$this->FEDERAL_TAG_TYPE.' and tag.TagValueID like ? '
				. ' and (product.Name like ? or vendor.CompanyName like ?)'	;				
			$q2 = '%'.$paging->sSearch2.'%';							
			array_unshift($prm,$q2);
			array_unshift($prm2,$q2);
		}
	}
}
*/
?>
