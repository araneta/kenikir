<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class ProductMapper extends BaseMapper{
	
	private $SOLUTION_TAG_TYPE =  0;
	private $FEDERAL_TAG_TYPE = 1;
	
	private $new_product;
	private $add_product_sql = 'insert into product(VendorID, Name, ProductInfo,
			 AdminNotes, CreatedDate, UpdatedDate, Active)
			values(?,?,?,?,?,?,?)
			';	
	private $add_product_images_sql = 'insert into productimages(ProductID, FileName
			)
			values(?,?)
			';		
	private $add_product_video_sql = 'insert into productvideos(ProductID,Title, VideoFileName)
			values(?,?,?)
			';				
	private $delete_product_videos_sql = 'delete from productvideos where ProductID = ?';
	
	private $add_product_doc_sql = 'insert into productdocs(ProductID,Title, FileName)
			values(?,?,?)
			';			
	private $delete_product_docs_sql = 'delete from productdocs where ProductID = ?';		
	
	private $add_tag_sql = 'insert into tag(TagValueID,ProductID) values(?,?)';	
	private $add_tag_value_sql = 'insert into tagvalues(TagValueID,Type) values(?,?)';
	
	private $get_solution_tags_sql = 'select TagValueID from tagvalues where Type = %d and (TagValueID like ? or ? is NULL)';		
	private $get_federal_tags_sql = 'select TagValueID from tagvalues where Type = %d and TagValueID like ?';		
	
	private $check_solution_tag_sql = 'select count(TagValueID) as c from tagvalues where Type = %d and TagValueID = ?';		
	private $check_federal_tag_sql = 'select count(TagValueID) as c from tagvalues where Type = %d and TagValueID = ?';		
	
	private $get_all_by_vendor_sql = 'select * from product where VendorID=? order by %s %s limit ?,?';
	private $get_all_by_vendor_count_sql = 'select count(*) as c from product where VendorID=? ';
	
	private $get_active_products_by_vendor_sql = 'select * from product where VendorID=? and Active=1 order by %s %s limit ?,?';
	private $get_active_products_by_vendor_count_sql = 'select count(*) as c from product where VendorID=? and Active=1 ';
	
	//private $get_product_by_vendor_sql = 'select * from product where VendorID = ? and ProductID = ?';
	private $get_tags_by_product_sql = 'select tagvalues.TagValueID, Type from tag 
			inner join tagvalues on tag.TagValueID = tagvalues.TagValueID
			where ProductID = ?';
	private $get_product_image_sql = 'select * from productimages where ProductID = ?';
	private $get_product_videos_sql = 'select * from productvideos where ProductID = ?';
	private $get_product_docs_sql = 'select * from productdocs where ProductID = ?';
	private $update_product_sql = 'update product set Name = ?, ProductInfo = ?,
			UpdatedDate = ?, Active = ?
			where VendorID = ? and ProductID = ?
			';	
	private $update_product_images_sql = 'update productimages set FileName = ?
			where ProductID = ?
			';		
	private $delete_tags_sql = 'delete from tag where ProductID = ?';
	private $get_product_by_id_sql = 'select product.*
			from product 			
			where ProductID = ?';
	private $get_product_images_by_vendor_sql = 'select FileName
			from product inner join productimages on product.ProductID = productimages.ProductID
			where VendorID=?';
	private $get_product_videos_by_vendor_sql = 'select VideoFileName
			from product 
			inner join productvideos on product.ProductID = productvideos.ProductID
			where VendorID=?';	
	private $get_product_docs_by_vendor_sql = 'select FileName
			from product 
			inner join productdocs on product.ProductID = productdocs.ProductID
			where VendorID=?';	
			
	private $delete_product_video_sql = 'delete from productvideos where ProductID=? and VideoFileName=?';
	private $delete_product_doc_sql = 'delete from productdocs where ProductID=? and FileName=?';
	private $delete_product_image_sql = 'update productimages set FileName=NULL where ProductID=? and FileName=?';
			
	public function __construct($gt){
		parent::__construct($gt);
		$this->get_solution_tags_sql = sprintf($this->get_solution_tags_sql,$this->SOLUTION_TAG_TYPE);
		$this->get_federal_tags_sql = sprintf($this->get_federal_tags_sql,$this->FEDERAL_TAG_TYPE);
		
		$this->check_solution_tag_sql = sprintf($this->check_solution_tag_sql,$this->SOLUTION_TAG_TYPE);
		$this->check_federal_tag_sql = sprintf($this->check_federal_tag_sql,$this->FEDERAL_TAG_TYPE);
	}
		
	public function add_product($p){
		$prm = array($p->VendorID, $p->Name, $p->ProductInfo,
			$p->AdminNotes, $p->CreatedDate, 
			$p->UpdatedDate, $p->Active
		);
		$this->trans_start();
		$ret = $this->execute($this->add_product_sql,$prm);
		if($ret==TRUE){
			$p->ProductID = $this->get_last_id();
			$this->new_product = $p;
			//add product image
			$prmimg =array($p->ProductID, $p->ProductImage);
			$ret &= $this->execute($this->add_product_images_sql,$prmimg);
			$ret &= $this->save_video_and_doc($p);
			
			//add tags
			$ret &= $this->save_tags($p);
		}
		$this->trans_complete();
		if ($this->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
		//return $ret;
	}
	public function get_solution_tags($term){
		$rows = $this->get_rows($this->get_solution_tags_sql,array('%'.$term.'%',$term));
		$ret = array();
		if($rows!=NULL){			
			foreach($rows as $tag){
				$ret[] = $tag->TagValueID;
			}			
		}
		return $ret;
	}
	public function get_federal_tags($term){
		$rows = $this->get_rows($this->get_federal_tags_sql,array('%'.$term.'%'));
		$ret = array();
		if($rows!=NULL){			
			foreach($rows as $tag){
				$ret[] = $tag->TagValueID;
			}			
		}
		return $ret;
	}
	public function get_products_by_vendor($paging,$vendorID){
		$res = new PagingResult();
			
		$sql = sprintf($this->get_all_by_vendor_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($vendorID,$paging->start,$paging->pagesize));
		
		$res->totalrecords = intval($this->get_scalar($this->get_all_by_vendor_count_sql,array($vendorID)));
		
		$res->totaldisplayrecords = count($rows);
		$res->data = array();
		if($rows!=NULL){
			foreach($rows as $r){
				$v = new Product();
				$v->bind($r);
				array_push($res->data,$v);
			}	
		}
		return $res;	
	}
	public function get_active_products_by_vendor($paging,$vendorID){
		$res = new PagingResult();
			
		$sql = sprintf($this->get_active_products_by_vendor_sql,$paging->sortby,$paging->order);
		$rows = $this->get_rows($sql,array($vendorID,$paging->start,$paging->pagesize));
		
		$res->totalrecords = intval($this->get_scalar($this->get_active_products_by_vendor_count_sql,array($vendorID)));
		
		$res->totaldisplayrecords = count($rows);
		$res->data = array();
		if($rows!=NULL){
			foreach($rows as $r){
				$v = new Product();
				$v->bind($r);
				array_push($res->data,$v);
			}	
		}
		return $res;	
	}
	public function get_product_and_media($productId){
		$p = $this->get_product_by_id($productId);
		if($p==NULL)
			return NULL;
		return $this->get_product_media($p);	
	}
	public function get_product_tags($p){
		$rows = $this->get_rows($this->get_tags_by_product_sql,array($p->ProductID));
		if($rows!=NULL){
			$solutionTags = array();
			$federalTags = array();
			foreach($rows as $r){
				if($r->Type==$this->SOLUTION_TAG_TYPE){
					array_push($solutionTags,$r->TagValueID);
				}else if($r->Type==$this->FEDERAL_TAG_TYPE){
					array_push($federalTags,$r->TagValueID);
				}
			}
			if(count($solutionTags)>0)
				$p->SolutionTags = implode(',',$solutionTags);
			if(count($federalTags )>0)
				$p->FederalTags = implode(',',$federalTags);	
		}
		return $p;
	}
	public function get_product_media($p){
		//get tags		
		$p = $this->get_product_tags($p);
		//get image
		$rows = $this->get_rows($this->get_product_image_sql,array($p->ProductID));
		if($rows!=NULL){
			$row = $rows[0];
			$p->ProductImage = $row->FileName;	
		}
		//get video
		$rows = $this->get_rows($this->get_product_videos_sql,array($p->ProductID));
		if($rows!=NULL){
			$p->VideoTitle = array();
			$p->VideoFileName = array();			
			foreach($rows as $r){
				array_push($p->VideoTitle,$r->Title);
				array_push($p->VideoFileName,$r->VideoFileName);
			}			
		}
		//get doc
		$rows = $this->get_rows($this->get_product_docs_sql,array($p->ProductID));
		if($rows!=NULL){
			$p->DocTitle = array();
			$p->DocFileName = array();			
			foreach($rows as $r){
				array_push($p->DocTitle,$r->Title);
				array_push($p->DocFileName,$r->FileName);
			}			
		}
		return $p;
	}
	private function save_video_and_doc($p){
		$ret = TRUE;
		//add product video
		$n = count($p->VideoFileName);
		for($i=0;$i<$n;$i++){
			$video = $p->VideoFileName[$i];
			$videotitle = $p->VideoTitle[$i];				
			if(!empty($video)&&!empty($videotitle)){
				$prmvid =array($p->ProductID, $videotitle,$video);
				$ret &= $this->execute($this->add_product_video_sql,$prmvid);
			}
		}
		//add product doc
		$n = count($p->DocFileName);
		for($i=0;$i<$n;$i++){
			$doc = $p->DocFileName[$i];
			$doctitle = $p->DocTitle[$i];
			if(!empty($doc)&&!empty($doctitle)){
				$prmdoc =array($p->ProductID, $doctitle,$doc);
				$ret &= $this->execute($this->add_product_doc_sql,$prmdoc);
			}
		}
		return $ret;
	}
	private function save_tags($p){
		$ret = TRUE;
		//add solution tag
		if(!empty($p->SolutionTags)){
			$tags = explode(',',$p->SolutionTags);
			if(count($tags)>0){
				foreach($tags as $t){							
					//check first
					//if new, should not saved
					if($this->get_scalar($this->check_solution_tag_sql,array($t))==0){					
						//$ret &= $this->execute($this->add_tag_value_sql,array($t,$this->SOLUTION_TAG_TYPE));						
						continue;
					}
					$ret &= $this->execute($this->add_tag_sql,array($t,$p->ProductID));
				}
			}
		}
		//add federal tag
		if(!empty($p->FederalTags)){
			$tags = explode(',',$p->FederalTags);
			if(count($tags)>0){
				foreach($tags as $t){		
					//check first
					//if new, should not be saved
					if($this->get_scalar($this->check_federal_tag_sql,array($t))==0){					
						//$ret &= $this->execute($this->add_tag_value_sql,array($t,$this->FEDERAL_TAG_TYPE));
						continue;
					}
					$ret &= $this->execute($this->add_tag_sql,array($t,$p->ProductID));
				}
			}
		}
		return $ret;
	}
	public function update_product($p){
		$prm = array($p->Name, $p->ProductInfo,
			$p->UpdatedDate, $p->Active,
			$p->VendorID,$p->ProductID
		);
		$this->trans_start();
		$ret = $this->execute($this->update_product_sql,$prm);
		if($ret==TRUE){			
			//update product image
			$prmimg =array($p->ProductImage, $p->ProductID,);
			$ret &= $this->execute($this->update_product_images_sql,$prmimg);
			//delete vid and doc			
			$ret &= $this->execute($this->delete_product_videos_sql,array($p->ProductID));
			$ret &= $this->execute($this->delete_product_docs_sql,array($p->ProductID));
			$ret &= $this->save_video_and_doc($p);
			//delete tags
			$ret &= $this->execute($this->delete_tags_sql,array($p->ProductID));
			$ret &= $this->save_tags($p);
		}
		$this->trans_complete();
		
		if ($this->trans_status() === FALSE)
		{
			return FALSE;
		}
		return TRUE;
	}
	public function get_product_by_id($productID){
		$rows = $this->get_rows($this->get_product_by_id_sql,array($productID));
		if($rows==NULL)
			return NULL;
			
		$p = new Product();
		$p->bind($rows[0]);
		return $p;
	}
	public function get_product_images_by_vendor($vendorID){
		$rows = $this->get_rows($this->get_product_images_by_vendor_sql,array($vendorID));
		if($rows==NULL)
			return NULL;
		$files = array();	
		foreach($rows as $row){
			$files[] = $row->FileName;
		}	
		return $files;
	}
	public function get_product_videos_by_vendor($vendorID){
		$rows = $this->get_rows($this->get_product_videos_by_vendor_sql,array($vendorID));
		if($rows==NULL)
			return NULL;
		$files = array();	
		foreach($rows as $row){
			$files[] = $row->VideoFileName;
		}	
		return $files;
	}
	public function get_product_docs_by_vendor($vendorID){
		$rows = $this->get_rows($this->get_product_docs_by_vendor_sql,array($vendorID));
		if($rows==NULL)
			return NULL;
		$files = array();	
		foreach($rows as $row){
			$files[] = $row->FileName;
		}	
		return $files;
	}
	public function delete_product_video($productID,$filename){
		return $this->execute($this->delete_product_video_sql,array($productID,$filename));
	}
	public function delete_product_doc($productID,$filename){
		return $this->execute($this->delete_product_doc_sql,array($productID,$filename));
	}
	public function delete_product_image($productID,$filename){
		return $this->execute($this->delete_product_image_sql,array($productID,$filename));
	}
}
?>
