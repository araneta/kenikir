<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once dirname(__FILE__).'/basemapper.php';
class MemberMapper extends BaseMapper{
	
	private $check_email_sql = 'select count(*) as c from member where Email = ? or Email2 = ?';
	private $check_email_member_sql = 'select count(*) as c from member where (Email = ? or Email2 = ?) and MemberID!=?';
	
	private $add_member_sql = 'insert into member(MemberID,AgencyID,Password,
		Question1,Answer1,Question2,Answer2,FirstName,LastName,Phone,Email,
		ReceiveNewsletter,ImagePath,MemberSince,Active,Hash) values(?,?,?,?,?,?,?,?,
		?,?,?,?,?,?,?,?)';
		
	private $check_code_sql = 'select MemberID from member where Email = ? and Hash = ?';	
	private $activate_member_sql = 'update member set Active = 1, LastUpdated = ? where MemberID = ?';	
	private $load_member_sql = 'select * from member where MemberID=?';	
	private $set_member_id_sql = 'update member set MemberID=?,LastUpdated = ? where MemberID=?';
	private $set_password_sql = 'update member set Password=?, Question1=?, Answer1=?, Question2=?, Answer2=? where MemberID=?';
	private $set_info_sql = 'update member set FirstName=?, LastName=?, Phone=?, Phone2=?, Email=?, Email2=?, URL=?, URL2=?, Address=?
		, City=?, State=?, Zip=?, Address2=?, City2=?, State2=?, Zip2=?, SubscribeNewsletter=?
		where MemberID=?';
	//login
	private $check_login_sql = 'select MemberID,FirstName,LastName from member where MemberID = ? and Password = ? and Active = 1';	
	private $update_login_time_sql = 'update member set LastLogin = ?, LastUpdated = ? where MemberID = ?';	
	
	
	public function __construct($gt){
		parent::__construct($gt);
	}
	
	public function email_exists($email){
		if(intval($this->get_scalar($this->check_email_sql,array($email,$email)))>0)
			return TRUE;
		return FALSE;	
	}		
	public function add_member($m){
		$data = array($m->MemberID,$m->AgencyID,$m->Password,$m->Question1,$m->Answer1,
		$m->Question2,$m->Answer2,$m->FirstName,$m->LastName,$m->Phone,
		$m->Email,$m->ReceiveNewsletter,$m->ImagePath,$m->MemberSince,$m->Active,$m->Hash);
		return $this->execute($this->add_member_sql,$data);
	}
	public function check_code($email,$code){
		$rows = $this->get_rows($this->check_code_sql,array($email,$code));
		if($rows==NULL)
			return NULL;
		$v = new Member();
		$v->bind($rows[0]);
		return $v;	
	}
	public function activate($v){
		return $this->execute($this->activate_member_sql,array($v->LastUpdated,$v->MemberID));
	}
	public function load_member($memberId){
		$rows = $this->get_rows($this->load_member_sql,array($memberId));
		if($rows==NULL)
			return NULL;
		$row = $rows[0];
		$member = new Member();
		$member->bind($row);
		return $member;	
	}
	public function set_memberid($oldid,$member){
		return $this->execute($this->set_member_id_sql,array($member->MemberID,$member->LastUpdated,$oldid));
		
	}
	public function set_password($member){
		$data = array($member->Password,$member->Question1,$member->Answer1
			,$member->Question2,$member->Answer2,$member->MemberID);
		return $this->execute($this->set_password_sql,$data);
		
	}
	
	public function set_info($member){
		$data = array($member->FirstName
			,$member->LastName
			,$member->Phone
			,$member->Phone2
			,$member->Email
			,$member->Email2
			,$member->URL
			,$member->URL2
			,$member->Address
			,$member->City
			,$member->State
			,$member->Zip
			,$member->Address2
			,$member->City2
			,$member->State2
			,$member->Zip2
			,$member->SubscribeNewsletter
			,$member->MemberID);
		return $this->execute($this->set_info_sql,$data);
		
	}
	public function email_used_by_others($email,$memberID){
		if(intval($this->get_scalar($this->check_email_member_sql,array($email,$email,$memberID)))>0)
			return TRUE;
		return FALSE;	
	}
	public function check_login($memberid,$password){
		$rows = $this->get_rows($this->check_login_sql,array($memberid,$password));
		if($rows==NULL)
			return FALSE;
		$v = new Member();
		$v->bind($rows[0]);
		return $v;			
	}
	public function update_login_time($member){
		$data = array($member->LastLogin,$member->LastUpdated,$member->MemberID);
		return $this->execute($this->update_login_time_sql,$data);
	}
}
?>
