<?php
require_once(dirname(__FILE__).'/baseentity.php');
class Member extends BaseEntity{
	public $MemberID, $AgencyID, $Password, $Question1, $Answer1, 
	$Question2, $Answer2, $FirstName, $LastName, $Phone, $Phone2, $Email,
	$Email2, $ReceiveNewsletter, $URL, $URL2, $Address2, $City2, $State2,
	$Zip2, $PublicInfo, $PrivateNotes, $ImagePath, $MemberSince, $LastUpdated, 
	$LastLogin,  $AdminNotes,  $Active
	;
	public function validate(){
		$k = array('MemberID','AgencyID','Password',
		'Question1','Answer1','Question2','Answer2',
		'FirstName','LastName', 'Phone', 'Email',
		'ReceiveNewsletter','ImagePath', 'MemberSince',
		'Active'
		);
		$this->required($k);
		
		return !$this->has_error();
	}
}
?>
