<?php
require_once(dirname(__FILE__).'/baseentity.php');
class TagValues extends BaseEntity{
	public $TagValueID, $Type,$PublicInfo, $AdminNotes, $CreatedDate
	;
	public function validate(){
		$k = array('TagValueID','Type','CreatedDate');
		$this->required($k);
		
		return !$this->has_error();
	}
}
?>
