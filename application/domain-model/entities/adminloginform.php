<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class AdminLoginForm extends BaseEntity{
	public $username;
	public $password;	
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->username)){
			$this->add_error('Username is empty');
		}
		if(empty($this->password)){
			$this->add_error('Password is empty');
		}
		return !$this->has_error();
	}
	
}
?>
