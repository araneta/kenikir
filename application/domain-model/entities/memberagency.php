<?php
require_once(dirname(__FILE__).'/baseentity.php');
class MemberAgency extends BaseEntity{
	public $AgencyID, $AgencyType, $AgencyName, $SubAgency, $PublicInfo,
	 $PrivateNotes, $ImagePath, $MemberSince, $UpdatedDate, $UpdatedBy, 
	 $Active, $AdminNotes
	;
	public function validate(){
		$this->required(array('AgencyID','AgencyType','AgencyName','MemberSince','Active'));
		
		return !$this->has_error();
	}
}
?>
