<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class Paging extends BaseEntity{
	public $filter;
	public $filter2;
	public $pagesize;
	public $page;
	public $sortby;
	public $order;	
	public $start;
	public $end;
	//datatables
	public $sSearch;
	public $sSearch2;
	public $iDisplayLength=0;
	public $iDisplayStart=0;
	public $sSortDir_0 = 'asc';
	public $iSortCol_0=0;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->sSearch)){
			//$this->add_error('sSearch empty');	
		}
		if(empty($this->iDisplayLength)){
			$this->add_error('iDisplayLength empty');	
		}
		//if($this->iDisplayStart==NULL){
			//$this->add_error('iDisplayStart empty ' . $this->iDisplayStart);	
		//}
		if(empty($this->sSortDir_0)){
			$this->add_error('sSortDir_0 empty');	
		}
		if($this->iSortCol_0==NULL){
			$this->add_error('iSortCol_0 empty' . $this->iSortCol_0);	
		}
		$this->filter = $this->sSearch;
		$this->filter2 = $this->sSearch2;
		$this->pagesize = intval($this->iDisplayLength);
		$this->page = intval($this->iDisplayStart)/$this->pagesize + 1;		
		$this->start = intval($this->iDisplayStart);
		$this->end = $this->start + $this->pagesize;
		//sort colid
		$this->sortby = $this->iSortCol_0;
		//order by
		$this->order = $this->sSortDir_0;
		return !$this->has_error();
	}
}
class PagingResult extends BaseEntity{
	public $totaldisplayrecords;
	public $totalrecords;
	public $data;
	public $start;
	public $end;
	public $page;
	public $totalpages;
	public function calculate($paging){
		$this->start = $paging->start;
		$this->end = $this->start+$this->totaldisplayrecords;
		$this->page = $paging->page;
		$this->totalpages = ceil($this->totalrecords/$paging->pagesize);
	}
}
?>
