<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class WishListEntryForm extends BaseEntity{
	public $Name;
	public $Password;
	public $CustomerNotes;
	public $MemberID;
	public $WishlistID;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('Name','Password','MemberID'));
		return !$this->has_error();
	}	
}
?>
