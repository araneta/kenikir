<?php
require_once(dirname(__FILE__).'/baseentity.php');
class Vendor extends BaseEntity{
	public $VendorID, $Password, $Question1, $Answer1, $Question2, 
	$Answer2, $CompanyName, $FirstName, $LastName, $Phone, $Phone2, 
	$Email, $Email2, $URL, $URL2, $Address, $City, $State, $Zip, 
	$Address2, $City2, $State2, $Zip2, $PublicInfo, $PrivateNotes, 
	$ImagePath, $CreatedDate, $UpdatedDate, $LogInDate, $Active, 
	$AdminNotes, $Hash, $SubscribeNewsletter, $NewEmail, $HashNewEmail,
	$Alias,$VideoTitle, $VideoFileName,$DocTitle,$DocFileName, $MediaID,
	$PublicStatus;
	public function validate(){
		if(empty($this->VendorID)){
			$this->add_error('VendorID is empty');
		}
		//encrypted
		if(empty($this->Password)){
			$this->add_error('Password is empty');
		}	
		if(empty($this->Question1)){
			$this->add_error('Question is empty');
		}
		if(empty($this->Answer1)){
			$this->add_error('Answer1 is empty');
		}
		if(empty($this->Question2)){
			$this->add_error('Question2 is empty');
		}
		if(empty($this->Answer2)){
			$this->add_error('Answer2 is empty');
		}
		if(empty($this->CompanyName)){
			$this->add_error('CompanyName is empty');
		}
		if(empty($this->FirstName)){
			$this->add_error('FirstName is empty');
		}
		if(empty($this->LastName)){
			$this->add_error('Last Name is empty');
		}
		if(empty($this->Phone)){
			$this->add_error('Phone is empty');
		}
		if(empty($this->Email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->Email) && !filter_var($this->Email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		if (!empty($this->Email2) && !filter_var($this->Email2, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address 2');
		}
		if (!empty($this->Email) && !empty($this->Email2) && ($this->Email2==$this->Email)){
			$this->add_error('Invalid Email address 2');
		}
		if(empty($this->Address)){
			$this->add_error('Address is empty');
		}
		if(empty($this->City)){
			$this->add_error('City is empty');
		}
		if(empty($this->State)){
			$this->add_error('State is empty');
		}
		if(empty($this->Zip)){
			$this->add_error('Zip is empty');
		}
		if(empty($this->ImagePath)){
			$this->add_error('Image Path is empty');
		}
		if(empty($this->CreatedDate)){
			$this->add_error('Created Date is empty');
		}
		if(empty($this->Active)){
			$this->add_error('Active is empty');
		}
		if(empty($this->Alias)){
			$this->add_error('Alias is empty');
		}
		return !$this->has_error();
	}
}
?>
