<?php
require_once(dirname(__FILE__).'/baseentity.php');
class SearchProductResult extends BaseEntity{
	public $ProductID, $Name, $CompanyName, 
		$ProductInfo, $FileName, $Alias, $MediaID
	;	
}

class SearchProductFilter extends BaseEntity{
	public $Solutions, $Vendors, $Products, $Federals;
}
