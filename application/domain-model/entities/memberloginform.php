<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class MemberLoginForm extends BaseEntity{
	public $UserID;
	public $Password;	
	public $RememberMe;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('UserID','Password'));
		
		return !$this->has_error();
	}
	
}
?>
