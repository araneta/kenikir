<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class BaseEntity{
	protected $errors = array();
	public function __construct(){
			
	}
	public function bind($properties){
		foreach($properties as $key => $value){
			$this->{$key} = $value;
		}	
	}
	public function has_error(){
		if (count($this->errors)>0)
			return TRUE;
		return FALSE;
	}
	public function error_messages(){
		return implode('<br />',$this->errors);	
	}
	public function add_error($e){
		array_push($this->errors, $e);	
	}
	protected function required($props){
		foreach($props as $key){
			if(empty($this->{$key})){
				$str = preg_replace('/([a-z])([A-Z])/', '$1 $2', ucfirst($key));
				$this->add_error($str.' is empty');
			}
		}	
	}
}
?>
