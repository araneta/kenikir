<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class VendorRegistrationForm extends BaseEntity{
	public $email;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->email) && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}	
}
class VendorVerificationCodeForm extends BaseEntity{
	public $code;
	public $email;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->code)){
			$this->add_error('Code is empty');
		}
		if(empty($this->email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->email) && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}
			
}
?>
