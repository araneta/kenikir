<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/product.php');

class AddProductForm extends Product{
	private $invalidtags = array();
	private $validtags = array("html","body","b","i","ul","li","ol","p");
			
	public function __construct(){
		parent::__construct();
	}
	private function recursiveCheck($elements){
		if (!is_null($elements)) {
			foreach ($elements as $element) {
				if(!in_array($element->nodeName,$this->validtags))
				{
					if($element->nodeType==XML_ELEMENT_NODE)
						$this->invalidtags[] = $element->nodeName;
				}			
				$nodes = $element->childNodes;
				$this->recursiveCheck($nodes);
			}
		}
	}
	public function validate(){
		//check public info html tag
		if(!empty($this->ProductInfo)){
						
			$doc = new DOMDocument();
			$doc->loadHTML($this->ProductInfo);
			$elements = $doc->getElementsByTagName('*');
			$this->recursiveCheck($elements);					
			if(count($this->invalidtags)>0){				
				$this->add_error('HTML code '.implode(',',array_unique($this->invalidtags)).' is not allowed');				
			}			
		}
		
		return (!$this->has_error() && parent::validate());	
	}
}
