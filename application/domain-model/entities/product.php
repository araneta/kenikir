<?php
require_once(dirname(__FILE__).'/baseentity.php');
class Product extends BaseEntity{
	public $ProductID, $VendorID, $Name, $ProductInfo, 
	$AdminNotes, $CreatedDate, $UpdatedDate, $Active, $ProductImage,
	$VideoTitle, $VideoFileName,$DocTitle,$DocFileName,	
	$SolutionTags, $FederalTags
	;
	public function validate(){
		
		if(empty($this->VendorID)){
			$this->add_error('VendorID is empty');
		}
		if(empty($this->Name)){
			$this->add_error('Name is empty');
		}
		if(empty($this->ProductImage)){
			$this->add_error('ProductImage is empty');
		}
			
		if($this->Active==''){
			$this->add_error('Active is empty');
		}
		return !$this->has_error();
	}
}
