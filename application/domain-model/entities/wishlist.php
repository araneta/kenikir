<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class WishList extends BaseEntity{
	public $WishlistID;
	public $Name;
	public $Password;
	public $CustomerNotes;
	public $MemberID;
	public $Access;
	public $CreatedDate;
	public $LastItem;
	public $LastUpdated;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('Name','Password','Access','MemberID','CreatedDate'));
		return !$this->has_error();
	}	
}
class WishListItem extends BaseEntity{
	public $ItemID;
	public $WishlistID;
	public $ProductID;
	public $MemberNotes;
	public $AdminNotes;
	public $DateAdded;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('WishlistID','ProductID','DateAdded'));
		return !$this->has_error();
	}	
}
class WishListProduct extends BaseEntity{
	public $ItemID;	
	public $ProductID;
	public $ProductName;
	public $DateAdded;
	public $VendorID;
	public $VendorName;
	
	public function __construct(){
		parent::__construct();
	}	
}
class WishListCount extends BaseEntity{
	public $WishlistID;
	public $Name;
	public $WishlistItemCount;
	
	public function __construct(){
		parent::__construct();
	}	
}
?>

