<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class VendorLoginForm extends BaseEntity{
	public $username;
	public $password;	
	public $rememberme;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->username)){
			$this->add_error('Username is empty');
		}
		if(empty($this->password)){
			$this->add_error('Password is empty');
		}
		return !$this->has_error();
	}
	
}
class VendorForgotForm1 extends BaseEntity{
	public $PrimaryEmail;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('PrimaryEmail'));
		if (!empty($this->PrimaryEmail) && !filter_var($this->PrimaryEmail, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}
}
class VendorForgotForm2 extends BaseEntity{
	public $PrimaryEmail;
	public $Answer1;
	public $Answer2;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('PrimaryEmail','Answer1','Answer2'));
		if (!empty($this->PrimaryEmail) && !filter_var($this->PrimaryEmail, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}
}
?>
