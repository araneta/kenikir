<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/baseentity.php');
class MemberRegistrationForm extends BaseEntity{
	public $email;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->email) && !filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}	
}
class MemberAgencyRegistrationForm extends BaseEntity{
	public $Email;
	public $AgencyType;
	public $AgencyName;
	public $SubAgency;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('Email','AgencyType','AgencyName','SubAgency'));
		
		if (!empty($this->Email) && !filter_var($this->Email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}	
}
class MemberVerificationCodeForm extends BaseEntity{
	public $Code;
	public $Email;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->Code)){
			$this->add_error('Code is empty');
		}
		if(empty($this->Email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->Email) && !filter_var($this->Email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		return !$this->has_error();
	}			
}
class MemberSetMemberIDForm extends BaseEntity{
	public $MemberID;
	public $OldMemberID;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->MemberID)){
			$this->add_error('UserID is empty');
		}else{
			//alphanum
			if(!ctype_alnum($this->MemberID)){
				$this->add_error('UserID musl alphanumeric');
			}
		}
		
		if(empty($this->OldMemberID)){
			$this->add_error('OldUserID is empty');
		}else{
			//alphanum
			if(!ctype_alnum($this->OldMemberID)){
				$this->add_error('OldMemberID musl alphanumeric');
			}
		}
		
		return !$this->has_error();
	}			
}
class MemberSetPasswordForm extends BaseEntity{
	public $Password;
	public $Password_1;
	public $Password_2;
	public $Question1;
	public $Question2;
	public $Answer1;
	public $Answer2;
	public $MemberID;
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('MemberID'));
		if(empty($this->Password_1) || empty($this->Password_2)){
			$this->add_error('Password is empty');	
		}
		if(!empty($this->Password_1) && !empty($this->Password_2) && ($this->Password_1!=$this->Password_2)){
			$this->add_error('Password is not match');	
		}else
		{
			$this->Password = $this->Password_1;			
		}
		if(empty($this->Question1)) $this->add_error('Security Question 1 is empty');
		if(empty($this->Question2)) $this->add_error('Security Question 2 is empty');
		if(empty($this->Answer1)) $this->add_error('Answer 1 is empty');
		if(empty($this->Answer2)) $this->add_error('Answer 2 is empty');
		
		return !$this->has_error();
	}	
}
class MemberSetInfoForm extends BaseEntity{
	public $FirstName;
	public $LastName;
	public $Phone;
	public $Phone2;
	public $Email;
	public $Email2;
	public $URL;
	public $URL2;
	public $Address;
	public $City;
	public $State;
	public $Zip;
	public $Address2;
	public $City2;
	public $State2;
	public $Zip2;
	public $SubscribeNewsletter;
	public $MemberID;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){		
		$this->required(array('FirstName','LastName','Phone','Phone2','Email','Email2','URL','URL2','Address','City','State','Zip','Address2','City2','State2','Zip2'));
		
		return !$this->has_error();
	}	
}
?>
