<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once(dirname(__FILE__).'/vendor.php');
//this form is displayed after user/vendor enters correct activation code
class AddIdVendorForm extends BaseEntity{
	public $VendorID;
	public $OldVendorID;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		$this->required(array('VendorID','OldVendorID'));
		if(!empty($this->VendorID)&&
			(!ctype_alnum($this->VendorID) || (strlen($this->VendorID)<7))
		)
		{
			$this->add_error('Invalid User ID');
		}
		return !$this->has_error();
	}
}
class AddVendorInfoForm extends Vendor{	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->CompanyName)){
			$this->add_error('Company Name is empty');
		}
		if(empty($this->PublicInfo)){
			$this->add_error('Public Info is empty');
		}
		if(empty($this->FirstName)){
			$this->add_error('First Name is empty');
		}
		if(empty($this->LastName)){
			$this->add_error('Last Name is empty');
		}
		if(empty($this->Phone)){
			$this->add_error('Phone is empty');
		}
		if(empty($this->Email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->Email) && !filter_var($this->Email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		if (!empty($this->Email2) && !filter_var($this->Email2, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address 2');
		}
		if (!empty($this->Email) && !empty($this->Email2) && ($this->Email2==$this->Email)){
			$this->add_error('Invalid Email address 2');
		}
		if(empty($this->Address)){
			$this->add_error('Address is empty');
		}
		if(empty($this->City)){
			$this->add_error('City is empty');
		}
		if(empty($this->State)){
			$this->add_error('State is empty');
		}
		if(empty($this->Zip)){
			$this->add_error('Zip is empty');
		}
		if(empty($this->ImagePath)){
			$this->add_error('Image Path is empty');
		}
		
		return !$this->has_error();
	}
}
class EditVendorInfoForm extends Vendor{	
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		if(empty($this->FirstName)){
			$this->add_error('FirstName is empty');
		}
		if(empty($this->LastName)){
			$this->add_error('Last Name is empty');
		}
		if(empty($this->Phone)){
			$this->add_error('Phone is empty');
		}
		if(empty($this->Email)){
			$this->add_error('Email is empty');
		}
		if (!empty($this->Email) && !filter_var($this->Email, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address');
		}
		if (!empty($this->Email2) && !filter_var($this->Email2, FILTER_VALIDATE_EMAIL)) {
			$this->add_error('Invalid Email address 2');
		}
		if (!empty($this->Email) && !empty($this->Email2) && ($this->Email2==$this->Email)){
			$this->add_error('Invalid Email address 2');
		}
		if(empty($this->Address)){
			$this->add_error('Address is empty');
		}
		if(empty($this->City)){
			$this->add_error('City is empty');
		}
		if(empty($this->State)){
			$this->add_error('State is empty');
		}
		if(empty($this->Zip)){
			$this->add_error('Zip is empty');
		}
				
		return !$this->has_error();
	}
}
class EditVendorSecurityInfoForm extends Vendor{
	public $Password_1;
	public $Password_2;
	
	public function __construct(){
		parent::__construct();
	}
	public function validate(){
		
		if(!empty($this->Password_1) && !empty($this->Password_2) && ($this->Password_1!=$this->Password_2)){
			$this->add_error('Password is not match');	
		}else
		{
			$this->Password = $this->Password_1;			
		}
		
		return !parent::has_error() && !$this->has_error();
	}
}
