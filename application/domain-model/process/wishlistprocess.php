<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('WishList'));
load_mappers(array('WishListMapper'));
load_helpers(array('TimeHelper'));

class WishListProcess extends BaseProcess {
	
	function __construct($gt){
		parent::__construct($gt);
	}
	private function encrypt_password($p){
		return md5($p);
	}
	public function add_wishlist($f){
		$mapper = new WishListMapper($this->gt);		
		$current = TimeHelper::get_time_in_utc();
		$f->CreatedDate = $current;		
		$f->Access = 0;//default
		$f->Password = $this->encrypt_password($f->Password);
		return $mapper->add_wishlist($f);
	}
	public function update_wishlist($f){
		$mapper = new WishListMapper($this->gt);		
		$current = TimeHelper::get_time_in_utc();
		$f->LastUpdated = $current;				
		$f->Password = $this->encrypt_password($f->Password);
		return $mapper->update_wishlist($f);
	}
	public function get_wishlist_by_member($paging,$memberid){
		$mapper = new WishListMapper($this->gt);
		$pagingresult = $mapper->get_wishlist_by_member($paging,$memberid);
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$wish){
				$wish->CreatedDate = TimeHelper::convert_to_string_1($wish->CreatedDate);
			}
		}
		return $pagingresult;	
	}
	public function get_default_wishlist($memberID){
		//default get the first wishlist
		$mapper = new  WishListMapper($this->gt);
		return $mapper->get_default_wishlist($memberID);
	}
	public function add_product_to_wishlist($product,$wishlist){
		$current = TimeHelper::get_time_in_utc();			
		$item = new WishListItem();
		$item->WishlistID = $wishlist->WishlistID;
		$item->ProductID = $product->ProductID;
		$item->DateAdded = $current;
		$mapper = new  WishListMapper($this->gt);
		return $mapper->add_product_to_wishlist($item);
	}
	public function get_wishlist($wishlistID,$memberID){
		$mapper = new  WishListMapper($this->gt);
		return $mapper->get_wishlist($wishlistID,$memberID);
	}
	public function get_wishlist_products($paging,$wishlistID,$memberID){
		$mapper = new  WishListMapper($this->gt);
		$pagingresult = $mapper->get_wishlist_products($paging,$wishlistID,$memberID);
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$p){
				$p->DateAdded = TimeHelper::convert_to_string_1($p->DateAdded);
			}
		}
		return $pagingresult;
	}
	public function get_wishlist_by_item($itemID,$memberID){
		$mapper = new  WishListMapper($this->gt);
		return $mapper->get_wishlist_by_item($itemID,$memberID);
		
	}
	public function delete_item($itemID){
		$mapper = new  WishListMapper($this->gt);				
		if(!$mapper->delete_item($itemID)){
			$this->add_error('Error deleting item:'.$itemID);
			return FALSE;
		}
		return TRUE;
	}
	public function get_wishlist_count_by_member($memberID){
		$mapper = new  WishListMapper($this->gt);				
		return $mapper->get_wishlist_count_by_member($memberID);
	}
}
?>
