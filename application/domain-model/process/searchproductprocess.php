<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('SearchProductResult','KeyValue','Paging'));
load_mappers(array('SearchProductMapper'));
load_helpers(array('TextHelper'));
class SearchProductProcess extends BaseProcess {
	private $max_sidebar_item = 13;
	private $search_product_mapper = NULL;
	
	function __construct($gt){
		parent::__construct($gt);
	}
	private function get_search_product_mapper(){
		if($this->search_product_mapper==NULL){
			$this->search_product_mapper = new SearchProductMapper($this->gt);
		}
		return $this->search_product_mapper;
	}
	public function search_product($paging,$filter){
		$mapper = $this->get_search_product_mapper();
		$pagingresult = $mapper->search_product_with_filter($paging,$filter,NULL);		
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$product){
				$product->ProductInfo = TextHelper::Cut($product->ProductInfo,450);
			}
		}
		return $pagingresult;
	}
	public function search_product_with_filter($paging,$filter,$filter2){
		$mapper = $this->get_search_product_mapper();		
		$pagingresult = $mapper->search_product_with_filter($paging,$filter,$filter2);		
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$product){
				$product->ProductInfo = TextHelper::Cut($product->ProductInfo,450);
			}
		}
		return $pagingresult;
	}
	public function get_all_vendor_name($paging){
		$mapper = $this->get_search_product_mapper();
		$vendors = $mapper->get_all_vendor_name($paging);
		return $vendors;
	}
	public function get_all_solution_name($paging){
		$mapper = $this->get_search_product_mapper();
		$solutions = $mapper->get_all_solution_name($paging);
		return $solutions;
	}
	public function get_all_federal_name($paging){
		$mapper = $this->get_search_product_mapper();
		$federal = $mapper->get_all_federal_name($paging);
		return $federal;
	}
	public function get_all_product_name($paging){
		$mapper = $this->get_search_product_mapper();
		$product = $mapper->get_all_product_name($paging);
		return $product;
	}
	//for homepage
	//TODO: cache it!!!
	public function get_sidebar_data(){
		$ret = array();
		/*list of vendor*/
		$vpaging = new Paging();
		$vpaging->iDisplayLength = $this->max_sidebar_item;				
		$vpaging->iDisplayStart=0;
		$vpaging->sSortDir_0 = 'asc';
		$vpaging->iSortCol_0 = 'CompanyName';
		if(!$vpaging->validate()){
			die('error validating vendor '.$vpaging->error_messages());
		}else{			
			$vendors = $this->get_all_vendor_name($vpaging);
			$ret['vendors'] = $vendors->data;
		}
		/*list of solution*/
		$spaging = new Paging();
		$spaging->iDisplayLength = $this->max_sidebar_item;				
		$spaging->iDisplayStart=0;
		$spaging->sSortDir_0 = 'asc';
		$spaging->iSortCol_0 = 'TagValueID';
		if(!$spaging->validate()){
			die('error validating solution '.$spaging->error_messages());
		}else{			
			$solutions = $this->get_all_solution_name($spaging);
			$ret['solutions'] = $solutions->data;
		}
		/*list of federal*/
		$fpaging = new Paging();
		$fpaging->iDisplayLength = $this->max_sidebar_item;				
		$fpaging->iDisplayStart=0;
		$fpaging->sSortDir_0 = 'asc';
		$fpaging->iSortCol_0 = 'TagValueID';
		if(!$fpaging->validate()){
			die('error validating federal '.$fpaging->error_messages());
		}else{			
			$federals = $this->get_all_federal_name($fpaging);
			$ret['federals'] = $federals->data;
		}
		/*list of product*/
		$ppaging = new Paging();
		$ppaging->iDisplayLength = $this->max_sidebar_item;				
		$ppaging->iDisplayStart=0;
		$ppaging->sSortDir_0 = 'asc';
		$ppaging->iSortCol_0 = 'Name';
		if(!$ppaging->validate()){
			die('error validating product '.$ppaging->error_messages());
		}else{			
			$products = $this->get_all_product_name($ppaging);
			$ret['products'] = $products->data;
		}
		return $ret;
	}
	//for search
	public function get_filter_data($keywords,$filter){
		$mapper = $this->get_search_product_mapper();
		return $mapper->get_search_product_filter($keywords,$filter);
	}
	
	
}
?>
