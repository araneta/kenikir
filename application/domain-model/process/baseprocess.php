<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('ENTITY_PATH',dirname(dirname(__FILE__)).'/entities/');
define('MAPPER_PATH',dirname(dirname(__FILE__)).'/mappers/');
define('HELPER_PATH',dirname(dirname(__FILE__)).'/helpers/');
define('PROCESS_PATH',dirname(dirname(__FILE__)).'/process/');
function load_entities($entities){
	foreach($entities as $e){
		require_once(ENTITY_PATH.strtolower($e).'.php');
	}	
}
function load_mappers($mappers){
	foreach($mappers as $m){
		require_once(MAPPER_PATH.strtolower($m).'.php');
	}	
}
function load_helpers($helpers){
	foreach($helpers as $h){
		require_once(HELPER_PATH.strtolower($h).'.php');
	}	
}
function load_process($process){
	foreach($process as $p){
		require_once(PROCESS_PATH.strtolower($p).'.php');
	}	
}
class BaseProcess{
	//table gateway
	protected $gt;
	function __construct($gt){
		$this->gt = $gt;
	}	
	protected $errors = array();
	public function has_error(){
		if (count($this->errors)>0)
			return TRUE;
		return FALSE;
	}
	public function error_messages(){
		return implode('<br />',$this->errors);	
	}
	public function add_error($e){
		array_push($this->errors, $e);	
	}
}
?>
