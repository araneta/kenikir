<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('Vendor'));
load_mappers(array('VendorMapper'));
load_helpers(array('TimeHelper','TextHelper'));

class VendorProcess extends BaseProcess {
	private $vendor;
	private $vendormapper;
	private $admin_process;
	
	function __construct($gt){
		parent::__construct($gt);
	}
	private function get_vendor_mapper(){
		if($this->vendormapper==NULL)		
			$this->vendormapper = new VendorMapper($this->gt);
		return $this->vendormapper;	
	}
	private function get_admin_process(){
		if($this->admin_process==NULL){
			load_process(array('AdminProcess'));
			$this->admin_process = new AdminProcess($this->gt);
		}
		return $this->admin_process;
	}
	function vendor(){
		return $this->vendor;
	}
	private function encrypt_password($p){
		return md5($p);
	}
	//used by admin 
	public function get_all($paging,$active){
		$mapper = $this->get_vendor_mapper();
		$vendors = $mapper->get_all($paging,$active);
		return $vendors;
	}
	//set public status,status active: 1, inactive: 0
	public function update_public_status($vendorID,$bStatus){
		$mapper = $this->get_vendor_mapper();
		$vendor = $this->load_vendor($vendorID);
		if($vendor==NULL){
			$this->add_error('Vendor not found');
			return FALSE;
		}
		//set public status
		$now = TimeHelper::get_time_in_utc();
		$vendor->PublicStatus = $bStatus===TRUE?1:0;
		$vendor->UpdatedDate = $now;
		$ret = $mapper->update_public_status($vendor);
		if($ret && $vendor->PublicStatus==1){
			//send notification
			$adminprocess = $this->get_admin_process();
			$adminemail = $adminprocess->get_admin_email();						
			$this->send_public_status_notification($vendor,$adminemail);
		}
		return $ret;
	}
	private function send_public_status_notification($vendor,$adminemail){		
		    $to      = $vendor->Email; // Send email to our user  
			$subject = 'GOVonomy:  Vendor Approved'; // Give the email a subject   
			$message = ' 
			 			
Your vendor account has been approved.  Your Vendor profile and products can now be published on GOVonomy.com
Vendor Profile: http://www.govonomy.com/v/'.TextHelper::sanitize($vendor->Alias).'/'.$vendor->CompanyName.'

			 
			'; // Our message above including the link  
								  
			$headers = 'From:noreply@govonomy.com' . "\r\n"; // Set from headers  
			if(!empty($adminemail))
				$headers .= 'Cc: '.$adminemail . "\r\n";
			//echo $message ;exit(0);	
			return mail($to, $subject, $message, $headers); // Send our email  
	}
	public function register($form){
		$mapper = $this->get_vendor_mapper();
		if($mapper->email_exists($form->email))
		{
			$this->add_error('Email already registered');
			return FALSE;
		}
		
		//create an empty vendor
		$vendor = $this->create_empty_vendor($form->email);
		
		if(!$mapper->add_vendor($vendor)){
			$this->add_error('Failed to add vendor');
			return FALSE;
		}
		
		//send verification code using email
		if(!$this->send_activation($form->email,$vendor->Hash)){
			$this->add_error('Failed to send email to this vendor');
			//return FALSE;
			return TRUE;
		}
		return TRUE;	
		
	}
	private function send_activation($email,$code){
		    $to      = $email; // Send email to our user  
			$subject = 'GOVonomy: Signup | Verification'; // Give the email a subject   
			$message = ' 
			 
To become a GOVonomy Vendor, please verify your email address by visiting the following address:
			
http://vendor.g.vega10.com/register/verify/verify?email='.$email.'&code='.$code.' 
			
(If you can’t click on the link above you can copy and paste it to your browser’s address bar)
 
Or by using the code below:
'.$code; // Our message above including the link  
								  
			$headers = 'From:noreply@govonomy.com' . "\r\n"; // Set from headers  
			return mail($to, $subject, $message, $headers); // Send our email  
		
	}
	private function create_empty_vendor($email){
		$hash = md5( rand(0,1000) ); //activation code
		$password = $this->encrypt_password(rand(1000,5000)); //random password
		$current = TimeHelper::get_time_in_utc();
		$v = new Vendor();
		$v->VendorID = 'v'.rand(0,9000);
		$v->Password = $password;
		$v->Question1 = 'Mom\'s Maiden Name?';
		$v->Answer1 = '';
		$v->Question2 = 'Father\'s Birthday?';
		$v->Answer2 = '';
		$v->CompanyName = '';
		$v->FirstName = '';
		$v->LastName = '';
		$v->Phone = '';
		$v->Email = $email;
		$v->Address = '';
		$v->City = '';
		$v->State = '';
		$v->Zip = '';
		$v->ImagePath = 'anonymouse.png';		
		$v->CreatedDate = $current;
		$v->Active = 0;
		$v->PublicStatus = 0;
		$v->Hash = $hash;
		$v->Alias = '';
		$v->MediaID = 'm'.rand(0,9000);
		return $v;
	}
	public function check_activation_code($f){
		$mapper = $this->get_vendor_mapper();
		$vendor = $mapper->check_code($f->email,$f->code);
		if($vendor==NULL){
			$this->add_error('Invalid code');
			return FALSE;
		}
		if($vendor->Active==1){
			$this->add_error('You have already created an account. Please login <a href="/login">here</a>.');
			return FALSE;
		}
		$this->vendor = $vendor;
		return TRUE;
	}
	/*
	public function activate($f){
		$mapper = $this->get_vendor_mapper();
		$vendor = $mapper->check_code($f->email,$f->code);
		if($vendor==NULL){
			$this->add_error('Invalid code');
			return FALSE;
		}
		if($vendor->Active==1){
			$this->add_error('Expired code. Please login');
			return FALSE;
		}
		$current = TimeHelper::get_time_in_utc();
		$vendor->UpdatedDate = $current;
		$vendor->Active = 1;	
		$ret = $mapper->activate($vendor);	
		if($ret==TRUE){
			$this->vendor = $vendor;
		}else{
			$this->add_error('Error activating code');
		}
		return $ret;
	}*/
	public function get_vendor_info($vendor_id){
		$mapper = $this->get_vendor_mapper();
		return $mapper->get_vendor_info($vendor_id);
	}
	public function get_vendor_info_and_media($vendor_id){
		$mapper = $this->get_vendor_mapper();
		return $mapper->get_vendor_info_and_media($vendor_id);
	}
	public function get_vendor_media($v){
		$mapper = $this->get_vendor_mapper();
		return $mapper->get_vendor_media($v);
	}
	public function get_vendor_info_by_alias($vendorAlias){
		$mapper = $this->get_vendor_mapper();
		return $mapper->get_vendor_info_by_alias($vendorAlias);
	}
	public function login($form){
		$mapper = $this->get_vendor_mapper();
		$vendor = $mapper->check_login($form->username,$this->encrypt_password($form->password));
		if($vendor!= NULL){
			//update login date and updated date
			$current = TimeHelper::get_time_in_utc();
			$vendor->LogInDate = $current;
			$vendor->UpdatedDate = $current;
			
			if(!$mapper->update_login_time($vendor)){
				echo 'failed update login date';
				exit(0);
			}
			$this->vendor = $vendor;
			return TRUE;
		}
		return FALSE;
	}
	public function validate_info($f,$VendorID){
		$mapper = $this->get_vendor_mapper();
		if($mapper->check_existing_company_name($f->CompanyName,$VendorID)){
			$this->add_error('This company name:'.$f->CompanyName.' is already used');
			return FALSE;
		}
		if($mapper->email_used_by_others($f->Email,$VendorID)){
			$this->add_error('This email:'.$f->Email.' is already used');
			return FALSE;
		}
		if(!empty($f->Email2)&&$mapper->email_used_by_others($f->Email2,$VendorID)){
			$this->add_error('This email:'.$f->Email2.' is already used');
			return FALSE;
		}
		if(!empty($f->NewEmail)&&$mapper->email_used_by_others($f->NewEmail,$VendorID)){
			$this->add_error('This email:'.$f->NewEmail.' is already used');
			return FALSE;
		}
		return TRUE;
	}
	//first registration
	public function add_info($f){
		$mapper = $this->get_vendor_mapper();		
		if(!$this->validate_info($f,$f->VendorID))
			return FALSE;
		//update updated date
		$current = TimeHelper::get_time_in_utc();
		$f->Active = 1;		
		$f->Alias = $this->get_alias($f->CompanyName);
		$f->UpdatedDate = $current;
		$ret = $mapper->add_info($f);
		//send email to notify admin
		if($ret){
			$adminprocess = $this->get_admin_process();
			$adminemail = $adminprocess->get_admin_email();			
			if($adminemail!=NULL){
				$this->send_new_vendor_notification($adminemail,$f);
			}
		}
		return $ret;
	}
	private function send_new_vendor_notification($adminemail, $info){
		    $to      = $adminemail; // Send email to our user  
			$subject = 'GOVonomy: New Vendor'; // Give the email a subject   
			$message = ' 
Company: '.$info->CompanyName.'
First Name:'.$info->FirstName.'		
Last Name:'.$info->LastName.'
Phone:'.$info->Phone.'
Email:'.$info->Email.'
Please login to activate vendor: http://admin.g.vega10.com/
';
			$headers = 'From:noreply@govonomy.com' . "\r\n"; // Set from headers  
			return mail($to, $subject, $message, $headers); // Send our email  
		
	}
	public function load_vendor($VendorID){
		$mapper = $this->get_vendor_mapper();
		return $mapper->load_vendor($VendorID);		
	}
	public function update_info($f){
		$mapper = $this->get_vendor_mapper();		
		if(!$this->validate_info($f,$f->VendorID)){
			return FALSE;
		}
		//load vendor to check the previous data
		$vendor = $this->load_vendor($f->VendorID);
		if($vendor==NULL){
			$this->add_error('Vendor not found');
			return FALSE;
		}
		//update updated date
		$current = TimeHelper::get_time_in_utc();
		if($vendor->CompanyName != $f->CompanyName)
			$f->Alias = $this->get_alias($f->CompanyName);
		else
			$f->Alias = $vendor->Alias;	
		$f->UpdatedDate = $current;		
		return $mapper->update_info($f);
	}
	public function update_security_info($f){
		$mapper = $this->get_vendor_mapper();
		if(!empty($f->Password)){
			$f->Password = $this->encrypt_password($f->Password);
		}
		$current = TimeHelper::get_time_in_utc();
		$f->UpdatedDate = $current;
		return $mapper->update_security_info($f);
	}
	public function send_new_email_activation($email,$code){
			
		    $to      = $email; // Send email to our user  
			$subject = 'GOVonomy: New Email Verification'; // Give the email a subject   
			$message = ' 
			 			
You have requested to change your primary email address. To proceed, click on the link below or copy and paste the address into your browser’s address bar:
			 
http://vendor.g.vega10.com/register/verify_new_email?email='.$email.'&code='.$code.' 
			 
			'; // Our message above including the link  
								  
			$headers = 'From:noreply@govonomy.com' . "\r\n"; // Set from headers  
			return mail($to, $subject, $message, $headers); // Send our email  
		
	}
	public function activate_new_email($f){
		$mapper = $this->get_vendor_mapper();
		$vendor = $mapper->check_code_email($f->email,$f->code);
		if($vendor==NULL){
			$this->add_error('Invalid code');
			return FALSE;
		}
		if($vendor->HashNewEmail==NULL || $vendor->NewEmail==NULL){
			$this->add_error('Expired code');
			return FALSE;
		}
		$current = TimeHelper::get_time_in_utc();
		$vendor->UpdatedDate = $current;		
		$vendor->Email = $vendor->NewEmail;
		$vendor->NewEmail = NULL;
		$vendor->HashNewEmail = NULL;
		
		$ret = $mapper->activate_new_email($vendor);	
		if($ret==TRUE){						
			$this->vendor = $vendor;
		}else{
			$this->add_error('Error activating code');
		}
		return $ret;
	}
	public function send_new_password_email($email,$code){
			
		    $to      = $email; // Send email to our user  
			$subject = 'GOVonomy: New Password'; // Give the email a subject   
			$message = ' 
			 			
Please use this temporary password to login: 

'.$code.'

You may reset your password after logging in.

Please login at: 

http://vendor.g.vega10.com
			 
			'; // Our message above including the link  
								  
			$headers = 'From:noreply@govonomy.com' . "\r\n"; // Set from headers  
			return mail($to, $subject, $message, $headers); // Send our email  
		
	}
	public function recover_password($f){
		$mapper = $this->get_vendor_mapper();
		$vendor =  $mapper->check_security_questions($f);
		if($vendor==NULL){
			$this->add_error('Invalid Answer');
			return FALSE;
		}		
		$current = TimeHelper::get_time_in_utc();
		//set new password
		//$pass = rand(1000,50000);
		$pass = md5( rand(0,1000) );
		$vendor->Password = $this->encrypt_password($pass); //random password
		$vendor->UpdatedDate = $current;
		$ret = $mapper->reset_password($vendor);
		if(!$ret){
			$this->add_error('Error resetting password');
			return FALSE;
		}
		$this->send_new_password_email($vendor->Email,$pass);
		//die('pass'.$pass);
		return TRUE;
	}
	public function get_vendor_by_email($email){
		$mapper = $this->get_vendor_mapper();
		return $mapper->get_vendor_by_email($email);
	}
	
	private function get_alias($name){
		//return htmlspecialchars(str_replace(' ','-',$name),ENT_QUOTES);		
		$alias = TextHelper::sanitize($name);
		$vendor = $this->get_vendor_info_by_alias($alias);
		if($vendor!=NULL)
			$alias .= rand(0,9000);
		return $alias;
	}
	public function set_vendor_id($f){
		//check existing id
		$mapper = $this->get_vendor_mapper();
		$v = $this->load_vendor($f->VendorID);
		if($v!=NULL){
			$this->add_error('Sorry, this User ID is already in use. Please try another one.');
			return FALSE;
		}
		return $mapper->set_vendor_id($f->OldVendorID,$f->VendorID);
	}
	private function delete_files_except($dir,$except){
		$files = glob($dir.'/*'); // get all file names
		foreach($files as $file){ // iterate files						
			if(is_file($file)&&!in_array(basename($file),$except))
			{					
				unlink($file); // delete file
			}
		}
	}
	public function delete_file($fname,$basepath){
		$fpath = $basepath.'/'.$fname;
		$ret = FALSE;
		if (file_exists($fpath)&&is_file($fpath)) {
			if(unlink($fpath)){
				$ret = TRUE;
			}else{
				$this->add_error('Failed deleting file:'.$fname);
			}
		}else{
			//$this->add_error('Could not find file:'.$fname);
			$ret = TRUE;
		}	
		return $ret;
		//delete from db
		
	}
	public function delete_vendor_video($VendorID,$videofilename){
		$mapper = $this->get_vendor_mapper();
		return $mapper->delete_vendor_video($VendorID,$videofilename);
	}
	public function delete_vendor_doc($VendorID,$filename){
		$mapper = $this->get_vendor_mapper();
		return $mapper->delete_vendor_doc($VendorID,$filename);
	}
	public function delete_vendor_image($VendorID,$filename){
		$mapper = $this->get_vendor_mapper();
		return $mapper->delete_vendor_image($VendorID,$filename);
	}
	
	public function clean_vendor_dirs($vendorId, $imgbasepath,$vidbasepath,$docbasepath){		
		$v = $this->get_vendor_info_and_media($vendorId);
		if($v==NULL){
			$this->add_error('Vendor not found');
			return FALSE;
		}
		//delete unused images
		$this->delete_files_except($imgbasepath,array($v->ImagePath));
		
		$videos = $v->VideoFileName;
		if(count($videos)>0){
			//delete unused videos
			$this->delete_files_except($vidbasepath,$videos);
		}
		$docs = $v->DocFileName;
		if(count($docs)>0){
			//delete unused docs
			$this->delete_files_except($docbasepath,$docs);
		}
	}
}
?>
