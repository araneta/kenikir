<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('Member','MemberAgency'));
load_mappers(array('MemberMapper','AgencyMapper'));
load_helpers(array('TimeHelper'));

class MemberProcess extends BaseProcess {
	private $member;
	private $agency_type = array('Federal','State','Local','Non-Profit');
	
	function __construct($gt){
		parent::__construct($gt);
	}
	function member(){
		return $this->member;
	}
	public function get_all_agency(){
		$mapper = new AgencyMapper($this->gt);
		return $mapper->get_all_by_alphabet();
	}
	public function check_email($form){
		$mapper = new MemberMapper($this->gt);
		if($mapper->email_exists($form->email))
		{
			$this->add_error('Email already registered');
			return FALSE;
		}
		return TRUE;
	}
	public function register($form){
		$mapper = new MemberMapper($this->gt);
		if($mapper->email_exists($form->Email))
		{
			$this->add_error('Email already registered');
			return FALSE;
		}
				
		//get agency id
		$agencymapper = new AgencyMapper($this->gt);
		$agency = $agencymapper->get_agency_by_name($form->AgencyName);
		if($agency==NULL){
			//create new agency			
			if(!in_array($form->AgencyType,$this->agency_type)){
				$this->add_error('Failed to find agency type:'.$form->AgencyType);
				return FALSE;
			}
			$agency = $this->create_agency($form->AgencyName,$form->SubAgency,$form->AgencyType);
			if(!$agencymapper->add_agency($agency)){
				$this->add_error('Failed to add agency');
				return FALSE;
			}
		}
		//create an empty member
		$member = $this->create_empty_member($form->Email,$agency->AgencyID);
		if(!$mapper->add_member($member)){
			$this->add_error('Failed to add member');
			return FALSE;
		}
		
		//send verification code using email
		if(!$this->send_activation($form->Email,$member->Hash)){
			$this->add_error('Failed to send email to this member');
			//return FALSE;
			return TRUE;
		}
		return TRUE;	
		
	}
	private function encrypt_password($p){
		return md5($p);
	}
	private function create_empty_member($email,$agencyid){
		
		$hash = md5( rand(0,1000) ); //activation code
		$password = $this->encrypt_password(rand(1000,5000)); //random password
		$current = TimeHelper::get_time_in_utc();
		$v = new Member();
		$v->MemberID = 'm'.rand(0,1000);
		$v->AgencyID = $agencyid;
		$v->Password = $password;
		$v->Question1 = 'Mom\'s Maiden Name?';
		$v->Answer1 = '';
		$v->Question2 = 'Father\'s Birthday?';
		$v->Answer2 = '';		
		$v->FirstName = '';
		$v->LastName = '';
		$v->Phone = '';
		$v->Email = $email;								
		$v->ReceiveNewsletter = 0;
		$v->ImagePath = '/images/anonymous.png';		
		$v->MemberSince = $current;
		$v->Active = 0;
		$v->Hash = $hash;
		return $v;
	}
	private function send_activation($email,$code){
		$to      = $email; // Send email to our user  
		$subject = 'Signup | Verification'; // Give the email a subject   
		$message = ' 
		 
		Thanks for signing up! 
		Your account has been created, you can login with the following credentials after you have activated your account by pressing the url below. 
		 
		------------------------ 
		Code: '.$code.' 
		------------------------ 
		 
		Please click this link to activate your account: 
		 
		http://vendor.g.vega10.com/register/verify/verify?email='.$email.'&code='.$code.' 
		 
		'; // Our message above including the link  
							  
		$headers = 'From:noreply@yourwebsite.com' . "\r\n"; // Set from headers  
		return mail($to, $subject, $message, $headers); // Send our email  
		
	}
	private function create_agency($agencyname,$subagency,$agencytype){
		$current = TimeHelper::get_time_in_utc();
		$agency = new MemberAgency();
		//$agency->AgencyID = auto;
		$agency->AgencyType = $agencytype;
		$agency->AgencyName = $agencyname;
		$agency->SubAgency = $subagency;		
		$agency->MemberSince = $current;				
		$agency->Active = 0;
		return $agency;
	}
	public function activate($f){
		$mapper = new MemberMapper($this->gt);
		$member = $mapper->check_code($f->Email,$f->Code);
		if($member==NULL){
			$this->add_error('Invalid code');
			return FALSE;
		}
		$current = TimeHelper::get_time_in_utc();
		$member->LastUpdated = $current;
		$member->Active = 1;	
		$ret = $mapper->activate($member);	
		if($ret==TRUE){
			$this->member = $member;
		}else
		{
			$this->add_error('error activating member');
			return FALSE;
		}
		return $ret;
	}
	public function set_memberid($f){
		$current = TimeHelper::get_time_in_utc();
		$mapper = new MemberMapper($this->gt);
		$member = $mapper->load_member($f->OldMemberID);
		if($member==NULL){
			$this->add_error('Member not found');
			return FALSE;
		}
		$member->MemberID = $f->MemberID;
		$member->LastUpdated = $current;
		$ret = $mapper->set_memberid($f->OldMemberID,$member);
		if(!$ret){
			$this->add_error('Error setting memberid');
			return FALSE;
		}
		$this->member = $member;
		return TRUE;
	}
	public function set_password($f){
		$current = TimeHelper::get_time_in_utc();
		$mapper = new MemberMapper($this->gt);
		$member = $mapper->load_member($f->MemberID);
		if($member==NULL){
			$this->add_error('Member not found');
			return FALSE;
		}
		$member->MemberID = $f->MemberID;
		$member->Password = $this->encrypt_password($f->Password);
		$member->Question1 = $f->Question1;
		$member->Question2 = $f->Question2;
		$member->Answer1 = $f->Answer1;
		$member->Answer2 = $f->Answer2;
		$member->LastUpdated = $current;
		$ret = $mapper->set_password($member);
		if(!$ret){
			$this->add_error('Error setting password');
			return FALSE;
		}
		$this->member = $member;
		return TRUE;
	}
	public function set_info($f){
		$current = TimeHelper::get_time_in_utc();
		$mapper = new MemberMapper($this->gt);
		$member = $mapper->load_member($f->MemberID);
		if($member==NULL){
			$this->add_error('Member not found');
			return FALSE;
		}
		if($mapper->email_used_by_others($f->Email,$f->MemberID)||$mapper->email_used_by_others($f->Email2,$f->MemberID)){
			$this->add_error('Email already used');
			return FALSE;
		}
		$member->MemberID = $f->MemberID;
		$member->FirstName = $f->FirstName;
		$member->LastName = $f->LastName;
		$member->Phone = $f->Phone;
		$member->Phone2 = $f->Phone2;
		$member->Email = $f->Email;
		$member->Email2 = $f->Email2;
		$member->URL = $f->URL;
		$member->URL2 = $f->URL2;
		$member->Address = $f->Address;
		$member->City = $f->City;
		$member->State = $f->State;
		$member->Zip = $f->Zip;
		$member->Address2 = $f->Address2;
		$member->City2 = $f->City2;
		$member->State2 = $f->State2;
		$member->Zip2 = $f->Zip2;
		$member->SubscribeNewsletter = $f->SubscribeNewsletter;
		$member->LastUpdated = $current;
		$ret = $mapper->set_info($member);
		if(!$ret){
			$this->add_error('Error setting member information');
			return FALSE;
		}
		$this->member = $member;
		return TRUE;
	}
	public function get_by_id($memberID){
		$mapper = new MemberMapper($this->gt);
		return $mapper->load_member($memberID);
	}
	public function login($form){
		$mapper = new MemberMapper($this->gt);
		$member = $mapper->check_login($form->UserID,$this->encrypt_password($form->Password));
		if($member!= NULL){
			//update login date and updated date
			$current = TimeHelper::get_time_in_utc();
			$member->LastLogin = $current;
			$member->LastUpdated = $current;
			
			if(!$mapper->update_login_time($member)){
				echo 'failed update login date';
				exit(0);
			}
			$this->member = $member;
			return TRUE;
		}
		return FALSE;
	}
}
?>
