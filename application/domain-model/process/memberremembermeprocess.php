<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/remembermeprocess.php';

load_mappers(array('RememberMeMapper'));
load_helpers(array('TimeHelper'));

class MemberRememberMeProcess extends RememberMeProcess{
	protected $tablename = 'member';
	protected $columnid = 'MemberID';
	function __construct($gt){		
		parent::__construct($gt);
		
	}
	
}
