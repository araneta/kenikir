<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('Product'));
load_helpers(array('TimeHelper'));

class ProductProcess extends BaseProcess {
	private $product_mapper = NULL;
	private $vendor_process = NULL;
	function __construct($gt){
		parent::__construct($gt);
	}
	
	private function get_product_mapper(){
		if($this->product_mapper==NULL){
			load_mappers(array('ProductMapper'));
			$this->product_mapper = new ProductMapper($this->gt);
		}
		return $this->product_mapper;	
	}
	private function get_vendor_process(){
		if($this->vendor_process==NULL){
			load_process(array('VendorProcess'));
			$this->vendor_process = new VendorProcess($this->gt);
		}
		return $this->vendor_process;	
	}
	public function add_product($f){
		$mapper = $this->get_product_mapper();
		//update updated date
		$current = TimeHelper::get_time_in_utc();
		$f->CreatedDate = $current;
		$f->UpdatedDate = $current;
		return $mapper->add_product($f);
	}
	public function get_solution_tags($term){
		$mapper = $this->get_product_mapper();
		return $mapper->get_solution_tags($term);
	}
	public function get_federal_tags($term){
		$mapper = $this->get_product_mapper();
		return $mapper->get_federal_tags($term);
	}
	//select products by vendor whether its active or inactive
	public function get_products_by_vendor($paging,$vendorId){
		$mapper = $this->get_product_mapper();
		$pagingresult = $mapper->get_products_by_vendor($paging,$vendorId);
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$product){
				$product->CreatedDate = TimeHelper::convert_to_string_1($product->CreatedDate);
			}
		}
		return $pagingresult;
	}
	//select active products by vendor
	public function get_active_products_by_vendor($paging,$vendorId){
		$mapper = $this->get_product_mapper();
		$pagingresult = $mapper->get_active_products_by_vendor($paging,$vendorId);
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$product){
				$product->CreatedDate = TimeHelper::convert_to_string_1($product->CreatedDate);
			}
		}
		return $pagingresult;
	}
	public function update_product($f){
		$mapper = $this->get_product_mapper();
		//update updated date
		$current = TimeHelper::get_time_in_utc();		
		$f->UpdatedDate = $current;
		return $mapper->update_product($f);
	}
	public function get_product_by_id($productID){
		$mapper = $this->get_product_mapper();
		return $mapper->get_product_by_id($productID);
	}
	private function delete_files_except($dir,$except){
		$files = glob($dir.'/*'); // get all file names
		foreach($files as $file){ // iterate files						
			if(is_file($file)&&!in_array(basename($file),$except))
			{					
				unlink($file); // delete file
			}
		}
	}
	public function clean_product_dirs($vendorId, $imgbasepath,$thumbbasepath,$vidbasepath,$docbasepath){
		$mapper = $this->get_product_mapper();		
		$images = $mapper->get_product_images_by_vendor($vendorId);				
		if($images!=NULL){						
			//delete unused images
			$this->delete_files_except($imgbasepath,$images);
			//delete unused  thumbnails
			$this->delete_files_except($thumbbasepath,$images);
		}
		$videos = $mapper->get_product_videos_by_vendor($vendorId);
		if($videos!=NULL){
			//delete unused videos
			$this->delete_files_except($vidbasepath,$videos);
		}
		$docs = $mapper->get_product_docs_by_vendor($vendorId);
		if($docs!=NULL){
			//delete unused docs
			$this->delete_files_except($docbasepath,$docs);
		}
	}
	public function get_product_and_media_by_id($productID){
		$mapper = $this->get_product_mapper();
		return $mapper->get_product_and_media($productID);
	}
	public function get_active_products_and_media_by_vendor($paging,$vendorId){
		$mapper = $this->get_product_mapper();
		$pagingresult = $mapper->get_active_products_by_vendor($paging,$vendorId);
		if($pagingresult->data!=NULL){
			foreach($pagingresult->data as &$product){
				$product = $mapper->get_product_media($product);
				$product->CreatedDate = TimeHelper::convert_to_string_1($product->CreatedDate);
			}
		}
		return $pagingresult;
	}
	public function get_product_tags($p){
		$mapper = $this->get_product_mapper();
		return $mapper->get_product_tags($p);
	}
	//http://stackoverflow.com/questions/6000684/php-store-last-viewed-items-in-session-array?rq=1
	function add_last_seen_product($productID){					
		if(session_id() == '') {
			session_start();
		}
		$cookiedata = array();
		if(isset($_SESSION['recentviews'])&&!empty($_SESSION['recentviews'])) {					
			$cookiedata = unserialize($_SESSION['recentviews']);	
			if(!is_array($cookiedata))
				$cookiedata = array();			
			array_push($cookiedata, $productID);  
			$cookiedata = array_unique($cookiedata);
			if(count($cookiedata) > 10){
				$cookiedata = array_slice($cookiedata,1);
			}								
		} else {			
			$cookiedata = array();
		}						
		$cookie = serialize($cookiedata);				
		$_SESSION['recentviews'] = $cookie;
	}
	function get_last_seen_products(){
		if(session_id() == '') {
			session_start();
		}
		if(isset($_SESSION['recentviews'])) {			
			$cookie = $_SESSION['recentviews'];			
			$products = unserialize($cookie);	
			if(is_array($products)){				
				$ret = array();
				//load vendor to get alias
				//TODO: if it slow create a new function for this
				$vendorprocess = $this->get_vendor_process();
				foreach($products as $p){
					$pm = $this->get_product_and_media_by_id($p);
					if($pm!=NULL){
						if($pm->Active==1){
							$vendor = $vendorprocess->load_vendor($pm->VendorID);					
							if($vendor!=NULL && $vendor->Active==1){
								$pm->VendorAlias = $vendor->Alias;
								$pm->MediaID = $vendor->MediaID;
								$ret[] = $pm;
							}
						}
					}		
				}
				return $ret;
			}
		}
		return NULL;	
	}
	public function delete_product_video($productID,$videofilename){
		$mapper = $this->get_product_mapper();
		return $mapper->delete_product_video($productID,$videofilename);
	}
	public function delete_product_doc($productID,$filename){
		$mapper = $this->get_product_mapper();
		return $mapper->delete_product_doc($productID,$filename);
	}
	public function delete_product_image($productID,$filename){
		$mapper = $this->get_product_mapper();
		return $mapper->delete_product_image($productID,$filename);
	}
	public function delete_file($fname,$basepath){
		$fpath = $basepath.'/'.$fname;
		$ret = FALSE;
		if (file_exists($fpath)&&is_file($fpath)) {
			if(unlink($fpath)){
				$ret = TRUE;
			}else{
				$this->add_error('Failed deleting file:'.$fname);
			}
		}else{
			//$this->add_error('Could not find file:'.$fname);
			$ret = TRUE;
		}	
		return $ret;
		//delete from db
		
	}
}
?>
