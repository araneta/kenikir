<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_mappers(array('RememberMeMapper'));
load_helpers(array('TimeHelper'));

class RememberMeProcess extends BaseProcess {
	protected $tablename;
	protected $columnid;
	function __construct($gt){
		parent::__construct($gt);
	}
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	public function remember_me_cookie_name(){
		return 'remember_me_token'.$this->tablename;
	}
	public function remember($id){
		$token = $this->generateRandomString();
		/*
		$cookie = array(
			'name'   => 'remember_me_token',
			'value'  => $this->generateRandomString(),
			'expire' => '1209600',  // Two weeks
			//'domain' => '.your_domain.com',
			'domain' => 'localhost',
			'path'   => '/'
		);
		*/ 
		//set_cookie($cookie);		
		$mapper = new RememberMeMapper($this->gt);
		$mapper->set_table($this->tablename,$this->columnid);
		if($mapper->save_token($id,$token));{
			setcookie("remember_me_token".$this->tablename, $token, time()+1209600);
			return TRUE;
		}
		return FALSE;	
	}
	//return true if remembered
	public function check_remember_me(){		
		$name = $this->remember_me_cookie_name();
		//echo 'name:'.$name;
		if(!isset($_COOKIE[$name]))
			return FALSE;
			
		$mapper = new RememberMeMapper($this->gt);
		$mapper->set_table($this->tablename,$this->columnid);
		$userid = $mapper->check_token($_COOKIE[$name]);
		//echo $userid;
		return $userid;
					
	}
	public function forget_me($id){
		$name = $this->remember_me_cookie_name();
		//echo 'name:'.$name;
		if(!isset($_COOKIE[$name])){
			return FALSE;
		}
		setcookie($name, "", time()-3600);
		unset($_COOKIE[$name]);	
		$mapper = new RememberMeMapper($this->gt);
		$mapper->set_table($this->tablename,$this->columnid);	
		return $mapper->save_token($id,NULL);
	}
}
