<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('Paging'));
//load_mappers(array('TagMapper'));
//load_helpers(array('TimeHelper'));
class SendMailProcess extends BaseProcess {
	private $vendor_process = NULL;
	private $admin_process = NULL;
	
	function __construct($gt){
		parent::__construct($gt);
	}
	private function get_vendor_process(){
		if($this->vendor_process==NULL){
			load_process(array('VendorProcess'));
			$this->vendor_process = new VendorProcess($this->gt);
		}
		return $this->vendor_process;
	}
	private function get_admin_process(){
		if($this->admin_process==NULL){
			load_process(array('AdminProcess'));
			$this->admin_process = new AdminProcess($this->gt);
		}
		return $this->admin_process;
	}
	public function send_to_all_vendors($from,$subject,$message){
		$process = $this->get_vendor_process();
		$vpaging = new Paging();
		$vpaging->iDisplayLength = 1000;				
		$vpaging->iDisplayStart=0;
		$vpaging->sSortDir_0 = 'asc';
		$vpaging->iSortCol_0 = 'CompanyName';
		if(!$vpaging->validate()){
			$this->add_error('error validating vendor '.$vpaging->error_messages());
			return FALSE;
		}else{		
			$ret = TRUE;	
			$pagevendor = $process->get_all($vpaging,TRUE);
			if($pagevendor->data!=NULL){
				foreach($pagevendor->data as $vendor){
					$to = $vendor->Email;
					$headers = 'From:'.$from . "\r\n"; // Set from headers  
					$retx = mail($to, $subject, $message, $headers); // Send our email 
					if(!$retx){
						$this->add_error('failed to send email to: '.$to);						
					}
					$ret &= $retx;
				}
			}
			return $ret;			
		}
	}
	public function send_to_all_admins($from,$subject,$message){		
		$adminprocess = $this->get_admin_process();
		$adminemail = $adminprocess->get_admin_email();		
		$to = $adminemail;
		$headers = 'From:'.$from . "\r\n"; // Set from headers  
		$retx = mail($to, $subject, $message, $headers); // Send our email 
		if(!$retx){
			$this->add_error('failed to send email to: '.$to);						
		}
		return $retx;		
	}
}
?>
