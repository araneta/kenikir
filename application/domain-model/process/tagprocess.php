<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('Paging','Tag'));
load_mappers(array('TagMapper'));
load_helpers(array('TimeHelper'));
class TagProcess extends BaseProcess {
	private $tag_mapper = NULL;
	
	function __construct($gt){
		parent::__construct($gt);
	}
	
	private function get_tag_mapper(){
		if($this->tag_mapper==NULL){
			$this->tag_mapper = new TagMapper($this->gt);
		}
		return $this->tag_mapper;
	}
	public function get_all_solution($paging){
		$mapper = $this->get_tag_mapper();
		return $mapper->get_all_solution($paging);
	}
	public function add_solution_tag($tagvalueid){
		$mapper = $this->get_tag_mapper();
		$now = TimeHelper::get_time_in_utc();
		
		$tag = new TagValues();
		$tag->TagValueID = $tagvalueid;		
		$tag->CreatedDate = $now;
		return $mapper->add_solution_tag($tag);
	}
	public function update_solution_tag($oritag,$newtag){
		$mapper = $this->get_tag_mapper();
		return $mapper->update_solution_tag($oritag,$newtag);
	}
	public function delete_solution_tag($tag){
		$mapper = $this->get_tag_mapper();
		$mapper->delete_solution_tag($tag);
	}
	public function get_all_federal($paging){
		$mapper = $this->get_tag_mapper();
		return $mapper->get_all_federal($paging);
	}
	public function add_federal_tag($tagvalueid){
		$mapper = $this->get_tag_mapper();
		$now = TimeHelper::get_time_in_utc();
		
		$tag = new TagValues();
		$tag->TagValueID = $tagvalueid;		
		$tag->CreatedDate = $now;
		return $mapper->add_federal_tag($tag);
	}
	public function update_federal_tag($oritag,$newtag){
		$mapper = $this->get_tag_mapper();
		return $mapper->update_federal_tag($oritag,$newtag);
	}
	public function delete_federal_tag($tag){
		$mapper = $this->get_tag_mapper();
		$mapper->delete_federal_tag($tag);
	}
}
?>
