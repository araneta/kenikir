<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/remembermeprocess.php';

load_mappers(array('RememberMeMapper'));
load_helpers(array('TimeHelper'));

class VendorRememberMeProcess extends RememberMeProcess{
	protected $tablename = 'vendor';
	protected $columnid = 'VendorID';
	function __construct($gt){		
		parent::__construct($gt);
		
	}
	
}
