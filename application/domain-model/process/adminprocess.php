<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__).'/baseprocess.php';

load_entities(array('AdminLoginForm','Admin'));
load_mappers(array('AdminMapper'));
load_helpers(array('TimeHelper'));

class AdminProcess extends BaseProcess {
	private $admin;
	private $admin_mapper = NULL;
	
	function __construct($gt){
		parent::__construct($gt);
	}
	private function get_admin_mapper(){
		if($this->admin_mapper==NULL){
			$this->admin_mapper = new AdminMapper($this->gt);
		}
		return $this->admin_mapper;
	}
	private function encrypt_password($p){
		return md5($p);
	}
	function verify($form){
		$mapper = $this->get_admin_mapper();
		$admin = $mapper->find_admin($form->username,$this->encrypt_password($form->password));
		if($admin != NULL){
			//update login date and updated date
			$current = TimeHelper::get_time_in_utc();
			$admin->LogInDate = $current;
			$admin->UpdatedDate = $current;
			
			if(!$mapper->update_login_time($admin)){
				echo 'failed update login date';
				exit(0);
			}
			$this->admin = $admin;
			return TRUE;
		}
		return FALSE;
	}
	function admin(){
		return $this->admin;	
	}
	public function get_admin_email(){
		$mapper = $this->get_admin_mapper();
		return $mapper->get_admin_email();
	}
}
?>
