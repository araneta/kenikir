<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
					<div id="becomemember">
						<h1>Become a GOVonomy member</h1>
						<br />
						<div id="errmsg" class="left">
							<?php  
							if ($this->session->flashdata('message')){								
								echo $this->session->flashdata('message');
								echo '<br /><br />';
							}    
							?>    
						</div>
						<h2>Please choose your agency.</h2>
						<br /><br />
						<?php echo form_open('register/create',array('name'=>'newmemberform')); ?>							
						<div id="agencytype">
						<label>Agency Type: </label>
							<input name="AgencyType" type="radio" value="Federal" id="federal" class="required" /><label for="federal">Federal</label>
							<input name="AgencyType" type="radio" value="State" id="state" class="required" /><label for="state">State</label>
							<input name="AgencyType" type="radio" value="Local" id="local" class="required" /><label for="local">Local</label>
							<input name="AgencyType" type="radio" value="Non-Profit" id="non-profit" class="required" /><label for="non-profit">Non-Profit</label>
						</div>		
						<div class="clear"></div>										
						
						<div class="cols">
							<div class="col2">
								<div class="formitem">
									<label>Enter your agency or select from the list below:</label>
									<?php echo form_input('AgencyName',set_value('AgencyName'),'style="width:410px;height:24px;" class="required" '); ?>
								</div>
							</div>	
							<div class="col2">
								<div class="formitem">
									<label>Enter Sub-Agency Name:</label>
									<?php echo form_input('SubAgency',set_value('SubAgency'),'style="width:410px;height:24px;" class="required" '); ?>
								</div>
							</div>
							
						</div>
						<div class="clear"></div>	
						<br /><br />
						<div id="agencytab">
							<ul>
								<?php
								foreach($alphabet as $k=>$v){
								?>
									<li><a href="#<?php echo $k;?>"><span><?php echo $k;?></span></a></li>
								<?php	
								}
								?>																
							</ul>
							<?php 							
							foreach($alphabet as $k=>$v){
								?>
								<div id="<?php echo $k;?>">
								<ul class="agencyli">
									<?php 
									foreach($v as $agency){
									?>
									<li><a href="#"><?php echo $agency;?></a></li>
									<?php									
									}
									?>									
								</ul>
								</div>
								<?php
							}
							?>
							
						</div>
						<br /><br /><br />
						<input type="hidden" name="Email" value="<?php echo $email;?>" />
						<div class="formbutton">							
							<div class="colspan">
								<input type="submit" class="btn center" value="Next >" />																
							</div>
						</div>
						<?php echo form_close();?>
					</div>				
			</div>	
		</div>
	</div>
</div>	
<script type="text/javascript">
	validateForm('form[name="newmemberform"]',function(){
			$('#errmsg').html('Please enter missing fields<br /><br />');	
		});
	$( "#agencytab" ).tabs();
	$('.agencyli li a').click(function(e){
		$('input[name="AgencyName"]').val($(e.target).html());
	});
</script>

