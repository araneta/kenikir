<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
					<div id="becomemember">
						<h1>Email Verification</h1>
						<br /><br />
						<p>Please check your email <?php echo $email;?> and enter the verification code</p>
						<br />
						<p>or click on the verification link in the email.</p>
						<br /><br />						
						<?php echo form_open('register/verify_email',array('name'=>'newmemberform')); ?>							
						<div class="colspan">
							<div class="formitem" style="width:100%;float:none;">
								<?php echo form_input('Code',set_value('Code'),'style="width:510px;height:24px;display:block;" class="required" '); ?>
							</div>
							<div id="errmsg" class="left">
								<?php  
								if ($this->session->flashdata('message')){
									echo '<br />';
									echo $this->session->flashdata('message');
								}    
								?>    
							</div>
						</div>	
						<br /><br /><br />
						<div class="formbutton">							
							<div class="colspan">
								<input type="submit" class="btn center" value="Verify" />																
							</div>
						</div>
						<input type="hidden" name="Email" value="<?php echo $email;?>" />
						<?php echo form_close();?>
					</div>				
			</div>	
		</div>
	</div>
</div>	
	
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newmemberform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
	});
	
</script>
		

