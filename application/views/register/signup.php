<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
					<div id="becomemember">
						<h1>Become a GOVonomy member</h1>
						<br /><br />
						<p>Thank you for your interest in becoming a member of GOVonomy.com.<br />
						As a member you will be able to submit orders and create wishlists.
						</p>
						<br /><br />
						<div class="note">Please enter your email address below to get started</div>
						<?php echo form_open('register/check_email',array('name'=>'newmemberform')); ?>							
						<div class="colspan">
							<div class="formitem" style="width:100%;float:none;">
								<?php echo form_input('email',set_value('email'),'style="width:510px;height:24px;display:block;" class="required email" '); ?>
							</div>
							<div id="errmsg" class="left">
								<?php  
								if ($this->session->flashdata('message')){
									echo '<br />';
									echo $this->session->flashdata('message');
								}    
								?>    
							</div>
						</div>	
						<br /><br /><br />
						<div class="formbutton">							
							<div class="colspan">
								<input type="submit" class="btn center" value="Become a Member" />																
							</div>
						</div>
						<?php echo form_close();?>
					</div>				
			</div>	
		</div>
	</div>
</div>	
		
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newmemberform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
	});
	
</script>
