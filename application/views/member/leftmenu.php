<div id="shopby">
	<div id="shophead">
		<div id="stitle">Shop by:</div>
	</div>
	
	<div id="shoptab" style="height:auto">
		<div id="box-sol">
			<div id="box-ven">
				<div id="box-prod">
					<div id="box-fed">
						<div class="tabbed-area">
							<ul class="tabs group">
								<li id="ssol"><a class="scap" href="<?echo base_url();?>#box-sol">Solution</a></li>
								<li id="sven"><a class="scap" href="<?echo base_url();?>#box-ven">Vendor</a></li>
								<li id="sprod"><a class="scap" href="<?echo base_url();?>#box-prod">Product</a></li>
								<li id="sfed"><a class="scap" href="<?echo base_url();?>#box-fed">Federal Requirements</a></li>
							</ul>							
						</div>	
					</div>	
				</div>
			</div>		
		</div>	
	</div>
	<div id="flist" class="leftpanel">		
		<?php
		$uri  = $this->uri->uri_string();
		$base = base_url();
		$menu = array(
				'members/'=>'View Member Information'
				,'members/editinfo'=>'Edit Member Information'				
				);		
		?>		
		<ul>
			<li class="fitem">
				<span class="fname">Member</span>
				<ul>
					<?php
					foreach($menu as $k=>$v){
						$c = '';
						if($uri==$k){
							$c='class="active"';
						}
						echo '<li '.$c.'><a href="'.$base.$k.'">'.$v.'</a></li>';
					}		
					?>				
				</ul>
			</li>
			<li  class="fitem">
				<span class="fname">Need Help?</span>
				<ul>
					<li><a href="#">FAQs</a></li>
					<li><a href="#">Contact Us</a></li>
				</ul>
			</li>
		</ul>
		

	</div>
	<!--endtab-->	
	
</div>
