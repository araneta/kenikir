<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
					<div id="becomemember">
						<h1>Enter Member Information</h1>
						<br /><br />
						<div id="errmsg" class="left">
							<?php  
							if ($this->session->flashdata('message')){
								
								echo $this->session->flashdata('message');
								echo '<br /><br />';
							}    
							?>    
						</div>
						<?php echo form_open('members/setinfo',array('name'=>'newmemberform')); ?>							
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label style="float:left;">User ID</label>
									<label style="float:left;font-weight:normal;margin-left:30px;"><?php echo $this->session->userdata('userid');?></label>
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<a href="#">Edit Password &amp; Security Questions</a>
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>First Name</label>
									<input type="text" name="FirstName" class="required" value="<?php echo $member->FirstName; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Last Name</label>
									<input type="text" name="LastName" class="required" value="<?php echo $member->LastName; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Phone</label>
									<input type="text" name="Phone" class="required" value="<?php echo $member->Phone; ?>"/>
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Phone 2</label>
									<input type="text" name="Phone2" class="required" value="<?php echo $member->Phone2; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Email</label>
									<input type="text" name="Email" class="required" value="<?php echo $member->Email; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Email 2</label>
									<input type="text" name="Email2" class="required" value="<?php echo $member->Email2; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="colspan">
								<div class="formitem">
									<label style="float:left">Do you want to receive GOVonomy Newsletters?</label>
									<div class="formradio"><input type="radio" name="SubscribeNewsletter" <?php if($member->SubscribeNewsletter==1) echo 'checked="checked"';?> value="1" class="required" id="newsletteryes"/><label for="newsletteryes">Yes</label></div>
									<div class="formradio"><input type="radio" name="SubscribeNewsletter" <?php if($member->SubscribeNewsletter==0) echo 'checked="checked"';?> value="0" class="required" /><label id="newsletterno">No</label></div>
								</div>
							</div>
						</div>	
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>URL</label>
									<input type="text" name="URL" class="required" value="<?php echo $member->URL; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>URL 2</label>
									<input type="text" name="URL2" class="required" value="<?php echo $member->URL2; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Address</label>
									<input type="text" name="Address" class="required" value="<?php echo $member->Address; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>City</label>
									<input type="text" name="City" class="required" value="<?php echo $member->City; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>State</label>
									<input type="text" name="State" class="required" value="<?php echo $member->State; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Zip</label>
									<input type="text" name="Zip" class="required" value="<?php echo $member->Zip; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Address 2</label>
									<input type="text" name="Address2" class="required" value="<?php echo $member->Address2; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>City 2</label>
									<input type="text" name="City2" class="required" value="<?php echo $member->City2; ?>" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>State 2</label>
									<input type="text" name="State2" class="required" value="<?php echo $member->State2; ?>" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Zip 2</label>
									<input type="text" name="Zip2" class="required" value="<?php echo $member->Zip2; ?>" />
								</div>
							</div>
						</div>
						
						<div class="clear"></div>
						
						
						<br /><br /><br /><br />
						<div class="formbutton">							
							<div class="colspan">
								<input type="submit" class="btn center" value="Next >" />																
							</div>
						</div>
						
						<?php echo form_close();?>
					</div>				
			</div>	
		</div>
	</div>
</div>	
	
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newmemberform"]',function(){
			$('#errmsg').html('Please enter missing fields<br /><br />');	
		});
	});
	
</script>
		

