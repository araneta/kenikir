<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
					<div id="becomemember">
						<h1>Enter Security Information</h1>
						<br /><br />
						<div id="errmsg" class="left">
							<?php  
							if ($this->session->flashdata('message')){
								
								echo $this->session->flashdata('message');
								echo '<br /><br />';
							}    
							?>    
						</div>
						<?php echo form_open('members/setpass',array('name'=>'newmemberform')); ?>							
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Password</label>
									<input type="password" name="Password_1" id="Password_1" class="required" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Confirm Password</label>
									<input type="password" name="Password_2" id="Password_2" class="required"/>
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Security Question 1</label>
									<input type="text" name="Question1" class="required" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Answer 1</label>
									<input type="text" name="Answer1" class="required" />
								</div>
							</div>
						</div>
						<div class="cols" style="width:530px;">
							<div class="col2">
								<div class="formitem">
									<label>Security Question 2</label>
									<input type="text" name="Question2" class="required" />
								</div>
							</div>
							<div class="col2">
								<div class="formitem">
									<label>Answer 2</label>
									<input type="text" name="Answer2" class="required" />
								</div>
							</div>
						</div>
						<div class="clear"></div>
						
						
						<br /><br /><br /><br />
						<div class="formbutton">							
							<div class="colspan">
								<input type="submit" class="btn center" value="Next >" />																
							</div>
						</div>
						
						<?php echo form_close();?>
					</div>				
			</div>	
		</div>
	</div>
</div>	
	
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newmemberform"]',function(){
			$('#errmsg').html('Please enter missing fields<br /><br />');	
		});
	});
	
</script>
		

