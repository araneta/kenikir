<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('member/leftmenu'); ?>
			<div id="main">			
				<div id="memberinfoform">
					<h1>View Member Information</h1>										
					<div id="errmsg" class="red"><?php  
							if ($this->session->flashdata('message')){
								echo '<br /><br />'.$this->session->flashdata('message');
							}    
							?></div>
					<table class="tblinfo">
					<tr>
						<td width="200" class="infotblcaption">User ID</td>
						<td width="450" ><?php echo $member->MemberID; ?></td>
					</tr>	
					<tr>
						<td class="infotblcaption">First Name</td>
						<td ><?php echo $member->FirstName; ?></td>
					</tr>	
					<tr>
						<td class="infotblcaption">Phone</td>
						<td ><?php echo $member->Phone; ?></td>
					</tr>	
					<tr>
						<td class="infotblcaption">Phone 2</td>
						<td ><?php echo $member->Phone2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Email</td>
						<td ><?php echo $member->Email; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Email 2</td>
						<td ><?php echo $member->Email2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Receive Newsletter</td>
						<td ><?php echo $member->SubscribeNewsletter==1?'Yes':'No'; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">URL</td>
						<td ><?php echo $member->URL; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Address</td>
						<td ><?php echo $member->Address; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">City</td>
						<td ><?php echo $member->City; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">State</td>
						<td ><?php echo $member->State; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Zip</td>
						<td ><?php echo $member->Zip; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Address 2</td>
						<td ><?php echo $member->Address2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">City 2</td>
						<td ><?php echo $member->City2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">State 2</td>
						<td ><?php echo $member->State2; ?></td>
					</tr>
					<tr>
						<td class="infotblcaption">Zip 2</td>
						<td ><?php echo $member->Zip2; ?></td>
					</tr>
					</table>		
				</div>
			</div>
		</div>
	</div>		
	<div class="clear"></div>
</div>
