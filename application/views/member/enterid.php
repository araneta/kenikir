<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
					<div id="becomemember">
						<h1>Become a GOVonomy Member</h1>
						<br />
						<p>Enter User ID</p>
						<br /><br /><br />						
						<?php echo form_open('members/setid',array('name'=>'newmemberform')); ?>							
						<div class="colspan">
							<div class="formitem" style="width:100%;float:none;">
								<?php echo form_input('MemberID',set_value('MemberID'),'style="width:510px;height:24px;display:block;" class="required" '); ?>
							</div>
							<div id="errmsg" class="left">
								<?php  
								if ($this->session->flashdata('message')){
									echo '<br />';
									echo $this->session->flashdata('message');
								}    
								?>    
							</div>
						</div>	
						<br /><br /><br />
						<div class="formbutton">							
							<div class="colspan">
								<input type="submit" class="btn center" value="Next >" />																
							</div>
						</div>
						
						<?php echo form_close();?>
					</div>				
			</div>	
		</div>
	</div>
</div>	
	
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="newmemberform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
	});
	
</script>
		

