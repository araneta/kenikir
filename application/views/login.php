<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('help');?>
			<div id="main">				
				<div id="loginform">
					<h1>Login</h1>
					<br /><br />
					<div id="errmsg" class="left">
							<?php  
							if ($this->session->flashdata('message')){
								
								echo $this->session->flashdata('message');
								echo '<br /><br />';
							}    
							?>    
					</div>
					<?php echo form_open('login/verify',array('name'=>'loginform')); ?>							
					<div class="cols" style="width:530px;">
						<div class="col2">
							<div class="formitem">
								<label style="float:left;">User ID</label>
								<input type="text" name="UserID" class="required" value="" />
							</div>
							<div class="formitem">
								<label style="float:left;">Password</label>
								<input type="password" name="Password" class="required" value="" />
							</div>
							<div class="formitem" style="margin-top:10px;">							
								<input name="RememberMe" id="rememberme" value="1" type="checkbox">
								<label style="font-weight:normal;display:inline;" class="grey" for="rememberme">Remember me on this computer</label>						
							</div>
							<div style="margin-top:20px;" class="formbutton">							
								<input type="submit" value="Login" class="btn">
								<a style="margin-left:10px;line-height:30px;" href="#">Forgot Password?</a>						
							</div>
						</div>
						<div style="height:200px;" class="coldivider"></div>
						<div class="col2">
							<div style="margin:50px 0px">
								<p class="subtitle grey">No account yet?</p>			
								<br>		
								<input type="button" value="Become a GOVonomy Member" onclick="window.location='register'" class="btn">
							</div>
						</div>
					</div>
				</div>										
			</div>	
		</div>
	</div>
</div>	
		
<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="loginform"]',function(){
			$('#errmsg').html('Please enter missing fields');	
		});
	});
	
</script>
