<div id="footer">
			<div class="inner-wrap">
				<div id="bottomlogo">
					<img src="<?php echo base_url('images/footer_logo.png');?>">
				</div>
				<ul>
					<li><a href="#">Conditions of Use</a></li>
					<li><a href="#">Privacy Notice</a></li>
					<li><a id="copy" href="#">&copy; 1996-2013. Govonomy.com, Inc. or its affiliates</a></li>
				</ul>
			</div>	
		</div>
		<!--end footer-->
	
