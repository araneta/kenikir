<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('filterby');?>
			<div id="main">
				
					<div id="searchresult">
						<div id="searchkeywords">
							Search Results for "<?php echo $keywords;?>"
						</div>
						<?php $this->load->view('searchpager');?>
						<div id="searchresultitems">
							<?php
							$thumburl = $this->config->item('thumb_url');
							foreach($searchresult->data as $p){
							?>
							<div class="sritem">
								<div class="srthumb">
									<img src="<?php echo $thumburl.'/'.$p->MediaID.'/'. $p->ProductImage;?>">
								</div>
								<div class="srdesc">
									<h2><a href="<?php echo base_url('v/'.$p->Alias.'/'.$p->ProductID.'/'.sanitize_filename($p->Name));?>"><?php echo $p->Name;?></a></h2>
									<h3><a href="<?php echo base_url('v/'.$p->Alias);?>"><?php echo $p->CompanyName;?></a></h3>
									<p><?php echo $p->ProductInfo;?></p>
								</div>								
								<div class="srorder">
									<div class="actionbox">
										<a href="#" class="btn">Add to Cart</a>
										<a href="<?php echo base_url('wishlists/add/'.$p->ProductID);?>" class="btn">Add to Wish List</a>
									</div>
								</div>
							</div>
							<?php
							}
							?>
							
						</div>
						<?php $this->load->view('searchpager');?>
					</div>
				
			</div>	
		</div>
	</div>
</div>	
		
