<div id="shopby">
	<div id="shophead">
		<div id="stitle">Shop by:</div>
	</div>	
	<div id="shoptab">
		<div id="box-sol">
			<div id="box-ven">
				<div id="box-prod">
					<div id="box-fed">
						<div class="tabbed-area">
							<ul class="tabs group">
								<li id="ssol"><a class="scap" href="#box-sol">Solution</a></li>
								<li id="sven"><a class="scap" href="#box-ven">Vendor</a></li>
								<li id="sprod"><a class="scap" href="#box-prod">Product</a></li>
								<li id="sfed"><a class="scap" href="#box-fed">Federal Requirements</a></li>
							</ul>
							
							<div class="box-wrap">
							
								<div class="box-sol">
									<ul>
										<?php			
										$i = 0;		
										$maxitem = 12;
										$max = count($solutions)>$maxitem? $maxitem:count($solutions);
										for($i=0;$i<$max;$i++){
											$solution = $solutions[$i];
											echo '<li><a href="'.base_url('s?keywords='.$solution->v.'&filter=solutions').'">'.$solution->v.'</a></li>';
										}
										?>
										<li><hr /></li>
										<li class="viewall"><a href="<?php echo base_url('viewall/solutions#box-sol');?>">View All Solutions</a></li>										
									</ul>													
								</div>
								
								<div class="box-ven">
									<ul>
										<?php			
										$max = count($vendors)>$maxitem? $maxitem:count($vendors);
										for($i=0;$i<$max;$i++){		
											$vendor = $vendors[$i];										
											echo '<li><a href="'.base_url('s?keywords='.$vendor->v.'&filter=vendor').'">'.$vendor->v.'</a></li>';
										}
										?>	
										<li><hr /></li>										
										<li class="viewall"><a href="<?php echo base_url('viewall/vendors#box-ven');?>">View All Vendors</a></li>										
									</ul>	
								</div>
								
								<div class="box-prod">
									<ul>
										<?php					
										$max = count($products)>$maxitem? $maxitem:count($products);
										for($i=0;$i<$max;$i++){	
											$product = $products[$i];
											echo '<li><a href="'.base_url('s?keywords='.$product->v.'&filter=product').'">'.$product->v.'</a></li>';
										}
										?>
										<li><hr /></li>
										<li class="viewall"><a href="<?php echo base_url('viewall/products#box-prod');?>">View All Products</a></li>										
									</ul>	
								</div>
								<div class="box-fed">
									<ul>
										<?php					
										$max = count($federals)>$maxitem? $maxitem:count($federals);
										for($i=0;$i<$max;$i++){	
											$federal = $federals[$i];
											echo '<li><a href="'.base_url('s?keywords='.$federal->v.'&filter=federal').'">'.$federal->v.'</a></li>';
										}
										?>
										<li><hr /></li>										
										<li class="viewall"><a href="<?php echo base_url('viewall/federals#box-fed');?>">View All Federal Requirements</a></li>										
									</ul>	
								</div>
								
							</div>
							
						</div>	
					</div>	
				</div>
			</div>		
		</div>	
	</div>
	<div id="bottomtab"></div>
	<!--endtab-->	
	<?php $this->load->view('quicklinks');?>
</div>
