<div class="searchpager">
	<div class="searchpagectrl">Showing <?php echo $searchresult->start+1;?>-<?php echo $searchresult->end;?> of <?php echo $searchresult->totalrecords;?> Results | 
	<?php
		if($searchresult->totalpages>1){
			if($searchresult->page<$searchresult->totalpages)
				$nextpage = $searchresult->page+1;
			else
				$nextpage = $searchresult->totalpages;
			echo '<a href="s?keywords='.$keywords.'&filter='.$filter.'&page='.$nextpage.'">Next</a> | '; 
			for($p=1;$p<=$searchresult->totalpages;$p++)
			{
				$c= '';
				if($p==$searchresult->page)
					$c = 'class="srcurpage"';
				echo '<a href="s?keywords='.$keywords.'&filter='.$filter.'&page='.$p.'" '.$c.'>'.$p.'</a>&nbsp;';
			}
			echo '| <a href="s?keywords='.$keywords.'&filter='.$filter.'&page='.$searchresult->totalpages.'">Last</a>';
		}
				
	?>
	</div>
	<div class="searchpagesort">Choose a category to enable sorting: <a href="s?keywords=<?php echo $keywords;?>&filter=solutions">Solution</a> | <a href="s?keywords=<?php echo $keywords;?>&filter=vendor">Vendor</a> | <a href="s?keywords=<?php echo $keywords;?>&filter=product">Product</a> | <a href="s?keywords=<?php echo $keywords;?>&filter=federal">Federal Regulation</a></div>
</div>
