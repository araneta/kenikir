<div id="shopby">
	<div id="shophead">
		<div id="stitle">Shop by:</div>
	</div>
	
	<div id="shoptab" style="height:auto">
		<div id="box-sol">
			<div id="box-ven">
				<div id="box-prod">
					<div id="box-fed">
						<div class="tabbed-area">
							<ul class="tabs group">
								<li id="ssol"><a class="scap" href="<?echo base_url();?>#box-sol">Solution</a></li>
								<li id="sven"><a class="scap" href="<?echo base_url();?>#box-ven">Vendor</a></li>
								<li id="sprod"><a class="scap" href="<?echo base_url();?>#box-prod">Product</a></li>
								<li id="sfed"><a class="scap" href="<?echo base_url();?>#box-fed">Federal Requirements</a></li>
							</ul>							
						</div>	
					</div>	
				</div>
			</div>		
		</div>	
	</div>
	<div id="filterby" class="leftpanel">
		<div id="ftitle">Filter By:</div>
		<div id="flist">
			<ul>
				<li class="fitem">
					<span class="fname">Solutions</span>
					<ul>
						<?php					
						foreach($solutions as $solution){
							$c = '';
							if($filter2=='solutions' && $keywords2==$solution){
								$c = 'class="filterselected"';
								echo '<li '.$c.'><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter).'">'.$solution.'</a></li>';
							}else{
								echo '<li><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter.'&keywords2='.$solution.'&filter2=solutions').'">'.$solution.'</a></li>';
							}
						}
						?>
					</ul>
				</li>
				<li class="fitem">
					<span class="fname">Vendors</span>
					<ul>
					<?php					
					foreach($vendors as $vendor){
						$c = '';
						if($filter2=='vendor' && $keywords2==$vendor->v){
							$c = 'class="filterselected"';
							echo '<li '.$c.'><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter).'">'.$vendor->v.'</a></li>';
						}else{
							echo '<li><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter.'&keywords2='.$vendor->v.'&filter2=vendor').'">'.$vendor->v.'</a></li>';
						}
					}
					?>											
					</ul>
				</li>
				<li class="fitem">
					<span class="fname">Products</span>
					<ul>
						<?php										
						foreach($products as $product){
							$c = '';
							if($filter2=='product' && $keywords2==$product->v){
								$c = 'class="filterselected"';
								echo '<li '.$c.'><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter).'">'.$product->v.'</a></li>';
							}else{
								echo '<li '.$c.'><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter.'&keywords2='.$product->v.'&filter2=product').'">'.$product->v.'</a></li>';
							}
						}
						?>
					</ul>
				</li>
				<li class="fitem">
					<span class="fname">Federal Requirements</span>
					<ul>
						<?php										
						foreach($federals as $federal){
							$c = '';
							if($filter2=='federal' && $keywords2==$federal){
								$c = 'class="filterselected"';
								echo '<li '.$c.'><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter).'">'.$federal.'</a></li>';
							}else{	
								echo '<li '.$c.'><a href="'.base_url('s?keywords='.$keywords.'&filter='.$filter.'&keywords2='.$federal.'&filter2=federal').'">'.$federal.'</a></li>';
							}
						}
						?>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!--endtab-->	
	<?php $this->load->view('quicklinks');?>
</div>
