<?php
if(!isset($ismember))
	$ismember = FALSE;
if(!isset($wishlist))
	$wishlist = NULL;	
?>	
<div id="header">
	<div class="inner-wrap">	
		<div id="logomenu">
			<div id="logo">
				<img src="<?php echo base_url();?>images/govonomy_logo.png">					
			</div>		
			<div id="topmenu">
				<ul>
					<li><a href="#">My GOVonomy</a></li>
					<li><a href="#">Today's Deals</a></li>
					<li><a href="<?php echo $this->config->item('vendor_url').'/register';?>">Sell</a></li>
					<li><a href="#">Help</a></li>
				</ul>
			</div>			
		</div>	
		<div id="searchbar">
			<form action="<?php echo base_url();?>s" method="get">
				<label>Search</label>						
				<div id="searchtext">
					<div id="leftst"></div>
					<div id="middlest">
						<input type="text" name="keywords" id="keywords" value="<?php if(!empty($keywords))echo $keywords;?>" />
					</div>	
					<div id="cats">
						<span id="selcat"><?php if(!empty($filtertitle))echo $filtertitle;?></span><img src="<?php echo base_url();?>images/down.png">
						<div id="catsddl">
							<select id="category" name="filter">
								<option value="all" <?php if($filter=='all') echo 'selected="selected"';?>>All Categories</option>
								<option value="solutions"  <?php if($filter=='solutions') echo 'selected="selected"';?>>Solutions</option>
								<option value="vendor"  <?php if($filter=='vendor') echo 'selected="selected"';?>>Vendor</option>
								<option value="product"  <?php if($filter=='product') echo 'selected="selected"';?>>Product</option>
								<option value="federal"  <?php if($filter=='federal') echo 'selected="selected"';?>>Federal Regulation</option>								
							</select>
						</div>
					</div>
				</div>
				
				<input type="submit" id="searchbtn" value="" />
			</form>
		</div>
		<div id="toolbar">			
				<?php
				if($ismember){
					?>
					<div class="titem" id="usernamebox" target="loginregister">					
						<div class="ttitle">Hello, <?php echo $this->session->userdata('firstname');?>!</div>
						<div class="tdesc">Welcome Back</div>
					</div>	
				<?php	
				}else{					
				?>	
					<div class="titem" id="usernamebox" target="loginregister">
						<div class="ttitle">Hello!</div>
						<div class="tdesc">Sign In or Register</div>
					</div>	
				<?php
				}
				?>
			
			<div class="titem" id="cartbox" target="cartempty">
				<div class="tcol">
					<div class="timg"><img src="<?php echo base_url();?>images/cart.png"></div>
				</div>
				<div class="tcol">
					<div class="ttitle">My cart</div>
					<div class="tdesc">0 items</div>
				</div>		
				<?php
				if($ismember){
				?>
				<div class="arrow_box" id="cartitem">					
					<div class="abitem">
						<a href="#">Iron Mountain Backup Tape Vaulting</a>
					</div>
					<div class="abitem">
						<a href="#">Livevault</a>
					</div>
					<div class="abitem" style="float:left">
						<a href="#" class="btn viewcartempty">View Cart (2 items)</a>						
					</div>					
				</div>
				<?php
				}
				?>
			</div>
			<div class="titem" id="wishbox" target="wishempty">
				<div class="ttitle">Wish List</div>
				<div class="tdesc">
					<?php 
					if(count($wishlist)>0)
						echo count($wishlist).' items';
					?>
				</div>
				<?php
				if($ismember){
				?>
				<div class="arrow_box" id="wishitem">
					<?php
						if(count($wishlist)>0){
							foreach($wishlist as $wl){
					?>
								<div class="abitem">
									<a href="<?php echo base_url('wishlists/edit/'.$wl->WishlistID);?>"><?php echo $wl->Name;?></a><br />
									<span class="small"><?php echo $wl->WishlistItemCount;?> Items</span>
								</div>
					<?php
							}
						}
					?>							
					<div class="abitem hr"></div>
					<div class="abitem center">
						<a href="#" class="wishempty">Create a New List</a>
					</div>
				</div>
				<?php
				}
				?>
			</div>
			
			<div class="arrow_box" id="loginregister">
				<?php
				if($ismember){
				?>
				<div class="abitem">
					<a href="#">My Account</a>
				</div>
				<div class="abitem">
					<a href="#">My Orders</a>
				</div>
				<div class="abitem">
					<a href="<?php echo base_url('login/logout');?>" class="signout">Sign Out</a>
				</div>
				<?php
				}else{
				?>
				<div class="abitem">
					<a href="<?php echo base_url('login');?>" class="btn signin">Sign In</a><br />
					<span class="small">No Account Yet? <a href="<?php echo base_url('register');?>">Click Here</a></span>
				</div>	
				<?php
				}
				?>
			</div>
			<div class="arrow_box" id="accountbox">
				<div class="abitem">
					<a href="#">My Account</a>
				</div>
				<div class="abitem">
					<a href="#">My Orders</a>
				</div>
				<div class="abitem">
					<a href="/login/logout" class="signout">Sign Out</a>
				</div>
			</div>
			<?php
			if($ismember){
			?>
			<div class="arrow_box" id="cartempty">
				<div class="abitem">
					<span style="line-height:20px;" class="small">Your Shopping Cart is Empty</span>
					<a href="#" class="btn viewcartitem">View Cart (0 items)</a>
					<div class="clear"></div>
				</div>
				
			</div>			
			
			<?php
			}
			?>
			<div class="arrow_box" id="wishempty">
				<div class="abitem">
					<a href="/wishlists/create" class="wishitem">Create a New List</a>
				</div>
			</div>					
			
		</div>
		
	</div>
	<div class="clear"></div>				
</div>
