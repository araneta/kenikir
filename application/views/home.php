<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('shopby');?>
			<div id="main">
				<div id="maincontent">
					<div id="banner">
						<div id="btnbanner">
							<a href="#" id="lnksolution"></a>
							<a href="#" id="lnkvendor"></a>
							<a href="#" id="lnkproduct"></a>
							<a href="#" id="lnkfederal"></a>
						</div>
					</div>
					<div class="cols">
						<div id="colnews" class="col">
							<div class="colhead">
								<h2>News &amp; Announcements</h2>
							</div>	
							<div class="colbody">
								<ul>
									<li><a href="#">News &amp; Announcement Item here. 2 lines of text per item</a></li>
									<li><a href="#">News &amp; Announcement Item here. 2 lines of text per item</a></li>
									<li><a href="#">News &amp; Announcement Item here. 2 lines of text per item</a></li>
								</ul>
								<br />
								<a href="#" class="colmore">Read more</a>
							</div>
						</div>
						<div id="colvendor" class="col">
							<div class="colhead">
								<h2>Feature Vendor</h2>
							</div>	
							<div class="colbody">
								<img src="<?php echo base_url();?>images/apiphany_logo.jpg">
								<br /><br />
								<p>APIphany is a new kind of API delivery platform born in cloud from a new kind of company.</p>
								<br />
								<a href="#" class="colmore">Learn more</a>
							</div>
						</div>
						<div id="colproduct" class="col">
							<div class="colhead">
								<h2>Feature Product</h2>
							</div>	
							<div class="colbody">
								<img src="<?php echo base_url();?>images/apiphany_product.png">
								<br /><br />
								<p>APIphany's API Delivery Platform was designed to help organizations drive developer adoption, control and scale their APIs, and gain insights into their API ecosystem</p>
								<br />
								<a href="#" class="colmore">Learn more</a>
							</div>
						</div>
					</div>
				</div>
				<div id="rightbar">
					<a href="#"><span id="suggest"></span></a>
					<a href="#"><img src="<?php echo base_url();?>images/BH_ad.png"></a>
					<a href="#"><img src="<?php echo base_url();?>images/IM_ad.png"></a>
				</div>
			</div>	
		</div>
	</div>
</div>	
		
