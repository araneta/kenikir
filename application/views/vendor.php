<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('shopby');?>
			<div id="main">
				<div id="viewvendor">
					<div id="errmsg" class="left">
						<?php  
						if ($this->session->flashdata('message')){								
							echo $this->session->flashdata('message');
							echo '<br /><br />';
						}    
						?>    
					</div>
					<div class="vendgeneralinfo">
						<div class="vendimgcol">
							<div class="vendimgcont">
								<img style="max-height: 100%; max-width: 100%"  src="<?php echo $this->config->item('vendor_images_url').'/'.$vendor->MediaID.'/'.$vendor->ImagePath;?>">
							</div>
						</div>
						<div class="venddesccol">
							<h1><?php echo $vendor->CompanyName;?></h1>								
							<p><?php echo $vendor->PublicInfo;?></p>
						</div>						
						<div class="clear"></div>
					</div>		
					
					<?php	
					if(count($vendorproducts)>0){					
						?>
						<div class="vendorprod">
							<h3>Products List</h3>
							<ul>
							<?php
							
							$imgdir = $this->config->item('images_url').'/'.$vendor->MediaID;						
							$i=0;
							//var_dump($moreproducts);
							foreach($vendorproducts as $prod){							
								$purl = base_url('v/'.$vendor->Alias.'/'.$prod->ProductID.'/'.sanitize_filename($prod->Name));
							?>
								<li class="vendthumb">
								<a href="<?php echo $purl;?>">
									<span class="vendprodcont">
										<?php 
										if(!empty($prod->ProductImage)){
										?>	
											<img title="<?php echo $prod->Name;?>" style="max-height: 100%; max-width: 100%"  src="<?php echo $imgdir.'/'.$prod->ProductImage;?>" />
										<?php
										}else
										{
											echo '<span title="'.$prod->Name.'" class="vendnoimg"></span>';
										}									
										?>										
									</span>
									<span class="vendprodtitle"><?php echo $prod->Name;?></span>
								</a>
								</li>			
						<?php
							$i++;
							}	
							?>
							</ul>
						</div>
						<?php	
					}					
					?>
						
					
					<?php					
					//var_dump($vendorvideos);	
					if(count($vendor->VideoFileName)>0){
						?>
						<div class="vendvideos">
							<h3>Videos</h3>
							<?php
							$viddir = $this->config->item('vendor_videos_url').'/'.$vendor->MediaID;
							$i = 0;
							foreach($vendor->VideoFileName as $vid){																
							?>
								<a target="_blank" href="<?php echo $viddir.'/'.$vid;?>" class="vendviditem">
									<span class="vidthumb">
										
									</span>
									<span class="vidtitle">
										<?php echo $vendor->VideoTitle[$i];?>
									</span>								
								</a>
													
							<?php							
								$i++;
							}							
							?>
						</div>
						<?php
					}
					?>
										
					<?php
					if(count($vendor->DocFileName)>0){
						?>
						<div class="venddocs">
							<h3>Additional Information</h3>
							<ul>
							<?php
							$docdir = $this->config->item('vendor_docs_url').'/'.$vendor->MediaID;
							$validext = array('doc','ppt','pdf','txt','xls');						
							$i=0;
							foreach($vendor->DocFileName as $doc){										
									$furl = $docdir.'/'.$doc;
									$ext = pathinfo($doc, PATHINFO_EXTENSION);	
									if(!in_array($ext,$validext)){
										$ext = 'doc';
									}
								?>
									<li class="venddoc <?php echo $ext;?>"><a target="_blank" href="<?php echo $furl;?>"><?php echo $vendor->DocTitle[$i];?></a></li>			
							<?php
								$i++;
							}	
							?>
							</ul>
						</div>
						<?php
					}						
					?>
						
					<br />					
					
				</div>
			</div>	
		</div>
	</div>
</div>	

<script type="text/javascript">
	jQuery(document).ready(function($){ 
	
	});
	
</script>
		

		
