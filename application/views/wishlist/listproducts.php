<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('shopby');?>
			<div id="main">
				<div id="wishlistform">					
					<div id="errmsg" class="left">
						<?php  
						if ($this->session->flashdata('message')){								
							echo $this->session->flashdata('message');
							echo '<br /><br />';
						}    
						?>    
					</div>
					<h1 style="float:left;margin-right:20px;"><?php echo $wishlist->Name;?></h1><a style="margin-top:8px;float:left;" href="<?php echo base_url('wishlists/edit/'.$wishlist->WishlistID);?>">Edit</a>					
					<div class="clear"></div>
					<br />
					<div>
						<?php echo $wishlist->CustomerNotes;?>
					</div>
					<br /><br />
					<table id="tblwishlistproducts" class='grid display' width="700">
					  <thead>
						<tr>						  
						  <th width="150">Product Name</th>
						  <th width="150">Vendor</th>
						  <th width="150">Date Created</th>				  
						  <th width="190">Action</th>				  
						</tr>
					  </thead>
					  <tbody>
						
					  </tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>
</div>	
<script type="text/javascript">
  /* Table initialisation */
  $(document).ready(function() {
	$('#tblwishlistproducts').dataTable( {	  
	 "aoColumnDefs": [
			{ 
				"mRender": function ( data, type, row ) {										
					var html = '<a href="<?php echo base_url();?>products/'+row[3]+'">'+row[0]+'</a>';
					return html;					
                },
                "aTargets": [0],
                "bSortable": true
			},
			{ 
				"mRender": function ( data, type, row ) {												
					var html = '<a href="<?php echo base_url();?>vendors/'+row[4]+'">'+row[1]+'</a>';
					return html;
										
                },
                "aTargets": [1],
                "bSortable": true
			},
			{ 
				"mRender": function ( data, type, row ) {												
					var html = '<a class="btn" href="<?php echo base_url();?>charts/add/'+row[3]+'">Add to Cart</a></a>';
					html += '<a style="margin:10px;float:left" href="<?php echo base_url();?>wishlists/delete_item/'+row[5]+'">Delete</a>';
					return html;
										
                },
                "aTargets": [3],
                "bSortable": true
			}
	  ],
	  "bFilter":false,
	  "bProcessing": true,
	  "aaSorting": [[ 1, "asc" ]],
	  "bServerSide": true,
	  "bJQueryUI": true,	  
	  "sPaginationType": "full_numbers",
	  "sAjaxSource": "<?php echo base_url();?>wishlists/list_products/<?php echo $wishlist->WishlistID;?>",
	  
	});	
	$('#tblwishlistproducts_length').css("display", "none");
  });
</script>
