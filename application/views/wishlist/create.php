<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('shopby');?>
			<div id="main">
				<div id="wishlistform">
					<h1>Create New Wish List</h1>
					<br />
					<div id="errmsg" class="left">
						<?php  
						if ($this->session->flashdata('message')){								
							echo $this->session->flashdata('message');
							echo '<br /><br />';
						}    
						?>    
					</div>
					
					<?php echo form_open('wishlists/save',array('name'=>'wishlistform')); ?>							
					<div class="cols">
						<div class="formitem">
							<label>Enter a name for your Wishlist</label>
							<input type="text" class="required" name="Name" style="width:410px;height:24px;" />
						</div>						
						<div class="formitem">
							<label>Enter a password for your Wishlist to share it with others</label>
							<input type="text" class="required" name="Password" style="width:410px;height:24px;" />
						</div>						
						<div class="formitem">
							<label>Description</label>
							<textarea name="CustomerNotes" style="width:410px;height:190px;"></textarea>
						</div>						
						<br /><br />
						<div class="formbutton">													
							<div class="col2">
								<input type="submit" class="btn center" value="        Save        " />																
							</div>	
							<div class="col2">
								<input onclick="window.location='<?php echo base_url('wishlists');?>'" type="button" class="btn center" value="        Cancel        " />																
							</div>	
						</div>
					</div>
					<?php echo form_close();?>
				</div>
			</div>	
		</div>
	</div>
</div>	

<script type="text/javascript">
	jQuery(document).ready(function($){ 
		validateForm('form[name="wishlistform"]',function(){
			$('#errmsg').html('Please enter missing fields<br />');	
		});
	});
	
</script>
		

		
