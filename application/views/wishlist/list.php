<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('shopby');?>
			<div id="main">
				<div id="wishlistform">
					<h1>View All Wishlists</h1>
					<br />
					<div id="errmsg" class="left">
						<?php  
						if ($this->session->flashdata('message')){								
							echo $this->session->flashdata('message');
							echo '<br /><br />';
						}    
						?>    
					</div>
					<table id="tblwishlist" class='grid display' width="540">
					  <thead>
						<tr>						  
						  <th width="290">Wishlist Name</th>
						  <th width="140">Date Created</th>				  
						  <th width="140">Last Item Added</th>				  
						</tr>
					  </thead>
					  <tbody>
						
					  </tbody>
					</table>
				</div>
			</div>	
		</div>
	</div>
</div>	
<script type="text/javascript">
  /* Table initialisation */
  $(document).ready(function() {
	$('#tblwishlist').dataTable( {	  
	 "aoColumnDefs": [
			{ 
				"mRender": function ( data, type, row ) {										
					var html = '<a href="<?php echo base_url();?>wishlists/edit/'+row[3]+'">'+row[0]+'</a>';
					return html;					
                },
                "aTargets": [0],
                "bSortable": true
			},
			{ 
				"mRender": function ( data, type, row ) {							
					if(row[2]!=null){
						var html = '<a href="<?php echo base_url();?>wishlists/view/'+row[3]+'">'+row[2]['Name']+'</a>';
						return html;
					}
					return '';
                },
                "aTargets": [2],
                "bSortable": false
			}
	  ],
	  "bFilter":false,
	  "bProcessing": true,
	  "aaSorting": [[ 1, "asc" ]],
	  "bServerSide": true,
	  "bJQueryUI": true,	  
	  "sPaginationType": "full_numbers",
	  "sAjaxSource": "<?php echo base_url();?>wishlists/list_wish",
	  
	});	
	$('#tblwishlist_length').css("display", "none");
  });
</script>
