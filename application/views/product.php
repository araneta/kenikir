<div id="content">
	<div class="inner-wrap">	
		<div id="sbox">
			<?php $this->load->view('shopby');?>
			<div id="main">
				<div id="viewproduct">
					<div id="errmsg" class="left">
						<?php  
						if ($this->session->flashdata('message')){								
							echo $this->session->flashdata('message');
							echo '<br /><br />';
						}    
						?>    
					</div>
					<div class="prodgeneralinfo">
						<div class="prodimgcol">
							<div id="imgcont">
								<img style="max-height: 100%; max-width: 100%"  src="<?php echo $this->config->item('images_url').'/'.$vendor->MediaID.'/'.$product->ProductImage;?>">
							</div>
						</div>
						<div class="proddesccol">
							<h1><?php echo $product->Name;?></h1>	
							<a href="<?php echo base_url('v/'.$vendor->Alias);?>"><?php echo $vendor->CompanyName;?></a>
							<p><?php echo $product->ProductInfo;?></p>
						</div>
						<div class="prodactioncol">
							<div class="actionbox">
								<a href="#" class="btn">Add to Cart</a>
								<a href="<?php echo base_url('wishlists/add/'.$product->ProductID);?>" class="btn">Add to Wish List</a>
							</div>
						</div>
						<div class="clear"></div>
					</div>		
					
					<?php						
					if(count($product->VideoTitle)>0){
						?>
						<div class="prodvideos">						
							<h3>Videos</h3>
							<?php
							$viddir = $this->config->item('videos_url').'/'.$vendor->MediaID;
							$i=0;
							foreach($product->VideoTitle as $vid){
							?>
								
								<a target="_blank" href="<?php echo $viddir.'/'.$product->VideoFileName[$i];?>" class="prodviditem">
									<span class="vidthumb">
										
									</span>
									<span class="vidtitle">
										<?php echo $vid;?>
									</span>								
								</a>
							<?php
							$i++;
							}	
							?>
						</div>
						<?php						
					}
					?>
										
					<?php
					if(count($product->DocTitle)>0){
						?>
						<div class="proddocs">
							<h3>Additional Product Information</h3>
							<ul>
							<?php
							$docdir = $this->config->item('docs_url').'/'.$vendor->MediaID;
							$validext = array('doc','ppt','pdf','txt','xls');						
							$i=0;
							foreach($product->DocTitle as $doc){							
								$furl = $docdir.'/'.$product->DocFileName[$i];
								$ext = pathinfo($product->DocFileName[$i], PATHINFO_EXTENSION);	
								if(!in_array($ext,$validext)){
									$ext = 'doc';
								}
							?>
								<li class="proddoc <?php echo $ext;?>">
									<a target="_blank" href="<?php echo $furl;?>"><?php echo $doc;?></a>
								</li>			
							<?php
							$i++;
							}	
							?>
							</ul>
						</div>
						<?php
					}						
					?>
						
					
					<?php	
					if(count($moreproducts)>0){					
						?>
						<div class="moreprod">
							<h3>More Products from <?php echo $vendor->CompanyName;?></h3>
							<ul>
							<?php
							$imgdir = $this->config->item('images_url').'/'.$vendor->MediaID;						
							$i=0;
							//var_dump($moreproducts);
							foreach($moreproducts as $prod){							
								$purl = base_url('v/'.$vendor->Alias.'/'.$prod->ProductID.'/'.sanitize_filename($prod->Name));
							?>
								<li class="prodthumb">
								<a href="<?php echo $purl;?>">
									<?php 
									if(!empty($prod->ProductImage)){
									?>	
										<img title="<?php echo $prod->Name;?>" style="max-height: 100%; max-width: 100%"  src="<?php echo $imgdir.'/'.$prod->ProductImage;?>" />
									<?php
									}else
									{
										echo '<span title="'.$prod->Name.'" class="prodnoimg"></span>';
									}
									?>
								</a>
								</li>			
						<?php
							$i++;
							}	
							?>
							</ul>
						</div>
						<?php	
					}					
					?>
						
					
					<?php	
					
					if(count($recentproducts)>0){												
						?>
						<div class="recentprod">
							<h3>Recently Viewed</h3>
							<ul>
							<?php
							$i=0;
							//var_dump($moreproducts);
							$imgurl = $this->config->item('images_url');
							foreach($recentproducts as $prod){			
								$imgdir = $imgurl.'/'.$prod->MediaID;										
								$purl = base_url('v/'.$prod->VendorAlias.'/'.$prod->ProductID.'/'.sanitize_filename($prod->Name));
							?>
								<li class="prodthumb">
								<a href="<?php echo $purl;?>">
									<?php 
									if(!empty($prod->ProductImage)){
									?>	
										<img title="<?php echo $prod->Name;?>" style="max-height: 100%; max-width: 100%"  src="<?php echo $imgdir.'/'.$prod->ProductImage;?>" />
									<?php
									}else
									{
										echo '<div title="'.$prod->Name.'" class="prodnoimg"></div>';
									}
									?>
								</a>
								</li>			
						<?php
							$i++;
							}	
							?>
							</ul>
						</div>
						<?php	
					}					
					?>
						
					<br />					
					
				</div>
			</div>	
		</div>
	</div>
</div>	

<script type="text/javascript">
	jQuery(document).ready(function($){ 
	
	});
	
</script>
		

		
